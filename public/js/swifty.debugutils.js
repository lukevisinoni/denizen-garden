/**
 * This assumes the existence of jQuery since laravel debugbar uses it
 */
if (typeof(Swifty) == 'undefined') {
    // namespace
    var Swifty = {};
    Swifty.$ = jQuery;
}

(function($) {

    $(function(){
        if (typeof(Swifty.utils) == 'undefined') {
            Swifty.utils = {};
        }
        
        var LinkIndicator = Swifty.utils.LinkIndicator = PhpDebugBar.DebugBar.Indicator.extend({
        
            tagName: 'a',
        
            render: function() {
                LinkIndicator.__super__.render.apply(this);
                this.bindAttr('href', function(href) {
                    this.$el.attr('href', href);
                });
            }
        
        });
        
        var DownloadLinkIndicator = Swifty.utils.DownloadLinkIndicator = LinkIndicator.extend({
        
            render: function() {
                // inject invisible iframe into html
                var downloadHref;
                var iframe = document.createElement('iframe');
                iframe.id = "debugDownload";
                iframe.style.display = "none";
                this.bindAttr('href', function(href) {
                    downloadHref = href;
                });
                document.body.appendChild(iframe);
                
                this.$el.on("click", function(event) {
                    event.preventDefault();
                    $(iframe).attr('src', downloadHref);
                });
                DownloadLinkIndicator.__super__.render.apply(this);
            }
        
        });
        

    });

})(Swifty.$);
