'use strict'

var path = require('path')
var webpack = require('webpack')

module.exports = {
    entry: {
        //denizen: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'resources/assets/js/denizen', 'index.jsx')],
        //main: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'resources/assets/js/app', 'index.jsx')],
        //properties: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'resources/assets/js/app', 'properties.jsx')],
        //account: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'resources/assets/js/account', 'index.js')],
        spa: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, 'resources/assets/js/spa', 'index.js')],
    },

    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, 'public/js/app')
    },

    // resolve: {
    //     alias: {config: path.resolve(__dirname, 'resources/assets/js/spa/config')}
    // },

    // needed to make aws-sdk work
    node: {
        fs: 'empty'
    },

    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react&presets[]=stage-0' },
            // matches only non-partial (s)css files (partials start with an underscore... I do not want to match them)
            // { test: /(.+\/|^)[a-z0-9\.]+\.s?css$/, include: path.join(__dirname, 'resources/assets/js'), loaders: ["style", "css", "sass"] },
            { test: /\.s?css$/, exclude: /node_modules/, loaders: ["style", "css", "sass"] },

            // needed to make aws-sdk work
            { test: /aws-sdk/, loaders: [ 'transform?aws-sdk/dist-tools/transform' ] },
            { test: /\.json$/, loaders: ['json'] }
        ]
    },

    plugins: [
        /*new webpack.DefinePlugin({
            'process.env':{
                'NODE_ENV': JSON.stringify('production')
            }
        }),*/
        //new webpack.optimize.DedupePlugin(),
        /*new webpack.optimize.UglifyJsPlugin({
            compress:{
                warnings: true
            }
        })*/
        // @todo use this for shared code (see: https://webpack.github.io/docs/optimization.html#multi-page-app)
        // new CommonsChunkPlugin("admin-commons.js", ["ap1", "ap2"]),
    ]
}
