<?php
use Step\Acceptance\UserLogin;
use Step\Acceptance\UserRegistration as UserReg;
use \Page\Register as RegisterPage;
use \Page\Dashboard as DashboardPage;
use \Page\Login as LoginPage;
use Swift\User;

class AuthCest
{
    protected $tester;
    
    public function _before(AcceptanceTester $I)
    {
        $this->tester = $I;
    }
    
    public function _after(AcceptanceTester $I)
    {
    }
    
    /**
     * All public methods not starting with underscore are ran as tests
     */
    
    public function tryToRegisterNewUser(UserReg $I)
    {
        $I->am("guest");
        $I->wantTo("register a new account");
        $I->amOnPage("/"); // start on home page
        $I->goToRegistrationPage();
        $I->dontSeeAuthentication();
        $I->dontSeeRecord('users', ['email' => $I->getUserTestDataValue('email')]);
        $I->registerNewUserAndAuthenticate();
        $I->seeRecord('users', ['email' => $I->getUserTestDataValue('email')]);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
        
        // make sure authentication persists across multiple requests
        $I->see("Admin Dashboard");
        $I->click(DashboardPage::$unsecureLink);
        $I->click(LoginPage::$loginLink);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
    }
    
    public function tryToRegisterNewUserLogoutAndLogin(UserReg $I, UserLogin $U)
    {
        $I->am("guest");
        $I->wantTo("register a new account and then be able to log in");
        $I->amOnPage("/"); // start on home page
        $I->goToRegistrationPage();
        $I->dontSeeAuthentication();
        $I->dontSeeRecord('users', ['email' => $I->getUserTestDataValue('email')]);
        $I->registerNewUserAndAuthenticate($I->getUserRegistrationData());
        $I->seeRecord('users', ['email' => $I->getUserTestDataValue('email')]);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
        
        // now logout...
        $U->clickLogoutButton();
        $I->dontSeeAuthentication();
        $I->seeCurrentUrlEquals("/");
        
        // and log back in...
        $U->loginByClickingLoginAndUsingForm($I->getUserTestDataValue('email'), $I->getUserTestDataValue('password'));
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
    }
    
    public function tryToRegisterWithErrors(UserLogin $I)
    {
        $I->am("guest");
        $I->wantTo("get meaningful error messages when I fill out the register form incorrectly");
        $I->amOnPage("/");
        $I->click(RegisterPage::$registerLink);
        
        // submit form with blank name, invalid e-mail and one blank password
        $bad = [
            'name' => '',
            'email' => 'TheNard#thestupidsonfamily.com',
            'password' => '',
            'password_confirmation' => '1_F0R60T_T0_3N7ER_P4$5W0RD'
        ];
        $I->submitForm(RegisterPage::$form, $bad);
        $I->dontSeeRecord('users', ['email' => $bad['email']]);
        $I->dontSeeAuthentication();
        $I->dontSeeCurrentUrlEquals(DashboardPage::$url);
        $I->seeFormErrorMessages([
            'name' => 'The name field is required.',
            'email' => 'The email must be a valid email address.',
            'password' => 'The password field is required.'
        ]);
        // data should be persisted (except for password fields)
        $bad['password'] = "";
        $bad['password_confirmation'] = "";
        $I->seeInFormFields(RegisterPage::$form, $bad);
        
        // submit form with really long name, blank e-mail, and mismatched passwords
        $mixed = [
            'name' => 'Leonard Stupidson McBrayer Johnson Taniqua McDormand McDonald Sutherland Brown Mickelson White Black Kelly Loren Willis Lucas Lawrence Kennedy Jones Smith Carlson Thompson Simpson Stimson Jetson Laurenson Carol Jenson Lippi Coogan Seinfeldt Connors Leonard McCoy Charter Dickson Lin Chin Gonzolez Braun Chekov Borat Martinson Finn Mertins',
            'email' => '', 
            'password' => 'stupidpassword',
            'password_confirmation' => 'dumbpassword'
        ];
        $I->submitForm(RegisterPage::$form, $mixed);
        $I->dontSeeRecord('users', ['email' => $mixed['email']]);
        $I->dontSeeAuthentication();
        $I->dontSeeCurrentUrlEquals(DashboardPage::$url);
        $I->seeFormErrorMessages([
            'name' => 'The name may not be greater than 255 characters.',
            'email' => 'The email field is required.',
            'password' => 'The password confirmation does not match.'
        ]);
        // data should be persisted (except for password fields)
        $mixed['password'] = "";
        $mixed['password_confirmation'] = "";
        $I->seeInFormFields(RegisterPage::$form, $mixed);
    }
    
    public function makeSureRegistrationEmailsNewUser()
    {
    }
    
    public function tryToLogout(UserLogin $I)
    {
        $I->am("authenticated (logged-in) user");
        $I->wantTo("log out");
        $I->haveRecord("users", $I->getUserTestData());
        $I->amLoggedAs($I->user());
        $I->seeAuthentication();
        $I->clickLogoutButton();
        $I->dontSeeCurrentUrlEquals(DashboardPage::$url);
        $I->dontSeeAuthentication();
    }
    
    public function tryToLoginWithBlankForm(UserLogin $I)
    {
        $I->am("unauthorized user");
        $I->wantTo("get login error messages because of blank form");
        $I->dontSeeAuthentication();
        $I->loginByClickingLoginAndUsingForm('', '');
        $I->seeCurrentUrlEquals(LoginPage::$url);
        $I->dontSeeAuthentication();
        $I->seeFormErrorMessages([
            'email' => 'The email field is required',
            'password' => 'The password field is required'
        ]);
    }
    
    public function tryToLoginWithInvalidLogin(UserLogin $I)
    {
        $I->am("unauthorized user");
        $I->wantTo("get login rejected because of invalid login");
        $I->dontSeeAuthentication();
        $I->loginByClickingLoginAndUsingForm('invalidlogin', 'invalidpass');
        $I->seeCurrentUrlEquals(LoginPage::$url);
        $I->dontSeeAuthentication();
        $I->seeFormErrorMessages(['email' => 'These credentials do not match our records.']);
    }
    
    public function tryToLoginWithInvalidPassword(UserLogin $I)
    {
        $I->am("unauthorized user");
        $I->wantTo("get login rejected because of invalid password");
        $I->dontSeeAuthentication();
        $I->loginByClickingLoginAndUsingForm(null, 'invalidpass'); // first value being null results in it using UserLogin default
        $I->seeCurrentUrlEquals(LoginPage::$url);
        $I->dontSeeAuthentication();
        $I->seeFormErrorMessages(['email' => 'These credentials do not match our records.']);
    }
    
    public function tryTologin(UserLogin $I)
    {
        $I->am("user");
        $I->wantTo("log in to the application");
        $I->haveRecord("users", $I->getUserTestData());
        $I->dontSeeAuthentication();
        $I->loginByClickingLoginAndUsingForm();
        $I->seeAuthentication();
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->see(DashboardPage::$header);
        // authentication should persist between requests
        $I->click(DashboardPage::$unsecureLink);
        $I->seeCurrentUrlEquals("/");
        $I->click(LoginPage::$loginLink);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
    }
    
    public function tryAccessingAuthRouteWithAuthenticatedUser(UserLogin $I)
    {
        $I->am("authenticated user");
        $I->wantTo("access a secured route");
        $I->haveRecord("users", $I->getUserTestData());
        $I->amLoggedAs($I->user());
        $I->amOnPage(DashboardPage::$url);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->see(DashboardPage::$header);
    }
    
    public function tryAccessingAuthRouteWithNonAuthenticatedUser(UserLogin $I)
    {
        $I->am("non-authenticated user");
        $I->wantTo("access a secured route");
        $I->amOnPage(DashboardPage::$url);
        $I->seeCurrentUrlEquals(LoginPage::$url); // sends them to login page
        $I->dontSee(DashboardPage::$header);
    }
    
    public function checkingRememberMeWhenLoggingInCreatesCookie(UserLogin $I)
    {
        $I->am("user");
        $I->wantTo("have a cookie installed on my machine so the application remembers me");
        $I->haveRecord("users", $I->getUserTestData());
        $I->dontSeeAuthentication();
        $I->logInUsingLoginFormAndRememberMe();
        $I->seeAuthentication();
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->see(DashboardPage::$header);
        $I->seeCookie(LoginPage::rememberMeCookieName());
    }
    
    /**
     * @todo Come back to this. I don't think this is actually testing
     * that the cookie is working. 
     */
    public function tryAuthenticatingWithRememberMeCookie(UserLogin $I)
    {
        $I->am("user");
        $I->wantTo("skip logging in every time by using a \"remember\" me cookie");
        $I->haveRecord("users", $I->getUserTestData());
        $I->dontSeeAuthentication();
        $I->logInUsingLoginFormAndRememberMe();
        $I->seeAuthentication();
        $I->click(DashboardPage::$unsecureLink);
        $I->seeCurrentUrlEquals("/");
        $I->click(LoginPage::$loginLink);
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->seeAuthentication();
        // I think this starts a new session or something... not sure
        // scratch that... I think this can't see authentication on "/" because its routed directly to a view
        $I->amOnPage(LoginPage::$url);
        $I->seeAuthentication();
        $I->seeCookie(LoginPage::rememberMeCookieName());
        
    }
    
    public function tryLoggingOutToSeeIfItClearsTheRememberMeCookie(UserLogin $I)
    {
        $I->am("user");
        $I->wantTo("clear my \"remember me\" cookie by logging out");
        $I->haveRecord("users", $I->getUserTestData());
        $I->logInUsingLoginFormAndRememberMe();
        $I->seeAuthentication();
        $I->seeCurrentUrlEquals(DashboardPage::$url);
        $I->clickLogoutButton();
        $I->seeCurrentUrlEquals("/");
        $I->dontSeeAuthentication();
        $I->click(LoginPage::$loginLink);
        $I->seeCurrentUrlEquals(LoginPage::$url);
        $I->dontSeeCookie(LoginPage::rememberMeCookieName());
    }
    
    /**
     * @todo Need to test the forgot password feature
     */
    
}
