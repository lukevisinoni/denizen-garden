<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that subdomain pulls account info into session');
$I->amOnPage('/'); 
$I->see('DeniZen Garden');
$I->seeInTitle("Luke's Property Management");
$I->seeInSession("account.short", "luke");