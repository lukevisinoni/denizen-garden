<?php
/**
 |------------------------------------------------------------------------------
 | Per-account customization tests
 |------------------------------------------------------------------------------
 |
 | All the customers that use DeniZen are served their application from this one
 | single application instance. The subdomain that's used to access the app
 | is what determines which "account" application to serve. This is a series of
 | tests to ensure that all the per-account customization and configurations are
 | done properly and that customer A doesn't see customer B's data and/or assets
 |
 */

class CustCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
    }
}
