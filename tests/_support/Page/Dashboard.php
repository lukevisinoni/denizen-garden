<?php
namespace Page;

class Dashboard
{
    public static $url = '/home';
    
    public static $link = "DeniZen Garden";
    
    public static $header = 'Admin Dashboard';
    
    public static $unsecureLink = "a.unsecure";
}
