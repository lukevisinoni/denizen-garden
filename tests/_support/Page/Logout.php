<?php
namespace Page;

class Logout
{
    // include url of current page
    public static $url = '/logout';
    
    public static $logoutLink = ".logout a";
}
