<?php
namespace Page;

class Login
{
    /**
     * @var string URL to login route
     */
    public static $url = '/login';
    
    /**
     * @var string Credential form fields
     */
    public static $credFields = [
        'login' => 'email',
        'password' => 'password',
        'remember' => 'remember'
    ];
    
    /**
     * @var string CSS Selector for submit button
     */
    public static $submit = "button[type=submit]";
    
    public static $loginLink = ".login a";
    
    /**
     * @todo Within the auth framework, there are options you can select
     *       which will break this. But I think it's fine doing it like
     *       this because it's just a test
     * @todo Also I'm not sure if this is the right place for this. I'm
     *       thinking maybe there should be a helper method called like
     *       $I->hasRememberMeCookie() and it just generates name in the
     *       helper. Come back to it later.
     */
    public static function rememberMeCookieName($guardName = null, $guardClass = null)
    {
        return implode('_', [
            'guard_prefix' => 'remember',
            'guard_name' => is_null($guardName) ? 'web' : $guardName,
            'guard_class' => is_null($guardClass) ? sha1('Illuminate\Auth\SessionGuard') : sha1($guardClass)
        ]);
    }
    
    public static function rememberMeAuthSessionName($guardName = null, $guardClass = null)
    {
        return implode('_', [
            'guard_prefix' => 'login',
            'guard_name' => is_null($guardName) ? 'web' : $guardName,
            'guard_class' => is_null($guardClass) ? sha1('Illuminate\Auth\SessionGuard') : sha1($guardClass)
        ]);
    }
    
    public static function getCredentialName($type)
    {
        if (array_key_exists($type, self::$credFields)) return self::$credFields[$type];
        else return $type;
        //    throw \Exception("There is no '$type' credential"); // @todo create better exception here
    }
     
}
