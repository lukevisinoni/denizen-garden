<?php
namespace Page;

class Register
{
    /**
     * @var string URL to login route
     */
    public static $url = '/register';
    
    /**
     * @var string CSS Selector for submit button
     */
    public static $submit = "button[type=submit]";
    
    public static $registerLink = ".register a";
    
    public static $form = ".main form";
}
