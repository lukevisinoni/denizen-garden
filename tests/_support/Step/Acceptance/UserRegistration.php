<?php
namespace Step\Acceptance;

use \Page\Dashboard as DashboardPage;
use \Page\Register as RegPage;

class UserRegistration extends \AcceptanceTester
{
    /**
     * Clicks on "register", which brings you to registration page
     */
    public function goToRegistrationPage()
    {
        $this->amOnPage("/");
        $this->click(RegPage::$registerLink);
    }
    /**
     * Assuming we are on register new user page, this step will register a new user
     */
    public function registerNewUserAndAuthenticate(Array $userData = [])
    {
        if (empty($userData)) $userData = $this->getUserTestData();
        if (!array_key_exists("password_confirmation", $userData) && array_key_exists("password", $userData)) {
            $userData['password_confirmation'] = $userData['password'];
        }
        // this step assumes the user is on the register page, so we better check right off the bat
        $this->seeCurrentUrlEquals(RegPage::$url);
        $this->submitForm(RegPage::$form, $userData);
    }

}