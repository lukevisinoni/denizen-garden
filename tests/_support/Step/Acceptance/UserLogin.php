<?php
namespace Step\Acceptance;
use \Page\Login as LoginPage;
use \Page\Logout as LogoutPage;
use \Page\Dashboard as DashboardPage;
use \Swift\User;

class UserLogin extends \AcceptanceTester
{
    /**
     * @todo Would probably make sense to split this into goToLoginPage() and authenticateUsingLoginForm()
     */
    public function loginByClickingLoginAndUsingForm($login = null, $password = null, $remember = false)
    {
        if (is_null($login)) $login = $this->getUserTestDataValue(LoginPage::getCredentialName('login'));
        if (is_null($password))  $password = $this->getUserTestDataValue(LoginPage::getCredentialName('password'));
        // start on home page
        $this->amOnPage("/");
        $this->click(LoginPage::$loginLink);
        $this->seeCurrentUrlEquals(LoginPage::$url);
        $this->fillField(LoginPage::getCredentialName('login'), $login);
        $this->fillField(LoginPage::getCredentialName('password'), $password);
        if ($remember) $this->checkOption(LoginPage::getCredentialName('remember'));
        $this->click(LoginPage::$submit);
    }
    
    /**
     * This simply provides a more readable interface than $I->loginByClickingLoginAndUsingForm(null, null, true)
     */
    public function logInUsingLoginFormAndRememberMe($login = null, $password = null)
    {
        return $this->loginByClickingLoginAndUsingForm($login, $password, true);
    }
    
    /**
     * Sets "remember me" cookie so that the application will fore-go the
     * login page and authenticate the user by their id and token.
     */
    public function rememberLoggedInUser(\Swift\User $user)
    {
        $cookie = [
            'name' => LoginPage::rememberMeCookieName(),
            'value' => $user->getAuthIdentifier() . "|" . $user->getRememberToken(),
            'expires' => time() + (10 * 365 * 24 * 60 * 60) // ten years from now
        ];
        
        $this->setCookie($cookie['name'], $cookie['value'], ['expires' => $cookie['expires']]);
        $this->amOnPage("/");
        $this->seeCookie($cookie['name']);
    }
    
    public function clickLogoutButton()
    {
        $this->amOnPage(DashboardPage::$url);
        $this->click(LogoutPage::$logoutLink);
    }
}