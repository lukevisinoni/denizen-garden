<?php
use \Page\Login as LoginPage;
use Swift\User;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;
    
    protected $user;
    
    protected $userData = [];
    
    public function __construct(\Codeception\Scenario $scenario)
    {
        parent::__construct($scenario);
        $this->userData = [
            'account_id' => 1,
            'name' => 'Luke Visinoni',
            'email' => "lvisinoni@denizengarden.dev",
            'password' => str_random(8),
            'remember_token' => str_random(32)
        ];
    }
    
    public function user()
    {
        if (!$this->user) {
            $this->user = User::where(['email' => $this->getUserTestDataValue('email')])->first();
        }
        return $this->user;
    }
    
    /**
     * Returns raw user data (optionally can send hashed password)
     */
    public function getUserTestData($hashedPassword = true)
    {
        $data = $this->userData;
        if ($hashedPassword) $data['password'] = User::hashPassword($data['password']);
        return $data;
    }
    
    public function getUserTestDataValue($field, $default = null)
    {
        $data = $this->getUserTestData(false);
        if (array_key_exists($field, $data)) return $data[$field];
        return $default;
    }
    
    /**
     * Returns only login (email) and unhashed password in array
     */
    public function getUserLoginCredentials($remember = false)
    {
        $creds = [
            'login' => $this->getUserTestDataValue(LoginPage::getCredentialName('login')),
            'password' => $this->getUserTestDataValue(LoginPage::getCredentialName('password'))
        ];
        if ($remember) $creds['remember'] = $this->getUserTestDataValue(LoginPage::getCredentialName('remember'));
        return $creds;
    }
    
    /**
     * Returns user data array in registration form format (no remember token, non-hashed password, and password_confirmation
     */
    public function getUserRegistrationData()
    {
        $data = $this->getUserTestData(false);
        $data['password_confirmation'] = $data['password'];
        return $data;
    }
}
