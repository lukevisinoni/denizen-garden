<?php namespace Command;

use \FunctionalTester;
use Illuminate\Support\Facades\Artison;
use Illuminate\Support\Facades\Storage;

class DomainMapCest
{

    protected $maps = ['primary' => 'primary.map', 'subdomain' => 'subdomain.map'];
    
    protected $disk = "testing";
    
    public function _before(FunctionalTester $I)
    {
        $this->destroyMaps();
    }

    public function _after(FunctionalTester $I)
    {
        $this->destroyMaps();
    }
    
    protected function disk()
    {
        return Storage::disk($this->disk);
    }
    
    protected function destroyMaps()
    {
        foreach ($this->maps as $map => $name) {
            if ($this->disk()->exists($name)) {
                $this->disk()->delete($name);
            }
        }
    }

    public function testGenerateCustomPrimaryDomainMap(FunctionalTester $I)
    {
        $this->assertFalse($this->disk()->exists($this->maps['primary']));
        $this->assertFalse($this->disk()->exists($this->maps['subdomain']));
        $exitCode = Artisan::call('domain:map', ['--primary' => true]);
        $this->assertTrue($this->disk()->exists($this->maps['primary']));
        $this->assertFalse($this->disk()->exists($this->maps['subdomain']));
    }

    public function testGenerateSubDomainMap(FunctionalTester $I)
    {
        $this->assertFalse($this->disk()->exists($this->maps['primary']));
        $this->assertFalse($this->disk()->exists($this->maps['subdomain']));
        $exitCode = Artisan::call('domain:map', ['--subdomain' => true]);
        $this->assertFalse($this->disk()->exists($this->maps['primary']));
        $this->assertTrue($this->disk()->exists($this->maps['subdomain']));
    }

    public function testGenerateDomainMapsNoFlags(FunctionalTester $I)
    {
        $this->assertFalse($this->disk()->exists($this->maps['primary']));
        $this->assertFalse($this->disk()->exists($this->maps['subdomain']));
        $exitCode = Artisan::call('domain:map');
        $this->assertTrue($this->disk()->exists($this->maps['primary']));
        $this->assertTrue($this->disk()->exists($this->maps['subdomain']));
    }

    public function testGenerateDomainMapsBothFlags(FunctionalTester $I)
    {
        $this->assertFalse($this->disk()->exists($this->maps['primary']));
        $this->assertFalse($this->disk()->exists($this->maps['subdomain']));
        $exitCode = Artisan::call('domain:map', ['--subdomain' => true, '--primary' => true]);
        $this->assertTrue($this->disk()->exists($this->maps['primary']));
        $this->assertTrue($this->disk()->exists($this->maps['subdomain']));
    }
}
