<?php namespace Config;

use Swift\Swifty\Config\ConfigAggregator;
use Swift\Swifty\Config\Aggregate\ArrayAggregate;
use Swift\Swifty\Config\Aggregate\DirAggregate;
use Swift\Swifty\Config\Aggregate\JsonAggregate;

class ConfigAggregatorTest extends \Codeception\TestCase\Test
{
    protected $arr1 = [
        'app' => [
            'title' => 'Test title',
            'url' => 'http://www.example.com',
        ],
        'mail' => [
            'driver' => 'smtp',
            'host' => 'smtp.mailgun.org',
            'port' => '25',
            'from' => ['address' => 'lvisinoni@denizengarden.com', 'name' => 'Luke Visinoni'],
            'encryption' => 'tls',
            'sendmail' => '/usr/sbin/sendmail -bs',
            'storage' => [
                'ext' => 'email.txt',
                'dir' => './storage/emails',
            ],
        ],
        'cache' => [
            'default' => 'file',
            'stores' => [
                'apc' => [
                    'driver' => 'apc',
                ],
                'array' => [
                    'driver' => 'array',
                ],
                'database' => [
                    'driver' => 'database',
                    'table'  => 'cache',
                    'connection' => null,
                ],
                'file' => [
                    'driver' => 'file',
                    'path'   => './storage/framework/cache',
                ],
            ]
        ],
        'session' => [
            'driver' => 'file',
            'lifetime' => 120,
            'expire_on_close' => false,
            'encrypt' => false,
            'files' => './storage/framework/sessions',
            'connection' => null,
            'table' => 'sessions',
            'lottery' => [2, 100],
            'cookie' => 'laravel_session',
            'path' => '/',
            'domain' => null,
            'secure' => false,
        ],
    ];
    
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Test instantiation and assemble empty
     */
    public function testEmptyAggregatorReturnsEmptyArray()
    {
        $config = new ConfigAggregator;
        $this->assertEquals([], $config->toArray());
    }
    
    public function testArrayConfigAggregateAcceptsArray()
    {
        $arrayAgg = new ArrayAggregate($this->arr1);
        $config = new ConfigAggregator;
        $config->push($arrayAgg);
        $this->assertEquals($this->arr1, $arrayAgg->toArray());
    }
    
    // test the push method of adding new aggregates (adds to bottom of the stack)
    public function testAggregatorAggregatesArrayConfigAggregatePush()
    {
        $arr1 = new ArrayAggregate($this->arr1);
        $arr2 = new ArrayAggregate([
            'app' => [
                'title' => 'New Title',
            ],
            'mail' => [
                'host' => 'smtp.example.org',
                'from' => ['address' => 'luke.visinoni@gmail.com', 'name' => 'Lucas D Visinoni'],
            ],
            'cache' => [
                'default' => 'array',
            ],
            'session' => [
                'lottery' => [5, 100],
            ],
        ]);
        $config = new ConfigAggregator;
        
        // first add the main config values and test their values...
        $config->push($arr1);
        $conf1 = $config->toArray();
        $this->assertEquals('Test title', $conf1['app']['title']);
        $this->assertEquals('smtp.mailgun.org', $conf1['mail']['host']);
        $this->assertEquals('lvisinoni@denizengarden.com', $conf1['mail']['from']['address']);
        $this->assertEquals('Luke Visinoni', $conf1['mail']['from']['name']);
        $this->assertEquals('file', $conf1['cache']['default']);
        $this->assertEquals(2, $conf1['session']['lottery'][0]);
        $this->assertEquals(100, $conf1['session']['lottery'][1]);
        
        // now add an additional array aggregate and test that values were overwritten...
        $config->push($arr2);
        $conf2 = $config->toArray();
        $this->assertEquals('New Title', $conf2['app']['title']);
        $this->assertEquals('smtp.example.org', $conf2['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf2['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf2['mail']['from']['name']);
        $this->assertEquals('array', $conf2['cache']['default']);
        $this->assertEquals(5, $conf2['session']['lottery'][0]);
        $this->assertEquals(100, $conf2['session']['lottery'][1]);
    }
    
    // test the unshift method of adding new aggregates (adds to top of the stack)
    public function testAggregatorAggregatesArrayConfigAggregateUnshift()
    {
        $arr1 = new ArrayAggregate($this->arr1);
        $arr2 = new ArrayAggregate([
            'app' => [
                'title' => 'New Title',
            ],
            'mail' => [
                'host' => 'smtp.example.org',
                'from' => ['address' => 'luke.visinoni@gmail.com', 'name' => 'Lucas D Visinoni'],
            ],
            'cache' => [
                'default' => 'array',
            ],
            'session' => [
                'lottery' => [5, 100],
            ],
        ]);
        $config = new ConfigAggregator;
        
        // first add the additional config values and test their values...
        $config->push($arr2);
        $conf1 = $config->toArray();
        $this->assertEquals('New Title', $conf1['app']['title']);
        $this->assertEquals('smtp.example.org', $conf1['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf1['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf1['mail']['from']['name']);
        $this->assertEquals('array', $conf1['cache']['default']);
        $this->assertEquals(5, $conf1['session']['lottery'][0]);
        $this->assertEquals(100, $conf1['session']['lottery'][1]);
        
        // now add the main array aggregate to the bottom of the stack and test that values weren't overwritten...
        $config->unshift($arr1);
        $conf2 = $config->toArray();
        // this first one is just to ensure that the "main" config array was added to the bottom
        $this->assertEquals('http://www.example.com', $conf2['app']['url']);
        $this->assertEquals('New Title', $conf2['app']['title']);
        $this->assertEquals('smtp.example.org', $conf2['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf2['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf2['mail']['from']['name']);
        $this->assertEquals('array', $conf2['cache']['default']);
        $this->assertEquals(5, $conf2['session']['lottery'][0]);
        $this->assertEquals(100, $conf2['session']['lottery'][1]);
    }
    
    // test the push method of adding new aggregates (adds to bottom of the stack)
    public function testAggregatorAggregatesArrayConfigAggregatePop()
    {
        $arr1 = new ArrayAggregate($this->arr1);
        $arr2 = new ArrayAggregate([
            'app' => [
                'title' => 'New Title',
            ],
            'mail' => [
                'host' => 'smtp.example.org',
                'from' => ['address' => 'luke.visinoni@gmail.com', 'name' => 'Lucas D Visinoni'],
            ],
            'cache' => [
                'default' => 'array',
            ],
            'session' => [
                'lottery' => [5, 100],
            ],
        ]);
        $config = new ConfigAggregator;
        
        // first add the main config values and test their values...
        $config->push($arr1);
        $conf1 = $config->toArray();
        $this->assertEquals('Test title', $conf1['app']['title']);
        $this->assertEquals('smtp.mailgun.org', $conf1['mail']['host']);
        $this->assertEquals('lvisinoni@denizengarden.com', $conf1['mail']['from']['address']);
        $this->assertEquals('Luke Visinoni', $conf1['mail']['from']['name']);
        $this->assertEquals('file', $conf1['cache']['default']);
        $this->assertEquals(2, $conf1['session']['lottery'][0]);
        $this->assertEquals(100, $conf1['session']['lottery'][1]);
        
        // now add an additional array aggregate and test that values were overwritten...
        $config->push($arr2);
        $conf2 = $config->toArray();
        $this->assertEquals('New Title', $conf2['app']['title']);
        $this->assertEquals('smtp.example.org', $conf2['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf2['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf2['mail']['from']['name']);
        $this->assertEquals('array', $conf2['cache']['default']);
        $this->assertEquals(5, $conf2['session']['lottery'][0]);
        $this->assertEquals(100, $conf2['session']['lottery'][1]);
        
        // now pop the most recent array off and check that values go back to what they were
        $config->pop();
        $conf3 = $config->toArray();
        $this->assertEquals('Test title', $conf3['app']['title']);
        $this->assertEquals('smtp.mailgun.org', $conf3['mail']['host']);
        $this->assertEquals('lvisinoni@denizengarden.com', $conf3['mail']['from']['address']);
        $this->assertEquals('Luke Visinoni', $conf3['mail']['from']['name']);
        $this->assertEquals('file', $conf3['cache']['default']);
        $this->assertEquals(2, $conf3['session']['lottery'][0]);
        $this->assertEquals(100, $conf3['session']['lottery'][1]);
    }
    
    // test the push method of adding new aggregates (adds to bottom of the stack)
    public function testAggregatorAggregatesArrayConfigAggregateShift()
    {
        $arr1 = new ArrayAggregate($this->arr1);
        $arr2 = new ArrayAggregate([
            'app' => [
                'title' => 'New Title',
            ],
            'mail' => [
                'host' => 'smtp.example.org',
                'from' => ['address' => 'luke.visinoni@gmail.com', 'name' => 'Lucas D Visinoni'],
            ],
            'cache' => [
                'default' => 'array',
            ],
            'session' => [
                'lottery' => [5, 100],
            ],
        ]);
        $config = new ConfigAggregator;
        
        // first add the main config values and test their values...
        $config->push($arr1);
        $conf1 = $config->toArray();
        $this->assertEquals('Test title', $conf1['app']['title']);
        $this->assertEquals('smtp.mailgun.org', $conf1['mail']['host']);
        $this->assertEquals('lvisinoni@denizengarden.com', $conf1['mail']['from']['address']);
        $this->assertEquals('Luke Visinoni', $conf1['mail']['from']['name']);
        $this->assertEquals('file', $conf1['cache']['default']);
        $this->assertEquals(2, $conf1['session']['lottery'][0]);
        $this->assertEquals(100, $conf1['session']['lottery'][1]);
        
        // now add an additional array aggregate and test that values were overwritten...
        $config->push($arr2);
        $conf2 = $config->toArray();
        $this->assertEquals('New Title', $conf2['app']['title']);
        $this->assertEquals('smtp.example.org', $conf2['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf2['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf2['mail']['from']['name']);
        $this->assertEquals('array', $conf2['cache']['default']);
        $this->assertEquals(5, $conf2['session']['lottery'][0]);
        $this->assertEquals(100, $conf2['session']['lottery'][1]);
        
        // now shift the oldest array off and check that values don't change, but that the original is gone
        $config->shift();
        $conf3 = $config->toArray();
        $this->assertEquals('New Title', $conf3['app']['title']);
        $this->assertEquals('smtp.example.org', $conf3['mail']['host']);
        $this->assertEquals('luke.visinoni@gmail.com', $conf3['mail']['from']['address']);
        $this->assertEquals('Lucas D Visinoni', $conf3['mail']['from']['name']);
        $this->assertEquals('array', $conf3['cache']['default']);
        $this->assertEquals(5, $conf3['session']['lottery'][0]);
        $this->assertEquals(100, $conf3['session']['lottery'][1]);
        // now check that the original array is gone...
        $this->assertArrayNotHasKey('url', $conf3['app']);
    }
    
    public function testJsonConfigAggregateAcceptsJson()
    {
        $json = new JsonAggregate('{
            "app": {"name": "Test title", "url": "http://www.example.com"},
            "session": {"lottery": [2,100]}
        }');
        $config = new ConfigAggregator;
        $config->push($json);
        $this->assertEquals(['app' => ['name' => 'Test title', 'url' => 'http://www.example.com'], 'session' => ['lottery' => [2, 100]]], $config->toArray());
    }
    
    public function testDirConfigAggregateAcceptsDir()
    {
        $dir = new DirAggregate(config_path());
        $config = $dir->toArray();
    }
}