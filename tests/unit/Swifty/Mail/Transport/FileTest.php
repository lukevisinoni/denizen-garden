<?php namespace Swifty\Mail\Transport;

use Swift_Message;
use Codeception\Util\Stub;
use Illuminate\Filesystem\Filesystem;
use Swift\Swifty\Mail\Storage\FilesystemStorage;
use Swift\Swifty\Mail\Storage\StorageException;
use Swift\Swifty\Mail\Transport\StorageTransport;

/**
 * @todo I'm not sure if it's a good idea to use the same namespace as the
 *       actual code I'm testing. Codeception generated the NS automatically
 */
class FileTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    /**
     * @var Filesystem stub
     */
    protected $fs;
    
    /**
     * @var array The filesystem stub stores its data here
     */
    protected $files;
    
    /**
     * @var array Just some test data to play with
     */
    protected $data;
    
    /**
     * @var string The directory to store test files in
     */
    protected $dir;
    
    protected function _before()
    {
        $this->data = [
            ['id' => sha1('This is the first e-mail test string'), 'email' => 'This is the first e-mail test string'],
            ['id' => sha1('This is the second e-mail test string'), 'email' => 'This is the second e-mail test string'],
        ];
        // create a fake local filesystem object
        $files = [];
        $this->fs = Stub::make('Illuminate\Filesystem\Filesystem', [
            'put' => function($id, $email) use (&$files) { $files[$id] = $email; },
            'get' => function($id) use (&$files) { return (array_key_exists($id, $files)) ? $files[$id] : null; },
            'delete' => function($id) use (&$files) { if (array_key_exists($id, $files)) unset($files[$id]); },
        ]);
        $this->files = &$files;
        $this->dir = storage_path() . "/emails/testing";
    }

    protected function _after()
    {
        $fs = new Filesystem;
        $fs->deleteDirectory($this->dir);
    }

    public function testFileTransportSendSavesMessage()
    {
        // content of e-mail is irrelevant it just has to be a string of some kind
        $subject = "This is the message that was sent.";
        $body = "This is the body of the e-mail";
        
        // create a swift message stub containing our test string
        $message = new Swift_Message($subject, $body);
        $message_string = (string) $message;
        
        // for our storage object, we'll stub the interface and store data in this array
        $data = [];
        $storage = Stub::makeEmpty('Swift\Swifty\Mail\Storage\StorageInterface', [
            'save' => function($id, $str) use (&$data) { $data[$id] = $str; },
            'get'  => function($id) use (&$data) { if (isset($data[$id])) return $data[$id]; },
        ]);
        
        // create our storage transport using the stub we just created as our storage object
        $transport = new StorageTransport($storage);
        
        // send the message
        $transport->send($message);
        
        // after message has been sent, we should see that the message was successfully stored in our storage object
        $this->assertEquals($message_string, $storage->get($message->getId()), "Storage transport should save e-mails rather than send them");
        
    }
    
    public function testLocalFileStorageCanSaveAndGetData()
    {
        // create storage object with the fake filesystem
        $storage = new FilesystemStorage($this->fs);
        
        // save the data
        $data = $this->data[0];
        $storage->save($data['id'], $data['email']);
        
        // check if we can retrieve the data
        $this->assertEquals($data['email'], $storage->get($data['id']), "Storage object should be able to save and retrieve items by their IDs");
    }
    
    public function testFileStorageCanConvertNonTextDataToText()
    {
    }
    
    public function testLocalFileStorageAllowsDirectory()
    {
        // use custom directory for storing files
        $dir1 = storage_path() . "/testing/emails1";
        $storage1 = new FilesystemStorage($this->fs, $dir1);
        $dir2 = storage_path() . "/testing/emails2";
        $storage2 = new FilesystemStorage($this->fs, $dir2);
        
        // use the same key for different values
        $data1 = $this->data[0];
        $data2 = $this->data[1];
        $storage1->save($data1['id'], $data1['email']);
        $storage2->save($data1['id'], $data2['email']);
        
        // because they were stored in different directories, the first should not have been overwritten
        $this->assertEquals($data1['email'], $storage1->get($data1['id']));
        $this->assertEquals($data2['email'], $storage2->get($data1['id']));
        $this->assertNotEquals($storage1->get($data1['id']), $storage2->get($data1['id']), "");
        
        // did the storage object store the file in the correct directory?
        $expected = [
            $dir1 . "/" . $data1['id'],
            $dir2 . "/" . $data1['id'],
        ];
        $this->assertEquals($expected, array_keys($this->files), "Ensure that storage object wrote to the correct directory/file names.");
    }
    
    public function testLocalFileStorageDoesntAllowInvalidDirname()
    {
    }
    
    public function testLocalFileStorageAllowsExtensionAndDefaultsToNone()
    {
        // create array for storage objects
        $storage = [];
        
        // create new filesystem storage using no file extension (default)
        $storage[''] = new FilesystemStorage($this->fs, storage_path());
        
        // create new filesystem storage using "txt" as the extension
        $storage['.txt'] = new FilesystemStorage($this->fs, storage_path(), "txt");
        
        // create new filesystem storage using "email.txt" as the extension
        $storage['.email.txt'] = new FilesystemStorage($this->fs, storage_path(), "email.txt");
        
        // loop through storage objects and test each one
        foreach ($storage as $ext => $s) {
        
            // save some data using the storage object
            $data = $this->data[0];
            $s->save($data['id'], $data['email']);
            
            // look in the files array to see if there was a file saved with expected extension
            $filename = storage_path() . "/" . $data['id'] . $ext;
            $this->assertTrue(array_key_exists($filename, $this->files), "Storage saves files with ext: \"" . $ext . "\"");
            $this->assertEquals($data['email'], $this->files[$filename]);
            
        }
    }
    
    public function testLocalFileStorageAllowsExtensionsWithOrWithoutStartingDot()
    {
        $data = $this->data[0];
            
        // Some valid extensions
        $valid = words(".txt email.txt file data email email.file email.file.txt .txt .email.txt");
        $valid[] = " ";
        $storage = new FilesystemStorage($this->fs);
        foreach ($valid as $ext) {
        
            $storage->setExt($ext)
                    ->save($data['id'], $data['email']);
            $filename = "/" . $data['id'];
            if (trim($ext)) $filename .= "." . trim($ext, ".");
            $this->assertTrue(array_key_exists($filename, $this->files), "Storage saves files with ext: \"" . $ext . "\"");
        
        }
    }
    
    public function testLocalFileStorageWillThrowExceptionIfInvalidExtension()
    {
        \PHPUnit_Framework_TestCase::setExpectedException(\Swift\Swifty\Mail\Storage\StorageException::class);
        $storage = new FilesystemStorage($this->fs);
        $storage->setExt("$$$");
    }
    
    public function testLocalFileStorageCanDelete()
    {
        $data = $this->data[0];
        
        // initialize storage
        $dir = storage_path() . "/emails";
        $storage = new FilesystemStorage($this->fs, $dir, "txt");
        
        // there should be no files yet
        $this->assertTrue(empty($this->files));
        
        // save a file
        $storage->save($data['id'], $data['email']);
        
        // make sure it saved
        $this->assertEquals(1, count($this->files));
        $this->assertEquals($storage->get($data['id']), $data['email']);
        
        // now delete it
        $storage->delete($data['id']);
        
        // and there should be no files now
        $this->assertEquals(0, count($this->files));
        $this->assertNull($storage->get($data['id']));
    }
    
    /*
    public function testLocalFileStorageCanClearAll()
    {
        $dir = storage_path() . "/emails";
        $storage = new FilesystemStorage($this->fs, $dir, "txt");
        
        // there should be no files yet
        $this->assertTrue(empty($this->files));
        
        // save some files
        $storage->save($this->data[0]['id'], $this->data[0]['email']);
        $storage->save($this->data[1]['id'], $this->data[1]['email']);
        
        // create an "expected" array to check against and add some files
        $expected = [];
        $expected[storage_path() . '/emails/' . sha1('foo') . ".json"] = "foo";
        $expected[storage_path() . '/emails/' . sha1('bar') . ".json"] = "bar";
        
        // also add some files in subdirectory
        $expected[storage_path() . '/emails/archive/' . sha1('baz') . ".txt"] = "baz";
        $expected[storage_path() . '/emails/archive/' . sha1('bat') . ".txt"] = "bat";
        
        // also add some files in parent directory
        $expected[storage_path() . '/' . sha1('baz') . ".txt"] = "baz";
        $expected[storage_path() . '/' . sha1('bat') . ".txt"] = "bat";
        
        // make sure there are only two files in storage
        $this->assertEquals(2, count($this->files));
        
        // now add all the expected files to the files array
        $this->files = array_merge($expected, $this->files);
        
        // now there should be 8 files total
        $this->assertEquals(8, count($this->files));
        
        // now clear the txt files only
        $storage->clear();
        
        // now there should be 6 files because the two text email were deleted
        $this->assertEquals(6, count($this->files));
    }
    */
    
    public function testLocalFileStorageCanClearAll()
    {
        $dir = $this->dir;
        $fs = new Filesystem();
        $storage = new FilesystemStorage($fs, $dir, "txt");
        
        // save some files
        $storage->save($this->data[0]['id'], $this->data[0]['id']);
        $storage->save($this->data[1]['id'], $this->data[1]['id']);
        
        // did they save?
        $this->assertTrue(file_exists($dir . "/{$this->data[0]['id']}.txt"));
        $this->assertTrue(file_exists($dir . "/{$this->data[1]['id']}.txt"));
        
        // now save some files with different extension
        $fs->put($dir . "/foo.json", json_encode($this->data));
        $fs->put($dir . "/bar.text", "This is a test file".PHP_EOL);
        
        // did they save?
        $this->assertTrue(file_exists($dir . "/foo.json"));
        $this->assertTrue(file_exists($dir . "/bar.text"));
        
        // now clear emails
        $storage->clear();
        
        // did they clear?
        $this->assertFalse(file_exists($dir . "/{$this->data[0]['id']}.txt"));
        $this->assertFalse(file_exists($dir . "/{$this->data[1]['id']}.txt"));
        
        // did they leave the unrelated files?
        $this->assertTrue(file_exists($dir . "/foo.json"));
        $this->assertTrue(file_exists($dir . "/bar.text"));
    }
    
}