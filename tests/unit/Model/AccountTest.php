<?php namespace Model;

use Illuminate\Filesystem\Filesystem;
use Swift\Account;

class AccountTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        $this->deleteExampleHomeDirectory();
    }

    protected function deleteExampleHomeDirectory()
    {
        $file = new Filesystem;
        if ($file->isDirectory($dir = cust_path(null, "example"))) {
            $file->deleteDirectory($dir);
        }
    }

    // tests
    public function testAccountCreationCreatesHomeDirectory()
    {
        $account = new Account(['name' => "Example Account", 'short' => "example"]);
        $account->save();
    }
}
