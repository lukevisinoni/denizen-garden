<?php
namespace Model;

use Swift\User;
use Illuminate\Support\Facades\Facade;

class UserTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testUserSaveThrowsExceptionIfNameIsEmpty()
    {
        \PHPUnit_Framework_TestCase::setExpectedException(\Illuminate\Database\QueryException::class);
        $emanon = factory(\Swift\User::class)->make(['name' => null]);
        $emanon->save();
    }
    
    public function testUserSaveThrowsExceptionIfEmailIsEmpty()
    {
        \PHPUnit_Framework_TestCase::setExpectedException(\Illuminate\Database\QueryException::class);
        $emanon = factory(\Swift\User::class)->make(['email' => null]);
        $emanon->save();
    }
    
    public function testUserSaveThrowsExceptionIfPasswordIsEmpty()
    {
        \PHPUnit_Framework_TestCase::setExpectedException(\Illuminate\Database\QueryException::class);
        $emanon = factory(\Swift\User::class)->make(['password' => null]);
        $emanon->save();
    }
    
    public function testUserSaveThrowsExceptionIfEmailAlreadyExists()
    {
        \PHPUnit_Framework_TestCase::setExpectedException(\Illuminate\Database\QueryException::class);
        $emanon = factory(\Swift\User::class)->make(['email' => 'luke.visinoni@swiftpropertymanager.com']);
        $emanon->save();
        $emanon = factory(\Swift\User::class)->make(['email' => 'luke.visinoni@swiftpropertymanager.com']);
        $emanon->save();
    }
    
}