-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2016 at 08:29 AM
-- Server version: 5.6.20
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `denizengarden`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `short` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `short`, `website`, `deleted_at`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Luke\'s Property Management', 'luke', NULL, NULL, '2016-04-17 07:30:46', '2016-04-17 07:30:46', 'active'),
(2, 'North Valley Property Management', 'nvpm', NULL, NULL, '2016-04-17 07:30:46', '2016-04-17 07:30:46', 'active'),
(3, 'Reliable Property Management', 'reliable', NULL, NULL, '2016-04-17 07:30:46', '2016-04-17 07:30:46', 'active'),
(4, 'Ponderosa Property Management', 'ponderosa', NULL, NULL, '2016-04-17 07:30:46', '2016-04-17 07:30:46', 'active'),
(5, 'Sporer, Baumbach and Leuschke', 'paucekorg', 'http://paucek.org', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(6, 'Cassin, Walker and Schamberger', 'rutherfordcom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(7, 'Stark, Koch and Mante', 'raynorbiz', 'http://raynor.biz', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(8, 'Ernser-Keeling', 'wilkinsoncom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(9, 'Hirthe-Kris', 'kingorg', 'http://king.org', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(10, 'Gerhold and Sons', 'kulascom', 'http://kulas.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(11, 'Rolfson Group', 'schroedercom', 'http://schroeder.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(12, 'Nienow, Thiel and Lesch', 'oberbrunnercom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(13, 'Nikolaus LLC', 'hoppenet', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(14, 'Pfeffer PLC', 'nicolascom', 'http://nicolas.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(15, 'Erdman, Veum and Mitchell', 'dickiorg', 'http://dicki.org', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(16, 'Kovacek, Greenholt and Quitzon', 'volkmannet', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(17, 'Haag, Rowe and Barrows', 'reillycom', 'http://reilly.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(18, 'Steuber PLC', 'nikolausnet', 'http://nikolaus.net', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(19, 'Little-Conn', 'vandervortorg', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(20, 'Bashirian, Kuhic and Simonis', 'beckercom', 'http://becker.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(21, 'Bahringer, Heller and Kerluke', 'herzogcom', 'http://herzog.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(22, 'Reinger-Bogan', 'leschbiz', 'http://lesch.biz', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(23, 'Koelpin LLC', 'bartellcom', 'http://bartell.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(24, 'Witting and Sons', 'dachcom', 'http://dach.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(25, 'Mayert-Turner', 'kovacekcom', 'http://kovacek.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(26, 'Borer LLC', 'caspercom', 'http://casper.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(27, 'Schuppe-Wehner', 'hanebiz', 'http://hane.biz', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(28, 'Baumbach-Bosco', 'lubowitzcom', 'http://lubowitz.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(29, 'O\'Reilly, Kozey and Paucek', 'stokesnet', 'http://stokes.net', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(30, 'Schuppe PLC', 'funknet', 'http://funk.net', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(31, 'McGlynn PLC', 'fritschorg', 'http://fritsch.org', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(32, 'Bayer-Kemmer', 'hillscom', 'http://hills.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(33, 'Funk-Nolan', 'feeneycom', 'http://feeney.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(34, 'Upton Inc', 'hartmanninfo', 'http://hartmann.info', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(35, 'Labadie PLC', 'ziemecom', 'http://zieme.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(36, 'Lubowitz, Kuphal and Metz', 'abernathycom', 'http://abernathy.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(37, 'Dare, Bergnaum and Nolan', 'uptonnet', 'http://upton.net', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(38, 'Schoen, Grant and Langosh', 'towneinfo', 'http://towne.info', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(39, 'Lebsack and Sons', 'brekkecom', 'http://brekke.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(40, 'Stark, Mante and Lockman', 'turcottecom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(41, 'White, Adams and Feeney', 'kubinfo', 'http://kub.info', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(42, 'Reilly Inc', 'boyercom', 'http://boyer.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(43, 'Legros-Langworth', 'schimmelcom', 'http://schimmel.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(44, 'Yost, Huel and Erdman', 'hoegercom', 'http://hoeger.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(45, 'Cruickshank, Oberbrunner and Smith', 'smithcom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(46, 'Hartmann, Bernhard and Bergstrom', 'medhurstcom', 'http://medhurst.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(47, 'Wolff LLC', 'cronacom', 'http://crona.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(48, 'Weimann-Zboncak', 'heathcotecom', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(49, 'Goldner, Walter and Kuhlman', 'mcculloughcom', 'http://mccullough.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(50, 'Strosin-Abbott', 'cruickshankbiz', NULL, NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(51, 'Swift-Senger', 'feestcom', 'http://feest.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(52, 'Hermiston, Luettgen and Reichert', 'morarcom', 'http://morar.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(53, 'Ritchie, Bayer and Schroeder', 'wildermanorg', 'http://wilderman.org', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active'),
(54, 'Altenwerth, Watsica and Gerhold', 'vandervortcom', 'http://vandervort.com', NULL, '2016-04-17 07:30:47', '2016-04-17 07:30:47', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_short` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `param` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `account_short`, `key`, `param`, `value`, `created_at`, `updated_at`) VALUES
(4, 'luke', 'mail', 'from.name', 'Lucas D Visinoni', '2016-04-19 07:36:46', '2016-04-19 07:36:46'),
(5, 'luke', 'mail', 'from.email', 'Luke.Visinoni@gmail.com', '2016-04-19 07:37:03', '2016-04-19 07:37:03');

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `account_id`, `name`, `is_primary`, `created_at`, `updated_at`) VALUES
(1, 1, 'luke.denizengarden.dev', 1, '2016-04-17 07:30:46', '2016-04-17 07:30:46'),
(2, 2, 'nvpm.denizengarden.dev', 1, '2016-04-17 07:30:46', '2016-04-17 07:30:46'),
(3, 3, 'reliable.denizengarden.dev', 1, '2016-04-17 07:30:46', '2016-04-17 07:30:46'),
(4, 4, 'ponderosa.denizengarden.dev', 1, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(5, 5, 'paucekorg.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(6, 6, 'rutherfordcom.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(7, 7, 'gleason.com', 1, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(8, 7, 'schroeder.org', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(9, 7, 'raynorbiz.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(10, 8, 'wilkinsoncom.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(11, 9, 'kingorg.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(12, 10, 'kulascom.denizengarden.dev', 0, '2016-04-17 07:30:47', '2016-04-17 07:30:47'),
(13, 11, 'schroedercom.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(14, 12, 'altenwerth.net', 1, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(15, 12, 'nitzsche.com', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(16, 12, 'oberbrunnercom.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(17, 13, 'hoppenet.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(18, 14, 'nicolascom.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(19, 15, 'dickiorg.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(20, 16, 'volkmannet.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(21, 17, 'reillycom.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(22, 18, 'halvorson.biz', 1, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(23, 18, 'nikolausnet.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(24, 19, 'wehner.com', 1, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(25, 19, 'vandervortorg.denizengarden.dev', 0, '2016-04-17 07:30:48', '2016-04-17 07:30:48'),
(26, 20, 'beckercom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(27, 21, 'herzogcom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(28, 22, 'streich.info', 1, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(29, 22, 'leschbiz.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(30, 23, 'bartellcom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(31, 24, 'dachcom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(32, 25, 'kovacekcom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(33, 26, 'jakubowski.info', 1, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(34, 26, 'caspercom.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(35, 27, 'rogahn.com', 1, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(36, 27, 'breitenberg.org', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(37, 27, 'hanebiz.denizengarden.dev', 0, '2016-04-17 07:30:49', '2016-04-17 07:30:49'),
(38, 28, 'lubowitzcom.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(39, 29, 'stokesnet.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(40, 30, 'funknet.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(41, 31, 'fritschorg.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(42, 32, 'brown.com', 1, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(43, 32, 'veum.info', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(44, 32, 'hillscom.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(45, 33, 'feeneycom.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(46, 34, 'hartmanninfo.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(47, 35, 'ziemecom.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(48, 36, 'abernathycom.denizengarden.dev', 0, '2016-04-17 07:30:50', '2016-04-17 07:30:50'),
(49, 37, 'uptonnet.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(50, 38, 'towneinfo.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(51, 39, 'hyatt.net', 1, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(52, 39, 'brekkecom.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(53, 40, 'muller.info', 1, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(54, 40, 'schumm.com', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(55, 40, 'bashirian.com', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(56, 40, 'turcottecom.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(57, 41, 'kubinfo.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(58, 42, 'hyatt.com', 1, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(59, 42, 'boyercom.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(60, 43, 'schimmelcom.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(61, 44, 'cole.biz', 1, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(62, 44, 'hoegercom.denizengarden.dev', 0, '2016-04-17 07:30:51', '2016-04-17 07:30:51'),
(63, 45, 'smithcom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(64, 46, 'medhurstcom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(65, 47, 'cronacom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(66, 48, 'heathcotecom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(67, 49, 'cormier.com', 1, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(68, 49, 'mcculloughcom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(69, 50, 'cruickshankbiz.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(70, 51, 'feestcom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(71, 52, 'morarcom.denizengarden.dev', 0, '2016-04-17 07:30:52', '2016-04-17 07:30:52'),
(72, 53, 'wildermanorg.denizengarden.dev', 0, '2016-04-17 07:30:53', '2016-04-17 07:30:53'),
(73, 54, 'vandervortcom.denizengarden.dev', 0, '2016-04-17 07:30:53', '2016-04-17 07:30:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_20_050606_create_accounts_table', 1),
('2016_03_20_221027_create_domains_table', 1),
('2016_04_17_000715_add_account_id_to_users_table', 1),
('2016_04_18_193745_create_configs_table', 2),
('2016_04_20_120937_add_status_to_accounts_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('lvisinoni@denizengarden.com', '414e1e5a72eef579857b3dc1aee9cf6aafff3d7603a37828f102da5084d4d71d', '2016-04-20 10:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `account_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `account_id`) VALUES
(1, 'Luke Visinoni', 'lvisinoni@denizengarden.com', '$2y$10$/GgCLK9T1gmHjtnKM6JzCeQNEOEV36AAvRi4MPq1zTXQVq87wBHs6', '2M90Kr1ccjRs77cp2uB9z64Gat8ZubAnhZ7yUbyMiXufPluCnK1nNOmaNUWO', '2016-04-17 07:30:46', '2016-04-20 05:21:47', NULL, 1),
(2, 'Norbert Schmitt', 'norbert.schmitt@nvpm.denizengarden.dev', '$2y$10$yQQz/omJYxyZ/nSTDM8uee.v4VkdphTx9joCaQM2ZokFmtE51pwaC', 'ZEGpmxImrADSYgK1BHGkbacB9g83oYV4pRQGhVejvoBXrBBx4LawnmLnmO1b', '2016-04-17 07:30:46', '2016-04-17 07:30:46', NULL, 2),
(3, 'Mathilde Nienow', 'mathilde.nienow@reliable.denizengarden.dev', '$2y$10$5qGdjaUq7QJDkjE/VEo0H.5Y4eHBQfC12h7zvfv7qtS6LsDrw1Cxu', '3EhauMOrJu3e3se9uzAdz476Fj3dRkTa2dhWbke2N4pKGDgrez2lLn6wN62O', '2016-04-17 07:30:46', '2016-04-17 07:30:46', NULL, 3),
(4, 'Mrs. Lora Simonis', 'mrs.lora.simonis@ponderosa.denizengarden.dev', '$2y$10$/CACmp7.JxSFpR.dIqouz.12/A7.dMSy6/yLIQCJT1fTXz33WijJC', 'e1dw9Oj7L3uCz7HtIw8tbmFvx6WWjMfpzWKrTLwrRFyLFj3dr73mFFZYSLr6', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 4),
(5, 'Mr. Lincoln Herman II', 'mr.lincoln.herman.ii@paucekorg.denizengarden.dev', '$2y$10$alyI3b4t04P2yGLZo2ZAsuXtTtssyozHxa/tHu5Tmi3HMg1oa0Xb.', 'lAs2Qvvc0BPMXqnoFqM1e3Gk5QlCs9dBXEstv8Da5Xhb0BkM8eUXAVyjNTob', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 5),
(6, 'Dr. Celine Quitzon', 'dr.celine.quitzon@rutherfordcom.denizengarden.dev', '$2y$10$kjQWdiYUPsgdSajBbnJJjeyOSX4iJ.c6BR5bEPltfi4ppsa.ekEL2', 'owiPfMoDyjKjuEGDjL7r1bxVFsHlGeTNK4HJeRD1ruJgRQqgwIqupMCHTpZf', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 6),
(7, 'Efren D\'Amore', 'efren.d.amore@raynorbiz.denizengarden.dev', '$2y$10$3lF6pvL61yoACY5UVUXBfu9YkhFPbQIMcMNJKQpRw1PcnsgnbteFu', 'mHnW5XyuSaOcSjquGfklU77qFetiz3TE2wN4Tx6jI7R8zGxuC6zAa41HhCF5', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 7),
(8, 'Dr. Malika Simonis Sr.', 'dr.malika.simonis.sr.@wilkinsoncom.denizengarden.d', '$2y$10$vE5FXii/xExX6zimAbqaWOFtF1Xzp5LJ8jgKauE13Tn2hetYE/n8G', 'IU23HqnHJ15NF8Z9uqPdRlyMeRFaP8nWNFgoMzLR2ZFM7Xc45C39npKW8rID', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 8),
(9, 'Adella Hintz', 'adella.hintz@kingorg.denizengarden.dev', '$2y$10$G487JJ6x0pc/g2r5HZKg6.LH/76i9opYX6CJxC.GwHU4xKapGIoRe', 'VDc80fk1gEVNvKFOVdTmOXX3pGmHqjSsmEbw84fwUFoKzkMr2d9DTaWcjm78', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 9),
(10, 'Riley Heller', 'riley.heller@kulascom.denizengarden.dev', '$2y$10$xHXSXulVvY6SHTrxCz8M7eFcVb2CahXEVVGw5pVSUYjDon.SS8HB6', 'NJ5DNzxVH61ArMoDa7duSV4q2F6tESVoePCywtEEEN5NAcLnSsZwDUiV679O', '2016-04-17 07:30:47', '2016-04-17 07:30:47', NULL, 10),
(11, 'Dr. Amir Runolfsdottir', 'dr.amir.runolfsdottir@schroedercom.denizengarden.d', '$2y$10$etGrQdOh2jU041QjowAFSeQxxC2qGij.oAVNhg684IllqQdyyiUkq', 'MI2bCbUIYDgZcRGaSZyqe6fv7B1C5sptnzT8E1W72zsQ6nKdJHNqcrBOePQC', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 11),
(12, 'Sarina Bins', 'sarina.bins@oberbrunnercom.denizengarden.dev', '$2y$10$bgE96WF/LOk1D4jBXxyZiO8C8kKxOIUdRc5n9h9zWXDLphpLCZWQe', 'RrmMWgHxqN1jfjrPyota7qZ2Scb3BzTBerfTrfUfI8NSPlS6HuAxqe0gOIU3', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 12),
(13, 'Buster Greenholt', 'buster.greenholt@hoppenet.denizengarden.dev', '$2y$10$spJfCrGOvHceCFzWyDehquINpktPZ0bcBxeTMDBTmv6fIzFECxNAa', 'teXIPdtO4zW3zvK6vnpazUNthql8y0GjlRnvOXFn0IFCoayli88uO1Jf8iuE', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 13),
(14, 'Kayley Hudson', 'kayley.hudson@nicolascom.denizengarden.dev', '$2y$10$F/RzeyyYyFnh8tFfyMrlheTr.1SdV9sHuba3BTLB7VzLghs29nuW6', 'jOSvyceMqq2Wefm7B7UWgnBxud4bnNCo8b250spADDyExwOU3KhxSlTpbPxC', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 14),
(15, 'Nels Schroeder', 'nels.schroeder@dickiorg.denizengarden.dev', '$2y$10$O4uGBcCs90mAcfEO1E0hjuIz49eV0RCScFJj9yQ3aSb3YGEearCOG', '1r5fB6azXHT7zszowYkiotLbe5kI65u8SE58RUJJoBBgflHmQEsHbmbBRNfE', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 15),
(16, 'Ms. Hailee Parker I', 'ms.hailee.parker.i@volkmannet.denizengarden.dev', '$2y$10$ALwVyLcl2G.hJz0P2zgCtuFMFBVnfUfeUD31AUGfeZmtcLto1tn1.', 'RHh1rMIVJgdOVauPELlMV62hYWcqsVj1NdjSYhvnsrlMRCI2qCmRpBcJj16h', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 16),
(17, 'Brandyn Ebert', 'brandyn.ebert@reillycom.denizengarden.dev', '$2y$10$nABlRYluulau8DjlQGz1/uKPq.oyww7Ddt2pCnKKTRDvA9xEHudlu', 'rugIFNNHlAZHWvPeQt54w6AD2shXlDaWoD5sUie0OQJo1lNenlZRJ4Vso5yj', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 17),
(18, 'Dr. Sydnie Koss', 'dr.sydnie.koss@nikolausnet.denizengarden.dev', '$2y$10$0kMO26aBgH7/lQ0DHXriD.E8/9GB5SsD0oYMxfEwWkmv4cb3a7zKm', 'VbRIG4C4Vya8pL12xKV7ONvesnRJLEapszFPTT4fNybp1CzxLMgrAYPg1sVu', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 18),
(19, 'Camron Koss', 'camron.koss@vandervortorg.denizengarden.dev', '$2y$10$xT5c/VyCJ3mB.4JmA0D41.Y6oZ6fujBO2NRmggWzbGTA02pOZpKbO', '42r7muk6jjwYHDtiGezfzEpgOQvGjnsD8xldjkTWqkvoBvXRTYgniZ9zRf0k', '2016-04-17 07:30:48', '2016-04-17 07:30:48', NULL, 19),
(20, 'Lulu Becker', 'lulu.becker@beckercom.denizengarden.dev', '$2y$10$L73NF85gHS9RkO/R9PlDYeNMmppODszk9Fs3Ij3AZBf/RbeSNb2Mm', '76q6LnuYWcGt0nVNZHFIAK7FIHXxx48dPFq9iQsOpdGWWeJXfBY2b7uKCnId', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 20),
(21, 'Prof. Letitia Ortiz IV', 'prof.letitia.ortiz.iv@herzogcom.denizengarden.dev', '$2y$10$nqxsM23xbc/d0jBmO57EBejc870Xv2VtVhnW6NMqWIAq3xpFKih.K', 'fXyPyhpfzJBKhJSetdCdyKyywAShBKP4F0o2zhhlfdleJaKAbtwD7sBh5fLM', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 21),
(22, 'Ms. Gretchen Nitzsche', 'ms.gretchen.nitzsche@leschbiz.denizengarden.dev', '$2y$10$zknOS549K.0GeBXfVFn0EuhKYaaMuQ90DD8RUmwf6pn/szAmMTDfu', 'kbgAblQwH7fi8w2PYrUztuxymxtYDB0q4qlgZj01MrnQdxxIzeCqYKHgBaAP', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 22),
(23, 'Joan Emmerich', 'joan.emmerich@bartellcom.denizengarden.dev', '$2y$10$5PoMJmw10kAwYc3p9oQwnuVQSd7qzGY9wwz05h1sVOwJK2WtsaoAu', 'wxgci4QgduhPEAbk8XRFTYgqWcwOBPz9ZYHc0i9QYSgNa4JpMLY38TqASxYh', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 23),
(24, 'Jody Hegmann', 'jody.hegmann@dachcom.denizengarden.dev', '$2y$10$wqi4n7iGEM9OZxRyESxL/.rHUdCHFCUqnuvf5f1tvyKX1bL/oYSNO', 'skNtb7r22rCW2aZcFEnzoYB5NQUpRcA7H1vhSHc3NaU6XSjYKkYHlRBFuQmq', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 24),
(25, 'Jonathan Stokes', 'jonathan.stokes@kovacekcom.denizengarden.dev', '$2y$10$GlnV3JuFeqA7dB7bkZly2OXJurBnBwYmC3UiIm532keMcKlkBdu8G', 'Su0nmqxsd426Fxmp1G6SwxCU6rLlVHjGNmjMqzm9jO1LlTyTqD7Z5gNBhVjZ', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 25),
(26, 'Mrs. Lacy Kilback', 'mrs.lacy.kilback@caspercom.denizengarden.dev', '$2y$10$4Wm955SndzlwLjm83Kav/O.DAqXjVlGZYll6bGEWdZTpYXFgM4KtO', 'qzM1Zo1VnGElwM2swtRBce7Gj9FKMZnrXldrwkZP73RUesS1KtDowjJjePvh', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 26),
(27, 'Mozelle Beahan', 'mozelle.beahan@hanebiz.denizengarden.dev', '$2y$10$ZkWwdXRSb6gUWK7IOYuMxeuWlKBKRpjYNwe9x7eqW1/eWpv/KJisi', 'DbmnIBtiUsjIXwh41z2fwFFcS89cs524SdWQmZqG8asYxE8G2e8TCkk2LO2K', '2016-04-17 07:30:49', '2016-04-17 07:30:49', NULL, 27),
(28, 'Darrel Steuber', 'darrel.steuber@lubowitzcom.denizengarden.dev', '$2y$10$ReEh6qa7yQQUbZjlqoka4ODauowHd5ghyIK.YvJJtgYO56SNJ93Ym', 'ev7EpzzjGy2dwZFw8zYMVclbFcdWGNiMjX5qwMnN3CbIn6bclJsKNsezioTq', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 28),
(29, 'Margarita Walker', 'margarita.walker@stokesnet.denizengarden.dev', '$2y$10$68ofjCKfdqXBiI/g8F.u0uPQPcGNWYPxhFD1byydJzitnsXOa5xY6', 'G4hSb07Ex10Q97WOrLTlOfC8rW8wZ526bTOWGo5OUUF9QQm72il2sx3wrr5V', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 29),
(30, 'Julio Gusikowski', 'julio.gusikowski@funknet.denizengarden.dev', '$2y$10$7GfwJMoDcbpenBd8Y1ZAJupFI2.tEbjv5e8NtULkkBohFUlZYXnQm', 'nuZOoDPvGOshpRxSUOxwYgl4JM3e53JSB2IG99jgSazToldEE4IBV7KIXkwm', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 30),
(31, 'Evans Dooley', 'evans.dooley@fritschorg.denizengarden.dev', '$2y$10$tiOIOmFDbqwsXmIXb1a.AuEun/f6ueCvfccyNcZc/KJflmZW.dN56', 'KuLa3UR0oRDw2785g9l6dcDv8TXorLKg11lsdhvc7PKIozRL6c824hBBAdB6', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 31),
(32, 'Emilie Lueilwitz Jr.', 'emilie.lueilwitz.jr.@hillscom.denizengarden.dev', '$2y$10$UTrfMZylAmZ9ZaY6hF.1QepqJvaafiDS0Ugo3a8vsRsNe0yordwOa', 'rrornwNIS5BoRdXf89Xtr3l5ZZEsbGFSm5hpXmeKIGb6yjVyTSaPUyBj5W0c', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 32),
(33, 'Stacy Hartmann', 'stacy.hartmann@feeneycom.denizengarden.dev', '$2y$10$HcpOxhZ6luBpUvF5DcIFbur/Jy7R2garR/QP4K5GMzfMd9zpwPpmK', 'BQid4vhMCDLF99QizEZvgPj7xzQQfUtfhZznScOC7R64DlEFMWOefbrAzhQO', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 33),
(34, 'Megane Goldner', 'megane.goldner@hartmanninfo.denizengarden.dev', '$2y$10$tOPXWEchJkJhQF/RP.9TuuPvhIGQKkoIPDwasLPNX6G3PzPiRbR06', '3w7FDgYfFSOZtKFl7wrZbo6JHm4H4chQVUnV0qr8olOtWkFDw9Afvt1sljyS', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 34),
(35, 'Adolfo Heller', 'adolfo.heller@ziemecom.denizengarden.dev', '$2y$10$Wmd7mUWDq1VoWmv/kvJ8luGwZrgAA6/rO6xYGaKzpfj04r4Ygwz2S', 'kDbir0xcYKXLKf36IkwTUwC8k0Z9W7YTbpYCtyzfvf1vZlOSM0QKebRtCRMR', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 35),
(36, 'Karl Strosin II', 'karl.strosin.ii@abernathycom.denizengarden.dev', '$2y$10$ItvYEnP6XO/O0U7IP2JLmuxHQqPXHgtEOByTgR5fd.VWRRKkWsiUW', 'sZYu6SbyiWOxs5dlAjpO4SeWGFYzCZ9bsknk5oRE2rsgJivOh41VsXVvd6mM', '2016-04-17 07:30:50', '2016-04-17 07:30:50', NULL, 36),
(37, 'Braulio Herman DDS', 'braulio.herman.dds@uptonnet.denizengarden.dev', '$2y$10$n9/rFrL/d1dEKW5UY.d4vOYFwVHwGTQ5bqC0Gs4C8qDbBOsIPeiVO', 'eEISFbi97MQpPMMctDb0mTj91cImMLNvQh94EqYMbeJDHKRg79C0rSK4MIqD', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 37),
(38, 'Prof. Lexie Collier PhD', 'prof.lexie.collier.phd@towneinfo.denizengarden.dev', '$2y$10$DzD0JktEL.1nXFksWCtnvOztI3Ud3fLyT51F091e2YedsCXaEINm.', '5AQNIQIhw6QkLmz3nLCkIrUZvQgnxjXeHJ3VTOnqKsy1tqfdDhilQP0OWoNR', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 38),
(39, 'Bette Rodriguez', 'bette.rodriguez@brekkecom.denizengarden.dev', '$2y$10$Xf1.qcwu4/HcAGJoXPTcHeKLs7actBDcoTvLxqD.e47XR0iBA/mle', 'MXUhJTwjiZu04kZatkQkV0VtHyZrmdspBFJgqo3eSnW9JQ1JMt04unsqX5ri', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 39),
(40, 'Mrs. Bettie Skiles', 'mrs.bettie.skiles@turcottecom.denizengarden.dev', '$2y$10$Jzejqk4l1hevuNyq/gC8vuvJnDACTDEM6XjDk5B3/RmZH6y0Wofwa', 'UGmu21yjThdIyjyhyxEKOJvEO7WBqPX2cJ5UteTsTEaG1yj9KRRYija8pOvc', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 40),
(41, 'Michelle Goodwin', 'michelle.goodwin@kubinfo.denizengarden.dev', '$2y$10$vkoC9OPReu338Y1H3fYYaumN9hKSdbdRwf9gZPk9bcfbSZdWINg9.', '3cY8Q6I15Mdr1dEEc03raVKlhhmqXgwUjywV6bOfoHjlaAU8x9iGD4YzphZ2', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 41),
(42, 'Eleanora Kuhn PhD', 'eleanora.kuhn.phd@boyercom.denizengarden.dev', '$2y$10$SVQCH4tmqvd.UWxJHNbVVu8n/6cMsuoEbjudme/jjvdWjrdMWPkbm', '6URTcv6oqXOIylskjca5M3u6jslczkRRGyVEXSkG3qpxWl7FKXJx43N3BvfK', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 42),
(43, 'Eunice Trantow', 'eunice.trantow@schimmelcom.denizengarden.dev', '$2y$10$bWodcqM5lwgGg/8uDuqUSuy2.KKtdEsJTWHOhtmu3c8KxNReK4MwG', '9PfzmlkEsStn6AyuQmaG3p569Z2UT4vuYx1Ct2weS2PhFJkKDEDSth7OlT4K', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 43),
(44, 'Nolan Ledner', 'nolan.ledner@hoegercom.denizengarden.dev', '$2y$10$/m3VJo0yT1T1x9fw3pJV2unz41ce7Qf9vnFRChsLdTbcAZDMmwVrW', 'lSbCDkcaFE4mSoCuc7tWeFyx3LEvMGrt0lBmFHfzp3GeLUpfbwSHI4600F01', '2016-04-17 07:30:51', '2016-04-17 07:30:51', NULL, 44),
(45, 'Verner Runolfsson', 'verner.runolfsson@smithcom.denizengarden.dev', '$2y$10$BjeeEKq1jdkqUo7s92jvteqNAN2mcmycSnjZbUfzzNvmC7aP72Sz6', 'SskVdkM3a1E1Okpfnng6CErpp8kxnhbg6JzN4onwnVlwuydjoDLYNiuPyM0J', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 45),
(46, 'Kelsie Lind', 'kelsie.lind@medhurstcom.denizengarden.dev', '$2y$10$Mp0fBOMXK9nr5/I5tFldjOzkPjU.gP0G53Zft/JrIzOrv6OkHeE0.', 'lDcs1aZ9OiHTSfOkuSNDYW3VWeuJgvH9zGGPhBW8phWbRxpJ9nUo13UNqpsa', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 46),
(47, 'Ms. Tania Rosenbaum Jr.', 'ms.tania.rosenbaum.jr.@cronacom.denizengarden.dev', '$2y$10$uv0PCfBoRspkto9UsrA6RuCssdF2pX15ZSj9xitgEboW5bj2AzpjO', 'P2R00Xgbew0EPvhSTvOfCkfkkOryl7Jhz9oBtJpVKZpD6Eu5pISZpozLjVwE', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 47),
(48, 'Dr. Xzavier Bergnaum III', 'dr.xzavier.bergnaum.iii@heathcotecom.denizengarden', '$2y$10$Qo/Px1vKv4.dsHqaE7q55.7AfSwrm.WBXuRhnG7SshAkh9ZMC1rMK', 'AV11nTHrbnYkYLFgSGpW7uVVqXIRfTQfqWEKf4dzUTWhvy5AgxePx1NEPHzr', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 48),
(49, 'Prof. Erick Gibson', 'prof.erick.gibson@mcculloughcom.denizengarden.dev', '$2y$10$HuJg0Ms5LPbGfG1E6xm/yOL17NElTE0R8eG5dwxM9TzXPymdI4KTO', 'aXBq3pOVwhptJuDd8GnDoUYe4QqwBlkTwxdK9Y6dnC6gQUdLbzUkW4Ztj7Kf', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 49),
(50, 'Mathew Jacobi Jr.', 'mathew.jacobi.jr.@cruickshankbiz.denizengarden.dev', '$2y$10$vlXZLcHb4rWKOzw8ulHA5.cNY3lVrYXmH0.OrGWF5cLzY.ccgqTFa', 'zIieg7ASmKYLDnXxreTvFNoFebo8mksrZH5NhcTUUnhAk8AmGDDVR3rYjBCR', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 50),
(51, 'Albina Davis', 'albina.davis@feestcom.denizengarden.dev', '$2y$10$0EMcUxXKhdh.kqoEAJllhOm52hlmSZe.ikM6J1Dsz3NnRxK/nNTCa', 'wGNvEmL0IpyTzefVAxNTxR6RyZhr29Ig3WdbcYeTDk34p25t4tOuvMC6UEuG', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 51),
(52, 'Harrison Spencer', 'harrison.spencer@morarcom.denizengarden.dev', '$2y$10$pXwWPgmYa.FsbG4caCH1k.Z8PN6sZc6uFvy7nQbCOe7Vx7l8434SC', 'WG2plzxA2XEya1MRTTPwx7JYSQJyjPaDeJ2tSUxpJrA4mti1MkxSGalOiD58', '2016-04-17 07:30:52', '2016-04-17 07:30:52', NULL, 52),
(53, 'Shirley Miller', 'shirley.miller@wildermanorg.denizengarden.dev', '$2y$10$4YN.XWlQRN.FF/U0DFGttuL7Gimd.FOeGV9QrGlJ8DHV6dqCpeKBe', 'aeJAP1i51lnJtTpFEUOHIrxiS6RpXoCEopj8MjONAt2Jd58qUdzbtkCE0Mke', '2016-04-17 07:30:53', '2016-04-17 07:30:53', NULL, 53),
(54, 'Isai Weissnat', 'isai.weissnat@vandervortcom.denizengarden.dev', '$2y$10$2svUTXxpNSjpA97JldNUc.6g5uO5.4gXtAhDhQSfctncHmZiYvEaC', 'T5QmOm7jwfvkoOuDrhgrWyJR3DZUQRF0vsTkEzTdTf0PJ0PVXngch4Xkg9Sx', '2016-04-17 07:30:53', '2016-04-17 07:30:53', NULL, 54);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accounts_short_unique` (`short`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `configs_account_short_key_param_unique` (`account_short`,`key`,`param`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `domains_name_unique` (`name`),
  ADD KEY `domains_account_id_foreign` (`account_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_account_id_foreign` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `configs`
--
ALTER TABLE `configs`
  ADD CONSTRAINT `configs_account_short_foreign` FOREIGN KEY (`account_short`) REFERENCES `accounts` (`short`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `domains`
--
ALTER TABLE `domains`
  ADD CONSTRAINT `domains_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
