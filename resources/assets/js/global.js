/**
 * Namespace Swifty
 * Assumes jQuery is present
 */
if (typeof(Swifty) == 'undefined') {
    // namespace
    var Swifty = {};
    Swifty.$ = jQuery;
}

(function($) {

    $(function(){
    
        /**
         * @todo This works but it causes the element to "jump" from its original
         * position on page-load. I would much prefer a plain css solution.
         */
        $(".valign-to-parent").each(function() {
            var ph = $(this).parent().height()
            var ch = $(this).height();
            var diff = ph - ch;
            var topmarg = diff / 2 + "px";
            $(this).css('margin-top', topmarg);
        });
    
    });
    
})(Swifty.$);