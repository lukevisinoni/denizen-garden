import React, { Component, PropTypes } from 'react'
import { reduxForm } from 'redux-form'

export const fields = [ 'name', 'website', 'email' ]

const validate = values => {
    const errors = {};
    if (!values.name) {
        errors.name = "Please enter an account name";
    }
    if (!values.website) {
        errors.website = "Please enter a valid website name";
    }
    if (!values.email) {
        errors.email = "Please enter a valid e-mail address";
    }
    return errors;
}

var SettingsForm = React.createClass({
    propTypes: {
        asyncValidating: PropTypes.string.isRequired,
        fields: PropTypes.object.isRequired,
        resetForm: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        submitting: PropTypes.bool.isRequired
    },

    isError: function (field) {
        return (field.touched && field.error);
    },

    isSuccess: function(field, options) {
        return options.showSuccess && (!field.active && (field.touched && field.valid));
    },

    renderSpinner: function(field, options) {
        if (this.isSpinner(field, options)) {
            return <div className="col-xs-1" style={{padding: ".375rem 0"}}><i className="fa fa-spinner fa-pulse fa-fw"/></div>;
        }
    },

    isSpinner: function(field, options) {
        return (options.asyncValidating && options.asyncValidating === field.name);
    },

    renderLabel: function(options) {
        if (options.label) {
            return <label className="form-control-label col-xs-4" htmlFor="email">{options.label}</label>;
        }
    },

    renderError: function (field) {
        if (this.isError(field)) {
            return <small className="text-help" style={{display: "block", padding: "0 5px"}}>{field.error}</small>;
        }
    },

    renderField: function(field, options) { // options = label, type, className, placeholder, asyncValidating, showSuccess
        return (
            <div className={(this.isError(field) ? ' has-danger ' : '') + (!this.isSpinner(field, options) && this.isSuccess(field, options) ? ' has-success ' : '') + (this.isSpinner(field, options) ? ' has-spinner ' : '') + ' form-group row '}>

                {this.renderLabel(options)}

                <div className=" col-xs-7">
                    <input
                        type={options.type || "text"}
                        placeholder={options.placeholder || ""}
                        className={(options.className || " ") + (this.isError(field) ? "form-control-danger" : " ") + (this.isSuccess(field, options) ? " form-control-success " : " ") + (this.isSpinner(field, options) ? " form-control-spinner " : " ") + " form-control "}
                        {...field} />
                    {this.renderError(field)}
                </div>

                {this.renderSpinner(field, options)}

            </div>
        );
    },

    renderTokenField: function () {
        var token = (!this.props.token) ? ('undefined' !== typeof(Denizen.csrfToken)) ? Denizen.csrfToken : '' : this.props.token;
        return (
            <input type="hidden" name="_token" value={token} />
        );
    },

    render: function() {
        const { asyncValidating, fields: { name, website, email }, resetForm, handleSubmit, load, submitting } = this.props;
        return (
            <form style={{padding: "1em"}} onSubmit={handleSubmit} method="POST" className={(this.props.className || ' form-horizontal')}>
                {this.renderTokenField()}
                {this.renderField(name, {label: "Account name", placeholder: "Account name", asyncValidating: asyncValidating})}
                {this.renderField(website, {label: "Website", placeholder: "Website", asyncValidating: asyncValidating})}
                {this.renderField(email, {label: "E-mail", placeholder: "E-mail", asyncValidating: asyncValidating})}
                <div className="form-group pull-right">
                    <button type="button" disabled={submitting} onClick={this.props.handleCancel} className="temporary btn btn-default">
                        Cancel
                    </button>
                    <button type="submit" disabled={submitting} className="temporary btn btn-primary">
                        {submitting ? <i className="fa fa-spinner fa-pulse fa-fw" /> : <i className="fa fa-user-plus" />} Save
                    </button>
                </div>
                {this.props.children}
            </form>
        )
    }
});

export default reduxForm({
    form: 'settings',
    fields,
    validate
})(SettingsForm);