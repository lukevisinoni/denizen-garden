import React from 'react';

import { render } from 'react-dom';

// Import css
import css from './styles/style.styl';

// Import Components
import App from './components/App';
import Settings from './components/Settings';
// import PhotoGrid from './components/PhotoGrid';

// import react router deps
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import store, { history } from './store';

const router = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/account" component={App}>
                <IndexRoute component={Settings}></IndexRoute>
                <Route path="/account/settings" component={Settings} />
            </Route>
        </Router>
    </Provider>
)

render(router, document.getElementById('account'));
