import React from 'react'
import SettingsForm from '../forms/SettingsForm.js'

const Settings = React.createClass({

    saveSettings: function(data) {
        console.log(data);
    },

    getInitialValues: function() {

        return new Promise(function(resolve, reject) {
            // @todo place this in Denizen object
            var request = {headers: {
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }};
            fetch('/ajax/account/settings', request)
                .then(response => {
                    if (response.ok) {
                        return response.json()
                    }
                })
                .then(body => {
                    if (body.status == "success") {
                        resolve(body.data)
                    } else if (body.status == "fail") {
                        reject(body.message);
                    } else {
                        console.error(body);
                    }
                });
        });

    },

    render: function() {
        return (
            <div className="card">
                <div className="card-header">
                    <h1 className="page-title"><i className="fa fa-cog"></i> Settings</h1>
                </div>
                <div className="card-block">
                    <SettingsForm onSubmit={this.saveSettings} getInitialValues={this.getInitialValues()} />
                </div>
            </div>
        )
    }
});

export default Settings;