import React from 'react';
import { Link } from 'react-router';

const Main = React.createClass({
    render() {
        return (
            <div className="container-fluid" id="account">
                <div className="row">
                    <div className="col-sm-3 col-md-2 sidebar">

                        <ul className="nav nav-sidebar">
                            <li className="heading"><h2>My Account</h2></li>
                            <li><Link to="/account/settings">Settings</Link></li>
                        </ul>

                    </div>
                    <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 admin-panel main">

                        {this.props.children}

                    </div>
                </div>
            </div>
        )
    }
});

export default Main;