import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import settings from './settings';

const rootReducer = combineReducers({settings, routing: routerReducer, form: formReducer });

export default rootReducer;