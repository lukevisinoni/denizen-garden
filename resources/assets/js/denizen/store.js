import { createStore, compose, applyMiddleware } from 'redux'
import { syncHistoryWithStore} from 'react-router-redux'
import { browserHistory } from 'react-router'
import settings from './data/settings';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/index';

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);

export function configureStore(initialState) {
    const store = createStoreWithMiddleware(rootReducer, initialState);
    return store;
}

// create an object for the default data
const initialState = {
    settings
};

const store = configureStore(initialState);

export const history = syncHistoryWithStore(browserHistory, store);

export default store;