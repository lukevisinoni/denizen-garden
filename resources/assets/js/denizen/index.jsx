// import { createDevTools } from 'redux-devtools'
// import LogMonitor from 'redux-devtools-log-monitor'
// import DockMonitor from 'redux-devtools-dock-monitor'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { createStore, combineReducers } from 'redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import store, { history } from './store'

// import my components
import DenizenApp from './components/DenizenApp'
import Home from './components/Home'
import Account from './components/Account'

// const DevTools = createDevTools(
//    <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
//        <LogMonitor theme="tomorrow" preserveScrollTop={false} />
//    </DockMonitor>
//)

// Import css
import css from './styles/style.styl'

const router = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/home" component={DenizenApp}>
                <IndexRoute component={Home}></IndexRoute>
                <Route path="/home" component={Home} />
                <Route path="/account" component={Account} />
            </Route>
        </Router>
    </Provider>
)

render(router, document.getElementById('account'));


