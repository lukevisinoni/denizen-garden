
function settings(state = [], action) {
    switch (action.type) {
        case "SETTINGS_LOAD":
            return action.settings;
    }
    return state;
}

export default settings;