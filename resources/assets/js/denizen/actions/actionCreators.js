
// update
export function updateSettings(settings) {
    return {
        type: 'UPDATE_SETTINGS',
        settings
    }
}

export function receiveSettings(settings) {
    return {
        type: 'SETTINGS_LOAD',
        settings: settings
    };
}

export function loadSettings() {
    return dispatch =>
        fetch('/ajax/account/settings', {headers: {'Accept': 'application/json', 'X-Requested-With': 'XMLHttpRequest'}})
            .then(response => response.json())
            .then(json => json.data.settings)
            .then(settings => dispatch(receiveSettings(settings)))
            .catch(err => { throw err; });
}
