import React from 'react'
import {Link} from 'react-router'

const Sidebar = (props) =>
    <nav>
        <ul className="nav nav-sidebar">
            <li><Link to="/home">Dashboard</Link></li>
            <li className="heading"><h2>My Account</h2></li>
            <li><Link to="/account">Account</Link></li>
        </ul>
    </nav>

export default Sidebar
