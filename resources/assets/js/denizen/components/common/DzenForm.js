import React, {PropTypes as PT}  from 'react'
import ClassNames from 'classnames'

export default React.createClass({
    propTypes: {
        formStyle: PT.string
    },
    getDefaultProps: function() {
        return {
            formStyle: "horizontal"
        }
    },
    render: function() {
        return (
            <form className={ClassNames( 'form-' + this.props.formStyle )} {...this.props}>
                {this.props.children}
            </form>
        );
    }
});
