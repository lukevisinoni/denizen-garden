import React, {PropTypes as PT}  from 'react'
import ClassNames from 'classnames'

export default React.createClass({
    propTypes: {
        submitText: PT.string,
        cancel: PT.func
    },
    getDefaultProps: function() {
        return {
            submitText: "Save",
            // cancel: () => { /* Send them back in history? */ }
        }
    },
    renderCancelBtn: function() {
        if (this.props.cancel) {
            return <button className="btn btn-default" onClick={this.props.cancel}>Cancel</button>
        }
    },
    renderSubmitBtn: function() {
        if (this.props.cancel) {
            return <button type="submit" className="btn btn-primary" {...this.props}>{this.props.submitText}</button>
        }
    },
    render: function() {
        return (
            <div className={ClassNames( 'submit-button pull-right')}>
                {this.props.children}
                {this.renderCancelBtn()}
                {this.renderSubmitBtn()}
            </div>
        );
    }
});
