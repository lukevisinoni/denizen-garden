import React, {PropTypes as PT} from 'react';
import ClassNames from 'classnames';

export default React.createClass({
    propTypes: {
        name: PT.string.isRequired,
        id: PT.string,
        label: PT.string,
        validate: PT.func,
        asyncValidate: PT.instanceOf(Promise),
        asyncValidating: PT.bool,
        defaultValue: PT.string
    },
    getDefaultProps: function() {
        return {
            asyncValidating: false
        }
    },
    getInitialState: function() {
        return {

        }
    },
    getId: function() {
        return (this.props.id) ? this.props.id : this.props.name;
    },
    isError: function() {

    },
    isSuccess: function() {

    },
    isWarning: function() {

    },
    isSpinner: function() {

    },
    renderLabel: function() {
        if (this.props.label) {
            return <label className="form-control-label" htmlFor={this.getId()}>{this.props.label}</label>
        }
    },
    renderInput: function() {
        var cls = ClassNames({
            'form-control': true,
            'form-control-danger': this.isError(),
            'form-control-success': this.isSuccess(),
            'form-control-warning': this.isWarning(),
            'form-control-spinner': this.isSpinner()
        });
        return (
            <input id={this.getId()} className={cls} {...this.props} />
        );
    },
    renderHelpText: function() {
        if (this.props.helpText) {
            return <small>{this.props.helpText}</small>
        }
    },
    render: function() {
        var cls = ClassNames({
            'form-group': true,
            'has-error': this.isError(),
            'has-success': this.isSuccess(),
            'has-warning': this.isWarning(),
            'has-spinner': this.isSpinner()
        });
        return (
            <div className={cls}>
                {this.renderLabel()}
                {this.renderInput()}
                {this.renderHelpText()}
            </div>
        );
    }
});