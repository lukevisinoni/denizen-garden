import React from 'react'

const Card = React.createClass({
    renderPageTitle: function(title, icon) {
        var iconHtml;
        if (icon) {
            iconHtml = <i className={"fa fa-" + icon}> </i>;
        }
        return (
            <h1 className="page-title">
                {iconHtml} {title}
            </h1>
        );
    },
    render: function() {
        return (
            <div className="card">
                <div className="card-header">
                    {this.renderPageTitle(this.props.title, this.props.icon)}
                </div>
                <div className="card-block">
                    {this.props.children}
                </div>
            </div>
        );
    }
});

export default Card;