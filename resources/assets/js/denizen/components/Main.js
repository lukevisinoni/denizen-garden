import React from 'react'
import { Link } from 'react-router'
import Sidebar from './common/Sidebar'

export default React.createClass({
    render: function() {
        return (
            <div className="container-fluid" id="account">
                <div className="row">
                    <div className="col-sm-3 col-md-2 sidebar">

                        <Sidebar />

                    </div>
                    <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 admin-panel main">

                        {this.props.children}

                    </div>
                </div>
            </div>
        );
    }
});