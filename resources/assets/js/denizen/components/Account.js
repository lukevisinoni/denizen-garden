import React, {PropTypes as P} from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';
import Card from './common/Card'
import DzenForm from './common/DzenForm'
import DzenInput from './common/DzenInput'
import DzenSubmit from './common/DzenSubmit'
//import request from 'superagent'

const Account = React.createClass({
    propTypes: {
        dispatch: P.func.isRequired,
    },
    getDefaultProps: function() {
        return {
            defaults: {}
        }
    },
    getInitialState: function() {
        return {
            data: this.props.settings
        };
    },
    handleChange: function(event) {
        console.log(event, "EVENT")
        const {name, value} = event.target;
        const data = Object.assign({}, this.state.data);
        data[name] = value;
        console.log(data)
        this.setState({data: data});
    },
    handleSubmit: function(e) {
        e.preventDefault();
    },
    handleCancel: function(e) {
        e.preventDefault();
    },
    componentDidMount: function() {

        actionCreators.loadSettings()(this.props.dispatch)
        this.setState({data: this.props.settings});

        /*var def = new Promise((resolve, reject) => {
            request
                .get(Denizen.getRoute('account.settings'))
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) {
                        reject(err);
                    }
                    if (res.body.status == "success") {
                        resolve(res.body.data.settings);
                    }
                });
        });
        def.then(defaults => {
            console.log(defaults);
            this.setState({
                defaults: defaults,
                data: defaults
            });
        });*/
    },
    render: function() {
        return (
            <Card title="My Account" icon="user">
                <DzenForm formStyle="horizontal" onSubmit={this.handleSubmit}>
                    <DzenInput type="text" name="name" ref="name" onChange={this.handleChange} value={this.state.data.name} label="Account Name" />
                    <DzenInput type="text" name="website" ref="website" onChange={this.handleChange} value={this.state.data.website} label="Company Website" />
                    <DzenInput type="text" name="email" ref="email" onChange={this.handleChange} value={this.state.data.email} label="Contact E-mail" />
                    <DzenSubmit cancel={this.handleCancel} />
                </DzenForm>
            </Card>
        )
    }
});

function mapStateToProps(state) {
    const { settings } = state;

    return {
        settings,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch: dispatch,

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)

