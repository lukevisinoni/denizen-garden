import React from 'react'
import Card from './common/Card'

export default React.createClass({
    render: function() {
        return (
            <Card title="Dashboard" icon="dashboard">
                {this.props.children}
            </Card>
        )
    }
});

