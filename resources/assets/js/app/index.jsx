// index.jsx
import React, { Component } from 'react'
import { render } from 'react-dom'
import MainApp from './components/MainApp'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

// const initialState = {
//     reg: {
//         userIsRegistered: false,
//         user: {}
//     },
// };
//
// const regReducer = function(state = initialState, action) {
//
//     // if (typeof(state) == 'undefined') {
//     //     return initialState;
//     // }
//     switch (action.type) {
//         case 'USER_REGISTERED':
//             var newstate = Object.assign({}, state, {
//                 reg: {
//                     userIsRegistered: true,
//                     user: action.user
//                 }
//             });
//             console.log(newstate);
//             return newstate;
//         default:
//             return state
//     }
//
// }

const reducers = {
    // ... your other reducers here ...
    form: formReducer,     // <---- Mounted at 'form'. See note below.
//    reg: regReducer
}
const reducer = combineReducers(reducers);
const store = createStore(reducer);

render(
    <Provider store={store}>
        <MainApp />
    </Provider>,
    document.getElementById('loginRegister')
)
