// BSForm.js
import React from 'react'

var Swifty = window.Swifty || {};

var BSInputObject = React.createClass({

    propTypes: {
        name: React.PropTypes.string.isRequired,
        label: React.PropTypes.string,
        type: React.PropTypes.string,
    },

    getDefaultProps: function() {
        return {
            className: '',
            type: "text"
        };
    },

    renderLabel: function() {
        console.table(this.props);
        return (this.props.label) ?
            (<label className="form-control-label" htmlFor={this.props.name}>{this.props.label}</label>) : '';
    },

    getErrorsAsString: function() {
        return this.props.errors.join(", ");
    },

    renderErrors: function() {
        return (this.props.error) ?
            (<small className="text-help">{this.getErrorsAsString()}</small>) : '';
    },

    render: function() {
        console.table(this.props);
        return (
            <div className={(false ? ' has-danger ' : '') + (false ? ' has-success ' : '') + ' form-group '}>
                {this.renderLabel()}
                <div className="form-input">
                    <input type={this.props.type} className={(this.props.error ? 'form-control-danger' : '') + (false ? ' form-control-success ' : '') + ' form-control ' + this.props.className} id={this.props.id || this.props.name} name={this.props.name} {...this.props} />
                    {/*this.props.touched && this.props.error && <div>{this.props.error}</div>*/}
                </div>
            </div>
        );
    }
});

var BSForm = React.createClass({

    propTypes: {
        id: React.PropTypes.string.isRequired,
        onSubmit: React.PropTypes.func,
        token: React.PropTypes.string,
        className: React.PropTypes.string,
        formStyle: React.PropTypes.string,
        method: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
            formStyle: 'horizontal',
            method: 'POST',
            className: ''
        };
    },

    getInitialState: function() {
        return {};
    },

    renderTokenField: function(token) {
        return (token) ?
            <input type="hidden" name="_token" value={token} /> : '';
    },

    render: function() {
        var token = !(this.props.token) ? ('undefined' !== typeof(Swifty.csrfToken)) ? Swifty.csrfToken : '' : this.props.token;
        return (
            <form
                className={this.props.className + ' form-' + this.props.formStyle}
                {...this.props}>
                {this.renderTokenField(token)}
                {this.props.children}
            </form>
        );
    }
});

export {BSInputObject};
export default {BSForm};