// LoginRegister.jsx
import React from 'react'
import Modal from 'react-modal'

export default React.createClass({

    propTypes: {
        text: React.PropTypes.string,
        modalTitle: React.PropTypes.string,
        btnText: React.PropTypes.string,
        handleBtnClicked: React.PropTypes.func,
        showButtons: React.PropTypes.bool
    },

    getDefaultProps: function() {
        return {btnText: "Save", showButtons: true};
    },

    getInitialState: function() {
        return {modalIsOpen: false};
    },

    openModal: function() {
        this.setState({modalIsOpen: true});
    },

    closeModal: function() {
        this.setState({modalIsOpen: false});
    },

    handleModalCloseRequest: function() {
        // opportunity to validate something and keep the modal open even if it
        // requested to be closed
        this.setState({modalIsOpen: false});
    },

    renderFooter: function() {
        if (this.props.showButtons) {
            return (
                <div className="modal-footer">
                    <button type="button" className="btn btn-default" onClick={this.handleModalCloseRequest}>Close</button>
                    <button type="button" className="btn btn-primary" onClick={this.props.handleBtnClicked}>{this.props.btnText}</button>
                </div>
            );
        }
    },

    render: function() {
        return (
            <div>
                <a href="javascript:;" onClick={this.openModal}>{this.props.text}</a>
                <Modal
                    className="Modal__Bootstrap modal-dialog"
                    closeTimeoutMS={150}
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.handleModalCloseRequest}
                >
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" onClick={this.handleModalCloseRequest}>
                                <span aria-hidden="true">&times;</span>
                                <span className="sr-only">Close</span>
                            </button>
                            <h4 className="modal-title">{this.props.modalTitle}</h4>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        {this.renderFooter()}
                    </div>
                </Modal>
            </div>
        );
    }
});
