import { combineReducers } from 'redux'

const SHOW_ALL = 1;

function visibilityFilter(state = SHOW_ALL, action) {
    return state
}

function nothings(state = [], action) {
    // Somehow calculate it...
    return state
}

const mainAppReducer = combineReducers({
    visibilityFilter,
    nothings
})

export default mainAppReducer;