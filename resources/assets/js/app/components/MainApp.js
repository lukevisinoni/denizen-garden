// MainApp.js
import React, {Component, PropTypes} from 'react';
import LinkToModal from './LinkToModal'
import SignUpForm from './forms/SignUpForm'
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';


var SignUpFormComponent = React.createClass({

    getInitialState: function() {
        return {
            errors: []
        };
    },

    contextTypes: {
        store: React.PropTypes.object.isRequired
    },

    handleCancel: function() {
        this.refs.SignUpModal.closeModal();
    },

    signUpSuccess: function(user) {
        return () => {

        }
    },

    /**
     * There's definitely a more pure "redux" way to do this (registering a new user). What I should be doing is
     * dispatching an action called REGISTER_USER that changes the state of the application to represent that a user has
     * been registered. It would also store the user data. Then I would dispatch a SAVE action that would persist that
     * data. But that's too much of a pain in the ass and I want to move on.
     * @param data
     * @param dispatch
     * @returns {Promise}
     */
    handleSignUp: function(data, dispatch) {
        // register new user

        return new Promise((resolve, reject) => {
            // register new user
            var fdata = new FormData();
            for (var elem in data) {
                fdata.set(elem, data[elem]);
            }

            // @todo Pull global options from Denizen object when I get it wired up
            var options = {
                method: 'POST',
                // credentials: 'same-origin',
                body: fdata,
                headers: {
                    'Accept': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            };

            fetch('/ajax/user', options)
                .then((response) => {
                    return response.json();
                })
                .then((body) => {
                    switch(body.status) {
                        case 'success':
                            // @todo resolve this when you've got a better grasp of react/redux
                            // dispatch({type: "USER_REGISTERED", user: body.data.user});
                            resolve();
                            break;
                        case 'fail':
                            reject(body.errors);
                            break;
                        case 'error':
                            reject({_error: body.message});
                            break;
                    }
                })
                .catch((error) => {
                    reject({_error: error.message});
                    console.error(error);
                });
        });
    },

    submitSignUpForm: function(e) {
        this.refs.SignUpForm.submit();
    },

    render: function() {
        return (
            <li className="register">
                <LinkToModal ref="SignUpModal" text="Sign Up" modalTitle="Sign up" handleBtnClicked={this.submitSignUpForm} showButtons={false} >
                    <SignUpForm ref="SignUpForm" onSubmit={this.handleSignUp} handleCancel={this.handleCancel} />
                </LinkToModal>
            </li>
        );
    }

});

// function mapStateToProps(state) {
//     return { reg: state.reg }
// }

// export default connect(mapStateToProps)(React.createClass({
export default React.createClass({
    contextTypes: {
        store: React.PropTypes.object.isRequired
    },
    render() {
        return (
            <div className="MainApp">
                <ul className="pull-right login-options">
                    <li className="login">
                        <a href="/login"> Login </a>

                    </li>
                    <SignUpFormComponent />
                </ul>
            </div>
        );
    }
});
