// LoginRegister.js
import 'whatwg-fetch';
import React from 'react'
import _ from 'underscore'
import LinkToModal from './LinkToModal'
import {BSForm, BSInputObject} from './BSForm'

var LoginComponent = React.createClass({

    getInitialState: function() {
        return {
            errors: [],
            email: '',
            password: '', // @todo move these into "fields"
        };
    },

    doLogin: function() {
        // figure out how to submit the form...
    },

    validateEmail: function(field) {
        // var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
        // don't need to validate because we're only checking authentication
    },

    validatePassword: function (field) {

    },

    handleEmailChange: function(e) {
        //e.preventDefault();
        var email = e.target.value;
        console.log(email);
        this.setState({email: email});
        // this.validateEmail(e.target);
    },

    handlePasswordChange: function(e) {
        e.preventDefault();
        var password = e.target.value;
        this.setState({password: password});
        // this.validatePassword(e.target);
    },

    handleLogin: function(e) {
        e.preventDefault();
        return false;
        // $.ajax(); // @todo I have to write an authentication method in my local json api for this to work...
    },

    render: function() {
        return (
            <li className="login">
                <LinkToModal text="Login" modalTitle="Sign in" handleBtnClicked={this.doLogin}>
                    <BSForm formStyle="vertical" id="login-form" onSubmit={this.handleLogin}>
                        <BSInputObject name="email" type="email" label="E-mail Address" errors={this.state.errors} value={this.state.email} onChange={this.handleEmailChange} />
                        <BSInputObject name="password" type="password" label="Password" errors={this.state.errors} onChange={this.handlePasswordChange} />
                        <button type="submit" className="temporary btn btn-primary">Login</button>
                    </BSForm>
                </LinkToModal>
            </li>
        );
    }

});

/**
 * @todo Implement validators as an array of anonymous functions that return true if valid and populate the errors array
 * if invalid (with an error message)
 * @todo Remove underscore dependancy and just copy the debounce method into Swifty
 * @todo Remove jquery dependancy and use a small ajax library (or roll your own)
 */
var RegisterComponent = React.createClass({

    getInitialState: function() {
        return {
            name: {
                isValid: false,
                errors: [],
                value: ''
            },
            email: {
                isValid: false,
                errors: [],
                value: ''
            },
            password: {
                isValid: false,
                errors: [],
                value: ''
            },
            password_verify: {
                isValid: false,
                errors: [],
                value: ''
            },
        };
    },

    doRegister: function() {
        // figure out how to submit the form...
    },

    resetField: function(field) {
        var initState = this.getInitialState();
        if (field in initState) {
            this.setState(initState[field]); // does assoc array notation work here?
        }
    },

    validateName: function(field) {
        if (field.value.length > 65) {
            this.setState({
                name: {
                    isValid: false,
                    errors: ['Sorry, your name is just too long to fit it in our database. Try something shorter please!']
                }
            });
        } else {
            this.resetField('name');
        }
    },

    /*validateEmail: function(field) {

        var email = field.value.trim();

        var doValidate = new Promise(function(resolve, reject){
            // check that it is a valid e-mail
            var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i;
            if (re.test(email)) {
                var errors = ['just checking...'];
                fetch('/ajax/auth/email/' + encodeURIComponent(email))
                    .then(function(response) {
                        if(response.ok) {
                            return response.json();
                        } else {
                            console.log('Network response was not ok.');
                        }
                    })
                    .then((function(json) {
                        console.log(errors, "errors!!");
                        console.log(json, 'JSON');
                        return (errors) => { if (!json.data.available) { console.log('add error'); errors.push('E-mail is not available'); } };
                    })(errors))
                    .catch((error) => { console.log('There has been a problem with your fetch operation: ' + error.message) });
            } else {
                errors.push('Invalid e-mail address');
            }
        });

        doValidate
            .then((val) => console.log(val + ' is the value'))
            .catch((error) => console.log(error + ' is the error'));

        this.resetField('email');
        this.setState({email: {isValid: true}});
        if (errors.length) {
            this.setState({
                email: {
                    isValid: false,
                    errors: errors
                }
            });
        }

    },*/

    /**
     * Majorly overcomplicated shit here...
     * @param field
    validateEmail: function(field) {

        var emailVal = field.value.trim();
        var eMail = {
            errors: [],
            value: field.value.trim()
        };

        var doValidate = new Promise(function(resolve, reject) {
            // each validator is a function that should return undefined or false if it passes, or an error message if it fails
            var errors = [function(email) {
                console.log(email, "EMAIL_1");
                return new Promise(function(resolve, reject){
                    var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i;
                    if (!re.test(email)) resolve('You must enter a valid e-mail address');
                    reject('Supposing valid e-mail address');
                });
            }, function(email) {
                console.log(email, "EMAIL_2");
                return new Promise(function(resolve, reject){
                    if (!! email) return;
                    fetch('/ajax/auth/email/' + encodeURIComponent(email))
                        .then(function(response) {
                            if(response.ok) {
                                return response.json();
                            } else {
                                console.log('Network response was not ok.');
                            }
                        })
                        .then(function(json) {
                            if (json.status == "success") {
                                if (!json.data.available) resolve('There is already an account under this e-mail address');

                            } else {
                                reject('There was a problem with the availability check');
                            }
                        })
                        .catch((error) => { console.log('There has been a problem with your fetch operation: ' + error.message); reject(error.message); });
                });
            }].map(function(validator, i, arr) {

                // although the error ultimately does get assigned to this variable, this function can't use it as a
                // return value because by the time it's assigned, it's too late. Need to "promise" the value of error
                var emailAvail = new Promise(function(resolve, reject) {
                    // although the error ultimately does get assigned to this variable, this function can't use it as a
                    // return value because by the time it's assigned, it's too late. Need to "promise" the value of error
                    var errorMsg;
                    var err;
                    validator(eMail.value)
                        .then(function(message){
                            errorMsg = message;
                            eMail.errors.push(errorMsg);
                            console.log(errorMsg, "errorMsg");
                            console.log(eMail, "eMail");
                        })
                        .catch((error) => { err = error; console.log(err, "ERR"); });
                    return errorMsg;

                });
                emailAvail.then(function(value) { console.log(value + ' is the availability message') }).catch(function(error){ console.log(error, "ERROR") });

            }, eMail);
            resolve(eMail);
        });

        doValidate
            .then(function(result){
                console.log(result, "FINAL COUNTDOWN");
            })
            .catch((error) => { console.log(error); });

    },
     */

    validateEmail: function(field) {

        var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i;
        if (!re.test(email)) {
            // foo
        }

    },

    validatePassword: function (field) {

        var fieldVal = field.value;
        var errors = [];
        var conditions = [];
        if (!/[A-Z]/.test(fieldVal)) {
            conditions.push('contain at least one capital letter');
        }
        if (!/[0-9]/.test(fieldVal)) {
            conditions.push('contain at least one digit');
        }
        if (fieldVal.length < 6) {
            conditions.push('be at least six characters long')
        }
        if (conditions.length) {
            errors.push('Password must ' + conditions.join(", "));
        }
        if (errors.length) {
            this.setState({
                password: {
                    isValid: false,
                    errors: errors
                }
            });
        } else {
            this.resetField('password');
        }
    },

    validatePasswordVerify: function (field) {
        var fieldVal = field.value;
        var errors = [];
        if (fieldVal != this.state.password.value) {
            errors.push('Passwords do not match, try again');
        }
        if (errors.length) {
            this.setState({
                password_verify: {
                    isValid: false,
                    errors: errors
                }
            });
        } else {
            this.resetField('password_verify');
        }
    },

    handleNameChange: function(e) {
        e.persist(); // necessary for debounce
        this.delayedHandleNameChange(e);
    },

    handleEmailChange: function(e) {
        e.persist(); // necessary for debounce
        this.delayedHandleEmailChange(e);
    },

    handlePasswordChange: function(e) {
        e.persist(); // necessary for debounce
        this.delayedHandlePasswordChange(e);
    },

    handlePasswordVerifyChange: function(e) {
        e.persist(); // necessary for debounce
        this.delayedHandlePasswordVerifyChange(e);
    },

    handleRegister: function(e) {
        e.preventDefault();
        return false;
        // $.ajax(); // @todo I have to write an registration method in my local json api for this to work...
    },

    componentWillMount: function() {

        this.delayedHandleNameChange = _.debounce(function(e) {
            e.preventDefault();
            var name = e.target.value;
            this.setState({name: {value: name}});
            this.validateName(e.target);
        }, 500);

        this.delayedHandleEmailChange = _.debounce(function(e) {
            e.preventDefault();
            var email = e.target.value;
            this.setState({email: {value: email}});
            this.validateEmail(e.target);
        }, 500);

        this.delayedHandlePasswordChange = _.debounce(function(e) {
            e.preventDefault();
            var password = e.target.value;
            this.setState({password: {value: password}});
            this.validatePassword(e.target);
        }, 500);

        this.delayedHandlePasswordVerifyChange = _.debounce(function(e) {
            e.preventDefault();
            var password_verify = e.target.value;
            this.setState({password_verify: {value: password_verify}});
            this.validatePasswordVerify(e.target);
        }, 500);

    },

    render: function() {
        return (
            <li className="login">
                <LinkToModal text="Sign Up" modalTitle="Sign up" handleBtnClicked={this.doRegister}>
                    <BSForm formStyle="vertical" id="login-form" onSubmit={this.handleRegister}>
                        <BSInputObject name="name" type="text" label="Name" errors={this.state.name.errors} value={this.state.name.value} onChange={this.handleNameChange} />
                        <BSInputObject name="email" type="email" label="E-mail Address" isValid={this.state.email.isValid} errors={this.state.email.errors} value={this.state.email.value} onChange={this.handleEmailChange} />
                        <BSInputObject name="password" type="password" label="Password" errors={this.state.password.errors} onChange={this.handlePasswordChange} />
                        <BSInputObject name="password_verify" type="password" label="Password (again)" errors={this.state.password_verify.errors} onChange={this.handlePasswordVerifyChange} />
                        <button type="submit" className="temporary btn btn-primary">Register</button>
                    </BSForm>
                </LinkToModal>
            </li>
        );
    }

});


var RegisterFormComponent = React.createClass({

    getInitialState: function() {
        return {
            errors: []
        };
    },

    render: function() {
        return (
            <li className="register">
                <LinkToModal text="Sign Up" modalTitle="Sign up" handleBtnClicked={this.doRegister}>
                    <SignUpForm />
                </LinkToModal>
            </li>
        );
    }

});



export default React.createClass({

    getInitialState: function() {
        return {
            errors: []
        };
    },

    validateLogin: function (data) {

    },

    doLogin: function() {
        // just do whatever you need to do here... no need to actually submit the form, dumbass!
        // @todo I would rather not have to use jQuery but I'm sick of dicking around... fix it later...
        //$('#login-form').
    },

    handleLoginFormSubmit: function(e) {
        e.preventDefault();

    },

    render: function() {
        return (
            <ul className="pull-right login-options">
                <LoginComponent />
                <RegisterFormComponent />
            </ul>
        );
    }

});
