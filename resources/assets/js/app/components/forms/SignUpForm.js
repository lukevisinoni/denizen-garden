// SignUpForm.js
import React, { Component, PropTypes } from 'react'
import { reduxForm } from 'redux-form'
// import { Denizen } from '../Denizen'

export const fields = [ 'name', 'email', 'password', 'password_confirmation' ]

var Swifty = window.Swifty || {};

var messages = {
    required: "This field is required",
    emailInvalid: "Please provide a valid e-mail address",
    emailUnavailable: 'That e-mail address is already signed up',
    passwordsMatch: 'Passwords do not match. Try retyping them again.',
    minLength: (field, min) => { field + ' must be at least ' + min + ' characters long' }
};

const validate = values => {
    const errors = {}
    if (!values.name) {
        errors.name = messages.required;
    }
    if (!values.email) {
        errors.email = messages.required;
    }
    if (!values.password) {
        errors.password = messages.required;
    } else if (values.password.length  < 6) {
        errors.password = messages.minLength('Password', 6);
    }
    if (!values.password_confirmation) {
        errors.password_confirmation = messages.required;
    }
    if (values.password_confirmation != values.password) {
        errors.password_confirmation = messages.passwordsMatch;
    }
    return errors
}

const asyncValidate = (values/*, dispatch */) => {
    return new Promise((resolve, reject) => {
        var re = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i;
        if (!values.email || !re.test(values.email.trim())) {
            reject({email: messages.emailInvalid});
        } else {
            // @todo Use Swifty object constants or methods to get the API url
            // fetch(Denizen.route('email.available', {email: email})) // I dont know why this wont work :(
            fetch('/ajax/auth/email/' + encodeURIComponent(values.email.trim()), {headers: {'Accept': 'application/json', 'X-Requested-With': 'XMLHttpRequest'}})
                .then((response) => {
                    if (response.ok) {
                        return response.json()
                    }
                })
                .then((body) => {
                    if (body.status == 'success') {
                        if (body.data.available) {
                            resolve();
                        }
                        else {
                            reject({email: messages.emailUnavailable});
                        }
                    } else {
                        // @todo handle this
                        if (body.status == "error") {
                            console.error(body.message);
                        } else {
                            console.error(body);
                        }
                    }
                })
                .catch((error) => {
                    console.error(error.message)
                });
        }
    });
}

var SignUpForm = React.createClass({

    propTypes: {
        asyncValidating: PropTypes.string.isRequired,
        fields: PropTypes.object.isRequired,
        resetForm: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        submitting: PropTypes.bool.isRequired
    },

    isError: function (field) {
        return (field.touched && field.error);
    },

    isSuccess: function(field, options) {
        return options.showSuccess && (!field.active && (field.touched && field.valid));
    },

    renderSpinner: function(field, options) {
        if (this.isSpinner(field, options)) {
            return <div className="col-xs-1" style={{padding: ".375rem 0"}}><i className="fa fa-spinner fa-pulse fa-fw"/></div>;
        }
    },

    isSpinner: function(field, options) {
        return (options.asyncValidating && options.asyncValidating === field.name);
    },

    renderLabel: function(options) {
        if (options.label) {
            return <label className="form-control-label col-xs-4" htmlFor="email">{options.label}</label>;
        }
    },

    renderField: function(field, options) { // options = label, type, className, placeholder, asyncValidating, showSuccess
        return (
            <div className={(this.isError(field) ? ' has-danger ' : '') + (!this.isSpinner(field, options) && this.isSuccess(field, options) ? ' has-success ' : '') + (this.isSpinner(field, options) ? ' has-spinner ' : '') + ' form-group row '}>

                {this.renderLabel(options)}

                <div className=" col-xs-7">
                    <input
                        type={options.type || "text"}
                        placeholder={options.placeholder || ""}
                        className={(options.className || " ") + (this.isError(field) ? "form-control-danger" : " ") + (this.isSuccess(field, options) ? " form-control-success " : " ") + (this.isSpinner(field, options) ? " form-control-spinner " : " ") + " form-control "}
                        {...field} />
                    {this.renderError(field)}
                </div>

                {this.renderSpinner(field, options)}

            </div>
        );
    },

    renderTokenField: function () {
        var token = (!this.props.token) ? ('undefined' !== typeof(Swifty.csrfToken)) ? Swifty.csrfToken : '' : this.props.token;
        return (
            <input type="hidden" name="_token" value={token} />
        );
    },

    renderError: function (field) {
        if (this.isError(field)) {
            return <small className="text-help" style={{display: "block", padding: "0 5px"}}>{field.error}</small>;
        }
    },

    render: function () {
        const { asyncValidating, fields: { name, email, password, password_confirmation }, resetForm, handleSubmit, submitting } = this.props;
        return (
            <form style={{padding: "1em"}} onSubmit={handleSubmit} method="POST" className={(this.props.className || ' form-horizontal')}>
                {this.renderTokenField()}
                {this.renderField(name, {label: "Your name", placeholder: "Your name", asyncValidating: asyncValidating})}
                {this.renderField(email, {label: "E-mail", placeholder: "E-mail", asyncValidating: asyncValidating, showSuccess: true})}
                {this.renderField(password, {label: "Password", placeholder: "Password", asyncValidating: asyncValidating, type: "password"})}
                {this.renderField(password_confirmation, {label: "Password (again)", placeholder: "Password (again)", asyncValidating: asyncValidating, type: "password"})}
                <div className="form-group pull-right">
                    <button type="button" disabled={submitting} onClick={this.props.handleCancel} className="temporary btn btn-default">
                        Cancel
                    </button>
                    <button type="submit" disabled={submitting} className="temporary btn btn-primary">
                        {submitting ? <i className="fa fa-spinner fa-pulse fa-fw" /> : <i className="fa fa-user-plus" />} Sign Up
                    </button>
                </div>
                {this.props.children}
            </form>
        )
    }

});

export default reduxForm({
    form: 'signup',
    fields,
    asyncValidate,
    asyncBlurFields: ['email'],
    validate
})(SignUpForm)