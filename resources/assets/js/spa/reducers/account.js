import {
    ACCOUNT_RECEIVE_SETTINGS,
    ACCOUNT_UPDATE_SETTINGS,
    ACCOUNT_PERSIST_SETTINGS,
    ACCOUNT_PERSIST_SETTINGS_SUCCESS,
    ACCOUNT_PERSIST_SETTINGS_FAIL,
    ACCOUNT_PERSIST_SETTINGS_ERROR
} from '../constants/ActionTypes'

const initialState = {
    settings: {
        name: "",
        email: "",
        website: ""
    },
    isSaving: false,
    isSaved: null
}

export default function account(state = initialState, action) {
    switch (action.type) {
        case ACCOUNT_RECEIVE_SETTINGS:
            return {...state, settings: {...action.settings}};
        case ACCOUNT_UPDATE_SETTINGS:
            return {...state, settings: {...action.settings}};
        case ACCOUNT_PERSIST_SETTINGS:
            return {...state, isSaving: true}
        case ACCOUNT_PERSIST_SETTINGS_SUCCESS:
            return {...state, settings: {...action.meta.previousAction.payload.request.data}, isSaving: false, isSaved: true}
        case ACCOUNT_PERSIST_SETTINGS_FAIL:
            return {...state, isSaving: false, isSaved: false}
        default:
            return state;
    }
}
