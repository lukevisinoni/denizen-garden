import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import account from './account'
import properties from './properties'

export default combineReducers({
  account: account,
  properties: properties,
  routing: routerReducer
})
