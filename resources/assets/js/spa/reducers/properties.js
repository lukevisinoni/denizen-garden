import {
    PROPERTIES_LOAD,
    PROPERTIES_LOAD_SUCCESS,
    PROPERTIES_LOAD_FAIL,
    PROPERTIES_CREATE_VALIDATE
} from '../constants/ActionTypes'

const initialState = {
    list: {},
    isLoading: false,
    isLoaded: false,
}

export default function account(state = initialState, action) {
    switch (action.type) {
        case PROPERTIES_LOAD:
            return {...state, isLoading: true}
        case PROPERTIES_LOAD_SUCCESS:
            var properties = [];
            if (action.payload.data.status == "success") {
                properties = action.payload.data.properties;
            }
            return {...state, isLoading: false, isLoaded: true, list: properties}
        case PROPERTIES_LOAD_FAIL:
            return {...state, isLoading: false, isLoaded: false, _error: "Could not load properties at this time."}
        case PROPERTIES_CREATE_VALIDATE:
            var data = action.property;
            // I'm really only interested in validating the address/zip code data
            
            return {...state, }
        default:
            return state;
    }
}
