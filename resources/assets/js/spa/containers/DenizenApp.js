import React, { Component, PropTypes } from 'react'
import Title from 'react-title-component'
import Sidebar from '../components/Sidebar'
import AppNavDrawer from '../components/AppNavDrawer'
import Content from '../containers/Content'

// material-ui components
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import {getMuiTheme} from '../mui-styles'
import {MuiThemeProvider} from '../mui-styles'
import {spacing} from '../mui-styles'
import {zIndex} from '../mui-styles'
import {darkWhite, lightWhite, grey900, blueGrey900, blueGrey800} from '../mui-styles/colors'
import withWidth, {MEDIUM, LARGE} from 'material-ui/utils/withWidth'

class DenizenApp extends Component {

    static propTypes = {
        children: PropTypes.node,
        location: PropTypes.object,
        width: PropTypes.number.isRequired,
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    state = {
        navDrawerOpen: true,
    };

    getStyles() {
      const styles = {
        appBar: {
          position: 'fixed',
          zIndex: zIndex.appBar,
          backgroundColor: blueGrey900,
          top: 0,
          height: 50
        },
        root: {
          paddingTop: spacing.desktopKeylineIncrement,
          minHeight: 400,
        },
        content: {
          margin: spacing.desktopGutter,
        },
        contentWhenMedium: {
          margin: `${spacing.desktopGutter * 2}px ${spacing.desktopGutter * 3}px`,
        },
        footer: {
          backgroundColor: grey900,
          textAlign: 'center',
        },
        a: {
          color: darkWhite,
        },
        p: {
          margin: '0 auto',
          padding: 0,
          color: lightWhite,
          maxWidth: 356,
        },
        browserstack: {
          display: 'flex',
          alignItems: 'flex-start',
          justifyContent: 'center',
          margin: '25px 15px 0',
          padding: 0,
          color: lightWhite,
          lineHeight: '25px',
          fontSize: 12,
        },
        browserstackLogo: {
          margin: '0 3px',
        },
        iconButton: {
          color: darkWhite,
        },
        navDrawer: {
            backgroundColor: blueGrey800,
            zIndex: zIndex.navDrawer,
        }
      };

      if (this.props.width === MEDIUM || this.props.width === LARGE) {
        styles.content = Object.assign(styles.content, styles.contentWhenMedium);
      }

      return styles;
    }

    handleTouchTapLeftIconButton = () => {
        this.setState({
            navDrawerOpen: !this.state.navDrawerOpen,
        });
    };

    handleChangeList = (event, value) => {
      this.context.router.push(value);
      this.setState({
        navDrawerOpen: true,
      });
    };

    handleChangeRequestNavDrawer = (open) => {
      this.setState({
        navDrawerOpen: open,
      });
    };

    render() {
        const {
          location,
          children,
        } = this.props;

        let {
          navDrawerOpen,
        } = this.state;

        const styles = this.getStyles();
        const title = "Denizen Garden";

        let docked = true;
        let showMenuIconButton = true;

        return (
            <div>
                <Title render={title} />
                <AppBar
                    onLeftIconButtonTouchTap={this.handleTouchTapLeftIconButton}
                    title={title}
                    zDepth={0}
                    iconElementRight={
                        <IconButton
                            iconClassName="muidocs-icon-custom-github"
                            href="https://github.com/callemall/material-ui"
                        />
                    }
                    style={styles.appBar}
                    showMenuIconButton={showMenuIconButton}
                />
                <Content>
                    {children}
                </Content>
                <AppNavDrawer
                    style={styles.navDrawer}
                    location={location}
                    docked={docked}
                    onRequestChangeNavDrawer={this.handleChangeRequestNavDrawer}
                    onChangeList={this.handleChangeList}
                    open={navDrawerOpen}
                />
            </div>
        )
    }
}

export default withWidth()(DenizenApp);
