import React, { Component, PropTypes } from 'react'
import Title from 'react-title-component'
import Paper from 'material-ui/Paper'

class GuestApp extends Component {
    render() {
        const title = "Denizen Garden";
        const style = {
            width: "50%",
            marginRight: 'auto',
            marginLeft: 'auto'
        };
        return (
            <div id="guest-app">
                <Title render={title} />
                <Paper style={style}>
                    {this.props.children}
                </Paper>
            </div>
        )
    }
}

export default GuestApp;
