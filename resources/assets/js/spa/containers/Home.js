import React, { Component } from 'react'
import Card from '../components/Card'

export default class Home extends Component {
    render() {
        return (
            <Card title="Dashboard" icon="dashboard">
                {this.props.children}
            </Card>
        )
    }
}
