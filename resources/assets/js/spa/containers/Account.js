import React, { Component } from 'react'
import Card from '../components/Card'
import {DzenForm, DzenInput, DzenSubmit} from '../components/Dzen'
import {connect} from 'react-redux'
import store from '../store'
import {persistAccountSettings} from '../actions'
import ClassNames from 'classnames'
import RaisedButton from 'material-ui/RaisedButton'
import ContentSave from 'material-ui/svg-icons/content/save'
import Snackbar from 'material-ui/Snackbar'
import {FormsyText} from 'formsy-material-ui/lib'

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            email: props.email,
            website: props.website,
            snackbarOpen: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.enableButton = this.enableButton.bind(this);
        this.disableButton = this.disableButton.bind(this);
        this.handleRequestClose = this.handleRequestClose.bind(this);
    }
    enableButton() {
        this.setState({
            canSubmit: true
        });
    }
    disableButton() {
        this.setState({
            canSubmit: false
        });
    }
    handleSubmit(model) {
        store.dispatch(persistAccountSettings(model))
            .then(() => {
                if (this.props.isSaved) {
                    this.setState({
                        snackbarOpen: true
                    })
                }
            })
            .catch((err) => { throw err; })
    }
    handleRequestClose = () => {
        this.setState({
            snackbarOpen: false,
        });
    };
    // @todo This doesn't display the initial value correctly because of the AJAX
    // loading taking place AFTER the initial render and it not re-rendering once
    // the ajax is finished. Maybe don't show the form until AFTER the async function finishes?
    render() {
        return (
            <Card title="Account" icon="user">
                <Snackbar
                    open={this.state.snackbarOpen}
                    onRequestClose={this.handleRequestClose}
                    autoHideDuration={3000}
                    message="Your account settings have been updated!"
                    className="snackbar"
                />
                <DzenForm formStyle="horizontal" onSubmit={this.handleSubmit} onValidate={this.submit} onValid={this.enableButton} onInvalid={this.disableButton}>
                    <div className="form-group">
                        <FormsyText
                            name="name"
                            validations="isWords"
                            validationError="Please enter your company name"
                            floatingLabelText="Company Name"
                            defaultValue={this.props.name}
                            value={this.props.name}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <FormsyText
                            name="email"
                            validations="isEmail"
                            validationError="Must be a valid e-mail address"
                            floatingLabelText="E-mail"
                            defaultValue={this.props.email}
                            value={this.props.email}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <FormsyText
                            name="website"
                            validations="isUrl"
                            validationError="Please enter a valid URL"
                            floatingLabelText="Website URL"
                            defaultValue={this.props.website}
                            value={this.props.website}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <RaisedButton
                            style={{marginTop: 32,}}
                            type="submit"
                            label="Save"
                            icon={<ContentSave />}
                            disabled={!this.state.canSubmit}
                        />
                    </div>
                </DzenForm>
            </Card>
        )
    }
}

function mapDispatchToProps(dispatch) {

}

function mapStateToProps(state) {
    const {account} = state;
    return {
        isSaved: account.isSaved,
        isSaving: account.isSaving,
        name: account.settings.name,
        email: account.settings.email,
        website: account.settings.website
    };
}

export default connect(mapStateToProps)(Account)
