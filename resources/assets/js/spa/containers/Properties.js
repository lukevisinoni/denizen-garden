import React, { Component, PropTypes as P } from 'react'
import {connect} from 'react-redux'
import store from '../store'
import Card from '../components/Card'
import Loading from '../components/Loading'
import PropertyListItem from '../components/PropertyListItem'
import PropertyCreateForm from '../components/PropertyCreateForm'
import {createNewProperty, validateNewProperty, loadProperties as loadPropertiesAction} from '../actions'
import ClassNames from 'classnames'
import ContentAddCircle from 'material-ui/svg-icons/content/add-circle'
import ContentAddBox from 'material-ui/svg-icons/content/add-box'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import Dialog from 'material-ui/Dialog'
import SmartyStreets from '../api/SmartyStreets'
import '../sass/properties.scss'

require('promise.prototype.finally')

class Properties extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: null,
            isLoaded: false,
            properties: [],
            dialogOpen: false,
            canSubmit: false,
            zipcodes: [],
            addressErrors: [],
            title: null,
            address: {},
            address_line2: null,
            address_zip: null
        };
        this.handleDialogOpen = this.handleDialogOpen.bind(this);
        this.handleDialogClose = this.handleDialogClose.bind(this);
        this.handleOnValid = this.handleOnValid.bind(this);
        this.handleOnInvalid = this.handleOnInvalid.bind(this);
        this.handleCreateProperty = this.handleCreateProperty.bind(this);
        this.handleUpdateValue = this.handleUpdateValue.bind(this);
        this.handleUpdateAddress = this.handleUpdateAddress.bind(this);
    }

    handleDialogOpen() {
        this.setState({dialogOpen: true});
    }

    handleDialogClose() {
        this.setState({dialogOpen: false});
    }

    handleOnValid() {
        this.setState({canSubmit: true});
    }

    handleOnInvalid() {
        this.setState({canSubmit: false});
    }

    handleUpdateValue(e) {
        var newstate = {};
        newstate[e.target.name] = e.target.value;
        this.setState(newstate);
    }

    handleUpdateAddress(address, index) {
        this.props.ssApi.zipcode(address.city, address.state)
            .then(function(resolved) { return resolved }, function(reason) { console.error("Address update promise rejected: " + reason); })
            .then(response => response[0].zipcodes)
            .then(zipcodes => zipcodes.filter(zip => { return (zip.zipcode_type == 'S') }))
            .then(zipcodes => zipcodes.map(zip => { return {county: zip.county_name, city: zip.default_city, state: zip.state_abbreviation, zipcode: zip.zipcode, lat: zip.latitude, lng: zip.longitude} } ))
            .then(zipcodes => {
                this.setState({address: address, zipcodes: zipcodes});
            })
            .catch((err) => { throw err; })
            .finally(() => {
                // I don't need this after all but it's nice to know the option is there...
            });
    }

    handleCreateProperty(e) {
        e.preventDefault();
        var data = {
            title: this.state.title,
            address: this.state.address,
            address_line2: this.state.line2,
            address_zip: this.state.address_zip
        };
        // validate address/zip code data
        this.props.ssApi.getAddressByZipcode(data.address.street_line, data.address_zip)
            .then(function(resolved) { return resolved; }, function (rejected) { console.error("Street address verification failed: " + rejected) })
            .then(matches => {
                console.log(matches, "MATCHES");
                if (!matches.length) {
                    // @todo Finish validation later... I gotta work on something
                    // else for a while or I'm going to end up punching a small
                    // child in the ear.
                    // this.setState({addressErrors: ['Invalid address']});
                } else {
                    // @todo It probably wouldn't be a bad idea to store some of
                    // the shit that comes up in the SS API results
                    store.dispatch(createNewProperty(data)).then((resolved) => {

                    },(rejected) => {

                    });
                }
            })
    }

    loadProperties() {
        if (!this.props.isLoaded) {
            store.dispatch(loadPropertiesAction())
                .then(() => {
                    this.setState({
                        isLoaded: true,
                        isLoading: false,
                        properties: this.props.list
                    })
                })
                .catch((err) => { throw err; })
        }
    }
    componentWillMount() {
        this.loadProperties();
    }
    // @todo Make sure this is necessary...
    componentWillReceiveProps(nextProps) {
        this.loadProperties();
    }
    renderProperties() {
        return (
            <div className="properties">
                {this.state.properties.map((property) => {
                    return (
                        <PropertyListItem key={property.id} property={property} />
                    )
                })}
            </div>
        )
    }
    renderDialog() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onTouchTap={this.handleDialogClose}
            />,
            <FlatButton
                label="Create"
                type="submit"
                primary={true}
                disabled={!this.state.canSubmit}
                onTouchTap={this.handleCreateProperty}
                icon={<ContentAddBox />}
            />,
        ];
        return (
            <Dialog
                title="Create new property"
                actions={actions}
                modal={true}
                open={this.state.dialogOpen}
            >
                <PropertyCreateForm
                    handleSubmit={this.handleCreateProperty}
                    handleOnValid={this.handleOnValid}
                    handleOnInvalid={this.handleOnInvalid}
                    handleUpdateValue={this.handleUpdateValue}
                    handleUpdateAddress={this.handleUpdateAddress}
                    addressItem={this.state.address}
                    validationErrors={this.state.addressErrors}
                    zipcodes={this.state.zipcodes}
                />
            </Dialog>
        )
    }
    render() {
        const {properties} = this.props;
        const PropertyActions = [<RaisedButton
            onTouchTap={this.handleDialogOpen}
            label="New Property"
            key="new"
            primary={true}
            icon={<ContentAddCircle />}
        />];
        if (this.state.isLoaded) {
            return (
                <div className="properties-wrapper">
                    {this.renderDialog()}
                    <Card title="Properties" icon="building" actions={PropertyActions}>
                        {this.renderProperties()}
                    </Card>
                </div>
            );
        } else {
            return <Loading />;
        }
    }
}

Properties.propTypes = {
    isLoaded: P.bool,
    isLoading: P.bool,
    list: P.arrayOf(P.object),
    ssApi: P.object.isRequired
}

Properties.defaultProps = {
    isLoaded: false,
    isLoading: true,
    list: [],
    ssApi: new SmartyStreets
}

function mapStateToProps(state) {
    const {properties} = state;
    return {
        isLoaded: properties.isLoaded,
        isLoading: properties.isLoading,
        list: properties.list
    };
}

export default connect(mapStateToProps)(Properties)
