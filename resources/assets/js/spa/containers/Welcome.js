import React, { Component, PropTypes } from 'react'
import Title from 'react-title-component'
import Card from '../components/Card'
import Paper from 'material-ui/Paper'
import Dialog from 'material-ui/Dialog'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import ContentAddCircle from 'material-ui/svg-icons/content/add-circle'
import ToggleCheckBox from 'material-ui/svg-icons/toggle/check-box'
import SignupForm from '../components/SignupForm'
import AWS from 'aws-sdk'

export default class Welcome extends Component {

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            signupDialogOpen: false,
            loginDialogOpen: false,
            signupCanSubmit: false,
            loginCanSubmit: false,
            signup: {},
            login: {}
        };
        this.handleSignupDialogOpen = this.handleSignupDialogOpen.bind(this);
        this.handleSignupDialogClose = this.handleSignupDialogClose.bind(this);
        this.handleLoginDialogOpen = this.handleLoginDialogOpen.bind(this);
        this.handleLoginDialogClose = this.handleLoginDialogClose.bind(this);
        this.handleSignupOnValid = this.handleSignupOnValid.bind(this);
        this.handleSignupOnInvalid = this.handleSignupOnInvalid.bind(this);
        this.handleSignupUpdateValue = this.handleSignupUpdateValue.bind(this);
        this.handleSignup = this.handleSignup.bind(this);
    }

    handleSignupDialogOpen() {
        this.setState({signupDialogOpen: true})
    }

    handleSignupDialogClose() {
        this.setState({signupDialogOpen: false})
    }

    handleLoginDialogOpen() {
        this.setState({loginDialogOpen: true})
    }

    handleLoginDialogClose() {
        this.setState({loginDialogOpen: false})
    }

    handleSignupOnValid() {
        this.setState({signupCanSubmit: true});
    }

    handleSignupOnInvalid() {
        this.setState({signupCanSubmit: false});
    }

    handleSignupUpdateValue(e) {
        var signup = this.state.signup;
        signup[e.target.name] = e.target.value;
        this.setState({signup: signup});
    }

    handleSignup(e) {
        AWS.config.region = 'us-east-1'; // Region
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'us-east-1:12ad494c-3d59-4f52-a54c-74ab75348243' // your identity pool id here
        });
        console.log(AWS.config.credentials, "AWS.config.credentials")
        AWS.config.credentials.get(function(err) {
            if (err) {
                console.error("Error: "+err);
                return;
            }
            console.log("Cognito Identity Id: " + AWS.config.credentials.identityId);

        });

        AWSCognito.config.region = 'us-east-1';
        AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'us-east-1:12ad494c-3d59-4f52-a54c-74ab75348243' // your identity pool id here
        });
        console.log(AWSCognito.config.credentials.get(), 'AWSCognito.config.credentials')

        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool({
            UserPoolId: 'us-east-1_jhuDjKrrD',
            ClientId: '47k11t14ipu5m6p7n9ft94k49a'
        });
        console.log(userPool, 'userPool')

        var attributeList = [
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'custom:account_id',
                Value: '3'
            }),
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'email',
                Value: this.state.signup.email
            }),
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'preferred_username',
                Value: this.state.signup.username
            }),
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'phone_number',
                Value: '+15304133076' // @todo I made this a required field so I have to provide it
            }),
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'given_name',
                Value: this.state.signup.firstname
            }),
            new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute({
                Name: 'family_name',
                Value: this.state.signup.lastname
            })
        ];

        userPool.signUp(this.state.signup.username, this.state.signup.password, attributeList, null, function(err, result){
            if (err) {
                console.error(err);
                //return;
            }
            cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
        });
    }

    renderSignupDialog() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onTouchTap={this.handleSignupDialogClose}
            />,
            <FlatButton
                label="Sign Up"
                type="submit"
                primary={true}
                disabled={!this.state.signupCanSubmit}
                onTouchTap={this.handleSignup}
            />,
        ];
        return (
            <Dialog
                title="Sign Up"
                actions={actions}
                modal={true}
                open={this.state.signupDialogOpen}
            >
                <SignupForm
                    handleSubmit={this.handleSignup}
                    handleOnValid={this.handleSignupOnValid}
                    handleOnInvalid={this.handleSignupOnInvalid}
                    handleUpdateValue={this.handleSignupUpdateValue}
                />
            </Dialog>
        )
    }

    renderLoginDialog() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onTouchTap={this.handleLoginDialogClose}
            />,
            <FlatButton
                label="Log in"
                type="submit"
                primary={true}
                disabled={!this.state.canSubmitLogin}
                onTouchTap={this.handleLogin}
            />,
        ];
        return (
            <Dialog
                title="Log in"
                actions={actions}
                modal={true}
                open={this.state.loginDialogOpen}
            >
                <p>This is where the login form goes</p>
            </Dialog>
        )
    }

    render() {
        const title = "Welcome"
        const actions = [<FlatButton
            onTouchTap={this.handleSignupDialogOpen}
            label="Sign up"
            key="signup"
            primary={true}
        />, <FlatButton
            onTouchTap={this.handleLoginDialogOpen}
            label="Log In"
            key="login"
            primary={true}
        />];
        return (
            <div id="welcome-page">
                <Title render={title} />
                {this.renderSignupDialog()}
                {this.renderLoginDialog()}
                <Card title="Welcome to Denizen Garden" actions={actions}>
                    <p>Welcome to the DeniZen Garden Property Manager home page. There isn't really much for this page to do yet, so I'm just going to leave it like this for now.</p>
                </Card>
            </div>
        )
    }
}
