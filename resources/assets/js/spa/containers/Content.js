import React, {Component} from 'react'
import Paper from 'material-ui/Paper'

class Content extends Component {
    getStyles() {
        return {
            padding: "25px 25px 25px " + (256 + 25) + "px"
        }
    }
    render() {
        const styles = this.getStyles();
        const {children} = this.props;
        return (
            <div style={styles}>
                <Paper>
                    {children}
                </Paper>
            </div>
        )
    }
}

export default Content;
