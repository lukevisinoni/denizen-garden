//import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
// import { amazon } from 'config'
// import AWS from 'aws-sdk'
import injectTapEventPlugin from 'react-tap-event-plugin'
import getMuiTheme from './mui-styles/getMuiTheme'
import MuiThemeProvider from './mui-styles/MuiThemeProvider'
import { getAccountSettings } from './actions'
import store, { history } from './store'
import routes from './routes'

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

store.dispatch(getAccountSettings())

const router = (
    <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme()}>
            <Router history={history}>
                {routes}
            </Router>
        </MuiThemeProvider>
    </Provider>
)

render(
  router,
  document.getElementById('denizen')
)
