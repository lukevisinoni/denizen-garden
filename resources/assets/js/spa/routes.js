import React from 'react'
import { Route, IndexRoute } from 'react-router'
import DenizenApp from './containers/DenizenApp'
import GuestApp from './containers/GuestApp'
import Home from './containers/Home'
import Welcome from './containers/Welcome'
import Account from './containers/Account'
import Properties from './containers/Properties'
import NotFound from './components/NotFound'

// @todo I wonder if I could do something like this rather than the if/else inside my denizenapp component...
const isLoggedIn = false; // obviously this would come from elsewhere...
if (isLoggedIn) {
    var App = DenizenApp;
    var HomePage = Home;
} else {
    var App = GuestApp;
    var HomePage = Welcome;
}

const routes = (
    <Route path="/" component={App}>
        <IndexRoute component={HomePage}></IndexRoute>
        <Route path="/home" component={HomePage} /> {/*<!-- @todo maybe get rid of this? -->*/}
        <Route path="/account" component={Account} />
        <Route path="/properties" component={Properties} />
        <Route path="*" component={NotFound}/>
    </Route>
);

export default routes;
