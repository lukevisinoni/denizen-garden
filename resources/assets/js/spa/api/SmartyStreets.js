//import {config} from '../config/index'
import jsonp from 'jsonp'
import axios from 'axios'

// @todo Need to unit test this
// @todo Pull preferred cities from geolocation or something
// @todo Also, don't hard-code this shit in here... figure out why it doesn't like
//       living in its own module
var config = {
    smartystreets: {
        key: '6297205615327880',
        preferred: ['Chico,CA', 'Paradise,CA', 'Oroville,CA', 'CA']
    }
}

class SmartyStreets {
    constructor(key = config.smartystreets.key) {
        this.endpoints = {
            autocomplete: 'https://autocomplete-api.smartystreets.com/suggest',
            zipcode: 'https://us-zipcode.api.smartystreets.com/lookup',
            address: 'https://api.smartystreets.com/street-address'
        };
        this.jsonpParam = 'callback';
        this.params = {};
        this.setParam('auth-id', key);
    }
    setParam(name, value) {
        this.params[name] = value;
        return this;
    }
    removeParam(name) {
        this.params[name] = undefined;
        return this;
    }
    getEndpoint(endpoint) {
        if (endpoint in this.endpoints) {
            return this.endpoints[endpoint];
        }
        throw new Error('SmartyStreets.getEndpoint() received invalid argument');
    }
    preparePreferred(preferred) {
        if (typeof preferred != 'Array') {
            throw new TypeError('SmartyStreets.preparePreferred() expects an array')
        }
        return preferred.join(';');
    }
    getQueryString() {
        return Object.entries(this.params)
            .map((pair, index, arr) => pair.map(elem => encodeURIComponent(elem)))
            .map((pair, index, arr) => pair.join('='))
            .join('&');
    }
    prepareUrl(endpoint) {
        return this.getEndpoint(endpoint) + '?' + this.getQueryString();
    }
    autocomplete(input, preferred = null) {
        return new Promise((resolve, reject) => {
            this.setParam('prefix', input);
            if (preferred) {
                this.setParam('prefer', this.preparePreferred(preferred));
            }
            jsonp(this.prepareUrl('autocomplete'), {param: this.jsonpParam}, (err, data) => {
                if (err) {
                    throw err;
                    // reject(err.message);
                } else {
                    // @todo on each of these I should be rejecting if status
                    // bad status code (or erroring?)
                    resolve(data);
                }
            });
        });
    }
    zipcode(city, state, zipcode = null) {
        return new Promise((resolve, reject) => {
            this.setParam('city', city);
            this.setParam('state', state);
            if (zipcode) {
                this.setParam('zipcode', zipcode);
            }
            jsonp(this.prepareUrl('zipcode'), {param: this.jsonpParam}, (err, data) => {
                if (err) {
                    throw err;
                    // reject(err.message);
                } else {
                    resolve(data);
                }
            });
        });
    }
    getAddressByCityAndState(street, city, state) {
        return new Promise((resolve, reject) => {
            this.setParam('street', street);
            this.setParam('city', city);
            this.setParam('state', state);
            jsonp(this.prepareUrl('address'), {param: this.jsonpParam}, (err, data) => {
                if (err) {
                    throw err;
                    // reject(err.message);
                } else {
                    resolve(data);
                }
            });
        });
    }
    getAddressByZipcode(street, zipcode) {
        return new Promise((resolve, reject) => {
            this.setParam('street', street);
            this.setParam('zipcode', zipcode);
            jsonp(this.prepareUrl('address'), {param: this.jsonpParam}, (err, data) => {
                if (err) {
                    throw err;
                    // reject(err.message);
                } else {
                    resolve(data);
                }
            });
        });
    }
}

export default SmartyStreets
