
export default class api {

    constructor(url, method = 'GET') {
        this.setUrl(url)
            .setMethod(method)
            .setHeaders(new Headers({
                'X-Requested-With': 'XMLHttpRequest',
                'Accept': 'application/json'
            }));
    }

    setMethod(method) {
        this.method = method;
        return this;
    }

    setUrl(url) {
        if (typeof (url) != 'undefined') {
            this.url = '/ajax/' + url.split('/').filter(val => val.length).join('/');
        }
        return this;
    }

    setHeaders(headers) {
        if (typeof (headers) == 'Headers') {
            this.headers = headers;
        } else {
            this.headers = new Headers(headers);
        }
        return this;
    }

    addHeader(name, value) {
        this.headers.append(name, value);
        return this;
    }

    removeHeader(name) {
        this.headers.delete(name);
        return this;
    }

    getHeaders() {
        return this.headers.getAll();
    }

    setData(data) {
        if (typeof (data) != 'FormData') {
            var fd = new FormData();
            for(var elem in data) {
                console.log(elem + ": " + data[elem], "YAY")
                fd.append(elem, data[elem]);
            }
        }
        this.data = data;
        return this;
    }

    post(url, data) {
        console.log(url, "URL");
        return this.setMethod('POST')
            .setData(data)
            .fetch();
    }

    fetch(url = "", init = {}) {
        if (typeof (url) == 'undefined') {
            const url = this.url;
        }
        if (typeof (init) == 'undefined') {
            const init = {method: this.method, headers: this.headers};
            if (this.data) {
                init.body = data;
            }
        }
        return fetch('/ajax/' + url.split('/').filter(val => val.length).join('/'), init);
    }

}
