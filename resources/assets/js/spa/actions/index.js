import api from '../api';
import * as types from '../constants/ActionTypes'

const API = new api();

function receiveAccountSettings(settings) {
  return {
    type: types.ACCOUNT_RECEIVE_SETTINGS,
    settings: settings
  }
}

export function getAccountSettings() {
    return dispatch => {
        API.fetch('/account/settings')
           .then(response => response.json())
           .then(json => json.data.settings)
           .then(settings => dispatch(receiveAccountSettings(settings)))
           .catch(err => { console.error(err); throw err; })
    }
}

export function persistAccountSettings(settings) {
    return {
        types: [
            types.ACCOUNT_PERSIST_SETTINGS,
            types.ACCOUNT_PERSIST_SETTINGS_SUCCESS,
            types.ACCOUNT_PERSIST_SETTINGS_FAIL
        ],
        payload: {
            request:{
                url:'/account/settings',
                method: 'post',
                data: settings,
            }
        }
    }
}

export function updateAccountSettings(settings) {
    return {
        type: types.ACCOUNT_UPDATE_SETTINGS,
        settings: settings
    }
}

export function loadProperties() {
    return {
        types: [
            types.PROPERTIES_LOAD,
            types.PROPERTIES_LOAD_SUCCESS,
            types.PROPERTIES_LOAD_FAIL
        ],
        payload: {
            request: {
                url: '/properties'
            }
        }
    }
}

export function validateNewProperty(data) {
    return {
        type: types.PROPERTIES_CREATE_VALIDATE,
        property: data
    }
}

export function createNewProperty(property) {
    return {
        types: [
            types.PROPERTIES_CREATE,
            types.PROPERTIES_CREATE_SUCCESS,
            types.PROPERTIES_CREATE_FAIL
        ],
        payload: {
            request: {
                method: 'POST',
                url: '/properties',
                data: property
            }
        }
    }
}
