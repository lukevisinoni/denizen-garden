import React, {Component} from 'react'
import Card, {CardActions, CardHeader, CardMedia, CardText, CardTitle} from 'material-ui/Card'

class PropertyListItem extends Component {
    getSummary() {
        if (this.props.property.summary) {
            return this.props.property.summary
        } else {
            return this.props.property.description.substring(0, 200)
        }
    }
    render() {
        return (
            <div className="property-list-item">
                <Card>
                    <CardHeader title={this.props.property.title} subtitle={this.props.property.address_full}>
                    </CardHeader>
                    <CardText>
                        <p className="summary">{this.getSummary()}</p>
                    </CardText>
                </Card>
            </div>
        )
    }
}

export default PropertyListItem
