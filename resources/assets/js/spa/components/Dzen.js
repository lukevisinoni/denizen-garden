import React, {Component, PropTypes as P}  from 'react'
import ClassNames from 'classnames'
import Formsy from 'formsy-react'
import {FormsyText} from 'formsy-material-ui/lib'

class DzenSubmit extends Component {
    renderCancelIcon() {
        if (this.props.cancelIcon) {
            return <i className={"fa fa-" + this.props.cancelIcon}></i>
        }
    }
    renderCancelBtn() {
        if (this.props.cancel) {
            return <button className="btn btn-default" onClick={this.props.cancel}>{this.props.cancelText}</button>
        }
    }
    renderSubmitIcon() {
        if (this.props.submitIcon) {
            var iconClass = "fa-" + this.props.submitIcon;
            var cls = { 'fa': true, };
            cls[iconClass] = true;
            return <i className={ClassNames(cls)}></i>
        }
    }
    renderSubmitBtn() {
        return <button type="submit" disabled={this.props.isDirty} className={ClassNames({"btn": true, "btn-primary": true, "btn-loading": this.props.isDirty})} {...this.props}>{this.renderSubmitIcon()} {this.props.submitText}</button>
    }
    render() {
        return (
            <div className={ClassNames({'submit-button':true, 'pull-right': true})}>
                {this.props.children}
                {this.renderCancelBtn()}
                {this.renderSubmitBtn()}
            </div>
        );
    }
}

DzenSubmit.propTypes = {
    submitText: P.string,
    submitIcon: P.string,
    cancelText: P.string,
    cancelIcon: P.string,
    cancel: P.func
}
DzenSubmit.defaultProps = {
    submitIcon: "save",
    submitText: "Save",
    cancelText: "Cancel",
}

var DzenInput = React.createClass({
    mixins: [Formsy.Mixin],
    changeValue: function (event) {
        if(this.getErrorMessage() != null){
            this.setValue(event.currentTarget.value);
        }
        else{
            if (this.isValidValue(event.target.value)) {
                this.setValue(event.target.value);
            }
            else{
                this.setState({
                    _value: event.currentTarget.value,
                    _isPristine: false
                });
            }
        }
    },
    blurValue: function (event) {
        this.setValue(event.currentTarget.value);
    },
    keyDown: function (event){
        if(event.keyCode=='13'){
            this.setValue(event.currentTarget.value);
        }
    },
    getId: function() {
        return (this.props.id) ? this.props.id : this.props.name;
    },
    showSpinner: function() {
        return false;
    },
    showSuccess: function() {
        return false;
    },
    showWarning: function() {
        return false;
    },
    renderLabel: function() {
        // @todo Should I put this in a "function/dumb component"?
        if (this.props.label) {
            return <label htmlFor={this.getId()} className="form-control-label">{this.props.label}</label>
        }
    },
    renderHelpText: function() {
        if (this.getErrorMessage()) {
            return <small className="help-text" style={{color: "#c00"}}>{this.getErrorMessage()}</small>;
        }
    },
    renderOldInput: function() {
        var cls = ClassNames({
            'form-control': true,
            'required': this.showRequired(),
            'error': this.showError(),
            'form-control-warning': this.showWarning(),
            'form-control-danger': this.showError(),
            'form-control-success': this.showSuccess(),
            'form-control-spinner': this.showSpinner()
        });
        return (
            <FormsyText
                type={this.props.type}
                name={this.props.name}
                id={this.getId()}
                onBlur={this.blurValue}
                onKeyDown={this.keyDown}
                onChange={this.changeValue}
                value={this.getValue()}
                defaultValue={this.props.defaultValue}
                placeholder={this.props.placeholder}
                className={cls}
            />
        );
    },
    renderInput: function() {
        return
    },
    render: function () {
        var cls = ClassNames({
            'form-group': true,
            'required': this.showRequired(),
            'error has-danger': this.showError()
        });
        console.log(this.props, 'DzenInput PROPS')
        return (
            <div className={cls}>
                <FormsyText formNoValidate {...this.props} defaultValue={this.props.value} />
            </div>
        );
    }
});

var DzenHiddenInput = React.createClass({
    mixins: [Formsy.Mixin],
    changeValue: function (event) {
        if(this.getErrorMessage() != null){
            this.setValue(event.currentTarget.value);
        }
        else{
            if (this.isValidValue(event.target.value)) {
                this.setValue(event.target.value);
            }
            else{
                this.setState({
                    _value: event.currentTarget.value,
                    _isPristine: false
                });
            }
        }
    },
    blurValue: function (event) {
        this.setValue(event.currentTarget.value);
    },
    keyDown: function (event){
        if(event.keyCode=='13'){
            this.setValue(event.currentTarget.value);
        }
    },
    getId: function() {
        return (this.props.id) ? this.props.id : this.props.name;
    },
    showSpinner: function() {
        return false;
    },
    showSuccess: function() {
        return false;
    },
    showWarning: function() {
        return false;
    },
    render: function () {
        return (
            <div className="dzen-hidden-input">
                <input type="hidden" name={this.props.name} id={this.getId()} value={this.getValue()} {...this.props} />
            </div>
        );
    }
});

/*DzenInput.propTypes = {
    name: P.string.isRequired,
    type: P.oneOf(['text', 'password', 'email', 'number', 'date', 'color', 'range', 'month', 'week', 'time', 'datetime', 'datetime-local', 'search', 'tel', 'url']),
    id: P.string,
    label: P.string,
    help: P.arrayOf(P.string),
    errors: P.arrayOf(P.string),
    success: P.arrayOf(P.string),
    warnings: P.arrayOf(P.string),
    validating: P.bool
}
DzenInput.defaultProps = {
    type: 'text',
    errors: [],
    help: [],
    success: [],
    warnings: [],
    errors: [],
    validating: false,
}*/

class DzenForm extends Component {
    render() {
        return (
            <Formsy.Form className={ClassNames( 'form-' + this.props.formStyle )} {...this.props}>
                {this.props.children}
            </Formsy.Form>
        );
    }
}

DzenForm.propTypes = {
    formStyle: P.string
}
DzenForm.defaultProps = {
    formStyle: "horizontal"
}

export {DzenForm, DzenInput, DzenHiddenInput, DzenSubmit};
