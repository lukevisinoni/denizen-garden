import React, { Component } from 'react'
import {Link} from 'react-router'
import Card from '../components/Card'

class NotFound extends Component {
    render() {
        return (
            <Card title="Well this is embarrassing..." icon="error">
                <p className="alert alert-warning"><h4><i className="fa fa-warning danger" style={{color: "#800"}}></i> Sorry...</h4> The page you requested doesn't exist! This is almost certainly our fault. It's probably just a typo in our code or something. We'll try to get this fixed as soon as possible, but in the mean time why don't you head <Link to="/home">home</Link>?</p>
            </Card>
        )
    }
}

export default NotFound;
