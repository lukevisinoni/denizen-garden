import React, {Component, PropTypes as P} from 'react';
import Formsy from 'formsy-react'
import {FormsyText, FormsySelect} from 'formsy-material-ui/lib'
import MenuItem from 'material-ui/MenuItem'

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
class SignupForm extends Component {
    render() {
        return (
            <div className="signup-form row">
                <div className="col-md-12"><p className="alert alert-info"><strong>This is an info alert.</strong> It's meant to portray information that, while important, doesn't require immediate attention and does not indicate any sort of problem.</p></div>
                <div className="col-md-6">
                    <fieldset>
                        <legend>Try Denizen for 30 days free!</legend>
                        <Formsy.Form onSubmit={this.props.handleSubmit} onValidate={this.props.handleSubmit} onValid={this.props.handleOnValid} onInvalid={this.props.handleOnInvalid} validationErrors={this.props.validationErrors}>
                            <div className="form-group">
                                <FormsyText
                                    name="firstname"
                                    validations="maxLength:30"
                                    validationError="Please enter your first name"
                                    validatePristine={true}
                                    floatingLabelText="First Name"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <FormsyText
                                    name="lastname"
                                    validations="maxLength:50"
                                    validationError="Please enter your last name"
                                    validatePristine={true}
                                    floatingLabelText="Last Name"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <FormsyText
                                    name="email"
                                    autoComplete="off"
                                    validations="isEmail"
                                    validationError="Please enter a valid e-mail address"
                                    floatingLabelText="E-mail"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <FormsyText
                                    name="username"
                                    autoComplete="off"
                                    validations={{minLength: 3, maxLength: 20, isAlphanumeric: true}}
                                    validationErrors={{
                                        minLength: "Username must be at least 3 characters",
                                        maxLength: "Username cannot exceed 20 characters",
                                        isAlphanumeric: "Username must only contain letters and numbers"
                                    }}
                                    floatingLabelText="Username"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                            <div className="form-group">
                                <FormsyText
                                    name="password"
                                    type="password"
                                    validations={{matchRegexp: /^(?=.*\d)(?=.*[A-Z]).{8,}$/}}
                                    validationError="Password must be at least eight characters and contain at least one uppercase letter and one digit"
                                    floatingLabelText="Password"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                            {/* @todo is this a security risk? Sending password to updatevalue? */}
                            <div className="form-group">
                                <FormsyText
                                    name="password_confirmation"
                                    type="password"
                                    validations={{equalsField: 'password'}}
                                    validationError="Passwords must match"
                                    floatingLabelText="Password (again)"
                                    onBlur={this.props.handleUpdateValue}
                                    required
                                />
                            </div>
                        </Formsy.Form>
                        <p className="help-text muted"><small>*All fields are required</small></p>
                    </fieldset>
                </div>
                <div className="col-md-6">
                    <p className="description"><strong>Sign in using Google or Facebook!</strong> This is where I will include the option to sign in using Google or Facebook. I guess I'll put a login form here too. Take a look at how other apps do it and be "inspired".</p>
                </div>
            </div>
        )
    }
}

SignupForm.propTypes = {
    handleSubmit: P.func.isRequired,
    handleOnValid: P.func.isRequired,
    handleOnInvalid: P.func.isRequired,
    validationErrors: P.object,
}

SignupForm.defaultProps = {
    validationErrors: {}
}

export default SignupForm
