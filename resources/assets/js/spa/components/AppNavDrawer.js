import React, {Component, PropTypes} from 'react';
import Drawer from 'material-ui/Drawer';
import {List, ListItem, MakeSelectable} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import SubHeader from 'material-ui/SubHeader';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import {spacing} from '../mui-styles';
import {typography} from '../mui-styles';
import {zIndex} from '../mui-styles';
import {blueGrey50, blueGrey800, blueGrey900} from '../mui-styles/colors';

const SelectableList = MakeSelectable(List);

const styles = {
    logo: {
        cursor: 'pointer',
        fontSize: 24,
        color: typography.textFullWhite,
        lineHeight: `${spacing.desktopKeylineIncrement}px`,
        height: `${spacing.desktopKeylineIncrement}px`,
        fontWeight: typography.fontWeightLight,
        backgroundColor: blueGrey900,
        paddingLeft: spacing.desktopGutter,
        marginBottom: 8,
    },
    drawerContainer: {
        zIndex: zIndex.navDrawer - 100,
        backgroundColor: blueGrey800,
        color: blueGrey50
    },
    logoHeader: {
        lineHeight: `${spacing.desktopKeylineIncrement}px`,
        verticalAlign: "middle",
    },
    listItem: {
        color: blueGrey50
    },
    version: {
        paddingLeft: spacing.desktopGutterLess,
        fontSize: 16,
    },
};

class AppNavDrawer extends Component {
    static propTypes = {
        docked: PropTypes.bool.isRequired,
        location: PropTypes.object.isRequired,
        onChangeList: PropTypes.func.isRequired,
        onRequestChangeNavDrawer: PropTypes.func.isRequired,
        open: PropTypes.bool.isRequired,
        style: PropTypes.object,
    };

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    handleRequestChangeLink = (event, value) => {
        window.location = value;
    };

    handleTouchTapHeader = () => {
        this.context.router.push('/');
        this.props.onRequestChangeNavDrawer(false);
    };

    render() {
        const {
            location,
            docked,
            onRequestChangeNavDrawer,
            onChangeList,
            open,
            style,
        } = this.props;

        return (
            <Drawer
                style={style}
                docked={docked}
                open={open}
                onRequestChange={onRequestChangeNavDrawer}
                containerStyle={styles.drawerContainer} >
                <div style={styles.logo} onTouchTap={this.handleTouchTapHeader}>
                    <h1 style={styles.logoHeader}>Denizen Garden</h1>
                </div>
                <SelectableList value={location.pathname} onChange={onChangeList}>
                    <ListItem style={styles.listItem} primaryText="Home" value="/home" />
                </SelectableList>
                <Divider />
                <SelectableList value={location.pathname} onChange={onChangeList}>
                    <ListItem
                        primaryText="My Account"
                        primaryTogglesNestedList={true}
                        style={styles.listItem}
                        nestedItems={[
                            <ListItem style={styles.listItem} primaryText="Settings" value="/account" />,
                        ]}
                    />
                </SelectableList>
                <SelectableList value={location.pathname} onChange={onChangeList}>
                    <ListItem
                        primaryText="Properties"
                        primaryTogglesNestedList={true}
                        style={styles.listItem}
                        nestedItems={[
                            <ListItem style={styles.listItem} primaryText="My Properties" value="/properties" />,
                        ]}
                    />
                </SelectableList>
            </Drawer>
        );
    }
}

export default AppNavDrawer;
