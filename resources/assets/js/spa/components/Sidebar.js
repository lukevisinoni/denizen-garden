import React from 'react'
import {Link} from 'react-router'
import SubHeader from 'material-ui/SubHeader';
import {List, ListItem} from 'material-ui/List';
// import  from 'material-ui/ListItem';
//import '../sass/sidebar.scss'

const Sidebar = (props) =>
    <nav>
        <List>
            <ListItem className="list-item"><Link to="/home" activeClassName="active">Dashboard</Link></ListItem>
            <SubHeader><h2>My Account</h2></SubHeader>
            <ListItem className="list-item"><Link to="/account" activeClassName="active">Account Settings</Link></ListItem>
        </List>
    </nav>

export default Sidebar
