import React, {PropTypes as P} from 'react'

const Card = React.createClass({
    propTypes: {
        actions: P.arrayOf(P.node),
        title: P.string,
        icon: P.string
    },
    getDefaultProps: function() {
        return {
            icon: "",
            actions: [],
        }
    },
    renderPageTitle: function() {
        var iconHtml;
        if (this.props.icon) {
            iconHtml = <i className={"fa fa-" + this.props.icon}> </i>;
        }
        return (
            <h1 className="page-title">
                {iconHtml} {this.props.title}
            </h1>
        );
    },
    renderActions: function() {
        if (this.props.actions.length) {
            return (
                <div className="pull-right actions">
                    {this.props.actions.map(action => action)}
                </div>
            )
        }
    },
    render: function() {
        return (
            <div className="card">
                <div className="card-header">
                    {this.renderActions()}
                    {this.renderPageTitle()}
                </div>
                <div className="card-block">
                    {this.props.children}
                </div>
            </div>
        );
    }
});

export default Card;
