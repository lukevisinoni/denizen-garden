import React, {PropTypes as P} from 'react'
import AutoComplete from 'material-ui/AutoComplete'
import SmartyStreets from '../api/SmartyStreets'
import Formsy from 'formsy-react'

const AddressAutocomplete = React.createClass({
    statics: {
        ss: new SmartyStreets
    },
    propTypes: {
        label: P.string.isRequired,
        maxSearchResults: P.number,
        handleSelectItem: P.func.isRequired,
        addressItem: P.object,
        errors: P.arrayOf(P.string)
    },
    getDefaultProps() {
        return {
            maxSearchResults: 7,
            errors: [],
            addressItem: {}
        }
    },
    getInitialState() {
        return {
            addressList: []
        }
    },
    handleInput(searchText, dataSource) {
        if (searchText.length < 5) return;
        AddressAutocomplete.ss.autocomplete(searchText)
            .then(results => results.suggestions)
            .then(suggestions => {
                var items = [];
                for (var index in suggestions) {
                    var results = suggestions[index];
                    items.push({...results, value: results.text});
                }
                this.setState({addressList: items})
            })
            .catch((err) => { throw err; });
    },
    render() {
        // @todo Try fuzzy filter on autocomplete
        return (
            <span className="address-autocomplete">
                <AutoComplete
                    floatingLabelText={this.props.label}
                    maxSearchResults={this.props.maxSearchResults}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSource={this.state.addressList}
                    onNewRequest={this.props.handleSelectItem}
                    onUpdateInput={this.handleInput}
                    errorText={this.props.errors.join(', ')}
                />
            </span>
        )
    }
});

export default AddressAutocomplete;
