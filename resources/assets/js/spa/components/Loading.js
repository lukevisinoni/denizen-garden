import React from 'react'
import LinearProgress from 'material-ui/LinearProgress'
import {lightGreen800} from '../mui-styles/colors';

const Loading = React.createClass({
    render: function() {
        return (
            <LinearProgress color={lightGreen800} />
        );
    }
});

export default Loading;
