import React, {Component, PropTypes as P} from 'react';
import Formsy from 'formsy-react'
import {FormsyText, FormsySelect} from 'formsy-material-ui/lib'
import MenuItem from 'material-ui/MenuItem'
import AddressAutocomplete from './AddressAutocomplete'

/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
class PropertyCreateForm extends Component {
    renderZipCodeField() {
        if (this.props.zipcodes.length) {
            return (
                <FormsySelect name="address_zip" floatingLabelText="Zip Code" onBlur={this.props.handleUpdateValue} required>
                    {this.props.zipcodes.map(zip => {
                        return <MenuItem value={zip.zipcode} key={zip.zipcode} primaryText={zip.zipcode} />;
                    })}
                </FormsySelect>
            )
        }
    }
    render() {
        return (
            <div className="property-create-dialog">
                <Formsy.Form onSubmit={this.props.handleSubmit} onValidate={this.props.handleSubmit} onValid={this.props.handleOnValid} onInvalid={this.props.handleOnInvalid} validationErrors={this.props.validationErrors}>
                    <div className="form-group">
                        <FormsyText
                            name="title"
                            validations={{minLength: 2, maxLength: 60}}
                            validationError="Please provide a property name"
                            floatingLabelText="Property Name"
                            onBlur={this.props.handleUpdateValue}
                            required
                        />
                    </div>
                    <div className="form-group">
                        {/* @todo I had really hoped these two would just
                            automatically line up nicely next to each other and
                            I wouldn't have to do anything special to make it
                            look nice, but they decided to be Ricky the Retard */}
                        <AddressAutocomplete
                            label="Address"
                            handleSelectItem={this.props.handleUpdateAddress}
                            addressItem={this.props.addressItem}
                            errors={this.props.addressErrors}
                        />
                        {this.renderZipCodeField()}
                    </div>
                    <div className="form-group">
                        <FormsyText
                            name="address_line2"
                            floatingLabelText="Address Line 2"
                            onBlur={this.props.handleUpdateValue}
                        />
                    </div>
                </Formsy.Form>
            </div>
        )
    }
}

PropertyCreateForm.propTypes = {
    handleSubmit: P.func.isRequired,
    handleOnValid: P.func.isRequired,
    handleOnInvalid: P.func.isRequired,
    handleUpdateValue: P.func.isRequired,
    handleUpdateAddress: P.func.isRequired,
    addressItem: P.object,
    addressErrors: P.arrayOf(P.string),
    validationErrors: P.arrayOf(P.string),
    zipcodes: P.arrayOf(P.object)
}

PropertyCreateForm.defaultProps = {
    addressErrors: [],
    zipcodes: [],
    validationErrors: {},
    addressItem: null
}

export default PropertyCreateForm
