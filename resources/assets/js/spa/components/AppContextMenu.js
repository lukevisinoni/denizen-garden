import React, {Component, PropTypes as P} from 'react'
import Avatar from 'material-ui/Avatar'
import IconMenu from 'material-ui/IconMenu'
import {ListItem} from 'material-ui/List'
import IconButton from 'material-ui/IconButton'
import Divider from 'material-ui/IconButton'

const style = {margin: 5, fontSize: "12px"}

class AppContextMenu extends Component {
    render() {
        return (
            <div className="app-context-menu">
                <IconMenu onItemTouchTap={this.props.handleOnRequestChange} iconButtonElement={<IconButton><Avatar size={30} style={style} anchorOrigin={{horizontal: 'right', vertical: 'top'}} targetOrigin={{horizontal: 'right', vertical: 'top'}} src="/img/icons/profile.png" /></IconButton>}>
                    <ListItem value="#" primaryText="My Profile" />
                    <Divider />
                    <ListItem value="/logout" primaryText="Log Out" />
                </IconMenu>
            </div>
        )
    }
}

export default AppContextMenu
