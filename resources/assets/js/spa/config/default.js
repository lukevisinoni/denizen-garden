/*
|--------------------------------------------------------------------------
| Denizen SPA Configuration
|--------------------------------------------------------------------------
|
| This is the global configuration for the Denizen SPA. You can get and set
| configuration values throughout the application using config.get() and
| config.set(). The get method does not provide a default parameter and will
| throw an exception for undefined directives.
|
| For full config docs, see: https://github.com/lorenwest/node-config
*/

// You can define a callback that sets a config directive to whatever the
// return values is using "defer". Would be useful for say... error messages
// var defer = require('config/defer').deferConfig;

// @todo I can't use this STUPID MOTHERFUCKING PIECE OF SHIT config library
// because no matter what the mother fuck I do, it just keeps finding ITS
// MOTHERFUCKING self!! Instead of finding my config directory it keeps finding
// ITS OWN STUPID base directory because it's called "config". Stupid!!
// https://github.com/lorenwest/node-config/issues/330

module.exports = {
    // @todo Now that I have a proper config file, go back to your smartystreets
    // clsas and actually use these configuration directives
    smartystreets: {
        endpoint: '//autocomplete-api.smartystreets.com/suggest',
        authId: 'df4dc906-fe6e-362c-e15f-c09e5dfe70d4',
        authToken: '3wanOB3jK2YP8PCOAji8',
        jsonpParam: 'callback',
        key: '6297205615327880',
    },
    amazon: {
        region: 'us-east-1',
        IdentityPoolId: 'us-east-1:394558e5-e446-4f6d-be39-2b130db1dd2b',
        appId: '47k11t14ipu5m6p7n9ft94k49a'
    }
}
