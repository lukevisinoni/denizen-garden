import { createStore, applyMiddleware } from 'redux'
import { syncHistoryWithStore} from 'react-router-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import reducer from './reducers'
import { browserHistory } from 'react-router'
import axios from 'axios'
import axiosMiddleware from 'redux-axios-middleware'

const client = axios.create({ //all axios can be used, shown in axios documentation
    baseURL:'/ajax/',
    responseType: 'json',
    headers: {'X-Requested-With': 'XMLHttpRequest', 'Accept': 'application/json'},
    progress: event => {
        console.log(event, "PROGRESS EVENT...")
    },
    transformResponse: [function (data) {
        if (data.status == "success") {
            return {...data.data, status: data.status};
        } else if("message" in data) {
            return {_error: data.message};
        } else {
            // throw exception?
            // @todo Handle this more gracefully
            return {_error: "Request failed"}
        }
    }]
});

const middleware = process.env.NODE_ENV === 'production' ?
  [ thunk, axiosMiddleware(client) ] :
  [ thunk, axiosMiddleware(client), logger() ]

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

export const history = syncHistoryWithStore(browserHistory, store);
export default store;
