@extends('layouts.guest')

@section('title')
    Login
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="card content login">
                <div class="card-header">
                    <div class="pull-right register">
                        <a href="{{ route("auth.register") }}">Register</a>
                    </div>
                    <h2 class="page-title"><i class="fa fa-sign-in"></i> Login</h2>
                </div>
                <div class="card-block">
                    <form id="login-form" class="form-vertical" role="form" method="POST" action="{{ route('auth.do_login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="email">E-Mail Address</label>

                            <div class="form-input">
                                <input type="email" class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}" id="email" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <small class="text-help">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="password">Password</label>

                            <div class="form-input">
                                <input type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" id="password" name="password">

                                @if ($errors->has('password'))
                                    <small class="text-help">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-input">
                                <div class="checkbox-inline">
                                    <label>
                                        <input class="checkbox" type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-link" href="{{ route('password.reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
