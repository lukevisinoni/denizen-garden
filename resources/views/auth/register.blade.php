@extends('layouts.guest')

@section('title')
    Register
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 main">
            <div class="card content register">
                <div class="card-header">
                    <div class="pull-right login">
                        <a href="{{ route("auth.login") }}">Login</a>
                    </div>
                    <h2 class="page-title"><i class="fa fa-user-plus"></i> Register</h2>
                </div>
                <div class="card-block">
                    <form class="form-vertical" role="form" method="POST" action="{{ route('auth.do_register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label class="form-control-label">Name</label>

                            <div class="form-input">
                                <input type="text" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <small class="text-help">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label class="form-control-label">E-Mail Address</label>

                            <div class="form-input">
                                <input type="email" class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <small class="text-help">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label class="form-control-label">Password</label>

                            <div class="form-input">
                                <input type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password">

                                @if ($errors->has('password'))
                                    <small class="text-help">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                            <label class="form-control-label">Confirm Password</label>

                            <div class="form-input">
                                <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' form-control-danger' : '' }}" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <small class="text-help">{{ $errors->first('password_confirmation') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group form-submit">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user-plus"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
