<script>
    var Denizen = Denizen || {
        csrfToken: '{{ csrf_token() }}',
        routes: {
            'account.settings': '{{ route('ajax.account.settings') }}'
        },
        getRoute: function(name, args) {
            if (this.routes[name]) {
                return this.routes[name];
            } else {
                throw "Sorry no route named " + name;
            }
        }
    };
    window.Denizen = Denizen;
</script>
