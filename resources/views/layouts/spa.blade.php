<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('title') | {{ config('app.title') }}</title>
        <link href="{{ asset("/css/admin.css") }}" rel="stylesheet">
        @include('layouts._denizenjs')
    </head>
    <body>
        <div id="denizen">
            <noscript>
                <div class="alert alert-danger">
                    <h1 class="page-title">Sorry, Grandpa!</h1>
                    <p>It looks like it may be time for you to join the rest of us in the 21st century and get yourself one of them new-fangled javascript-enabled browsers.</p>
                </div>
            </noscript>
        </div>
        <script src="{{ asset('/js/vendor/big-integer/jsbn.js') }}"></script>
        <script src="{{ asset('/js/vendor/big-integer/jsbn2.js') }}"></script>
        <script src="{{ asset('/js/vendor/sjcl.js') }}"></script>
        <script src="{{ asset('/js/vendor/moment.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/aws-cognito-sdk.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/amazon-cognito-identity.min.js') }}"></script>
        <script src="{{ asset('/js/vendor/aws-sdk.min.js') }}"></script>
        <script src="{{ asset('/js/app/spa.bundle.js') }}"></script>
    </body>
</html>
