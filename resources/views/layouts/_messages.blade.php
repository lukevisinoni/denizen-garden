{{-- @todo Eventually I might implement these as growl-style alerts instead --}}

@if(Session::has("success"))
    <div class="alert alert-success"><i class="fa fa-thumbs-up alert-icon"></i> <span class="alert-text">{{ Session::get('success') }}</span></div>
@elseif(Session::has("info"))
    <div class="alert alert-info"><i class="fa fa-info alert-icon"></i> <span class="alert-text">{{ Session::get('info') }}</span></div>
@elseif(Session::has("warning"))
    <div class="alert alert-warning"><i class="fa fa-warning alert-icon"></i> <span class="alert-text">{{ Session::get('warning') }}</span></div>
@elseif(Session::has("danger"))
    <div class="alert alert-danger"><i class="fa fa-danger alert-icon"></i> <span class="alert-text">{{ Session::get('danger') }}</span></div>
@endif