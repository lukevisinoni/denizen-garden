@inject('debugbar', 'Barryvdh\Debugbar\LaravelDebugbar')

@if ($debugbar->isEnabled())
    
    {{-- @todo It might be better to put this swifty debug data download stuff into some kind of helper
               Also, I decided to take it out of the version method in my gulpfile because I don't imagine
               it will ever change. Plus, I'm the only guy who will ever likely use it.
    <script src="{{ elixir('js/swifty.debugutils.js') }}"></script> --}}
    <script src="{{ asset('/js/swifty.debugutils.js') }}"></script>
    <script>
        $(function(){
        
            var SwiftyDownloadButton = new Swifty.utils.DownloadLinkIndicator({ tooltip: "Download", icon: "download", href: "{{ route('swifty.debugbar.download', ['request_id' => $debugbar->getCurrentRequestId()]) }}" })
            phpdebugbar.addIndicator('download', SwiftyDownloadButton, 'right');
        
        });        
    </script>

@endif