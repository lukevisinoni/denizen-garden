<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') | {{ config('app.title') }}</title>
    <link href="{{ asset("/css/admin.css") }}" rel="stylesheet">
    @include('layouts._swiftycsrf')
  </head>

  <body>

    <nav class="navbar navbar-fixed-top top">
      <div class="row">
          <div class="col-md-2 logo">
              <a href="{{ route("dashboard.admin") }}" id="logo" class="navbar-brand dzg">Deni<strong>Zen Garden</strong></a>
          </div>
          <div class="col-md-10 topnav">
              <ul class="nav navbar-nav pull-xs-right">
                {{--
                <li class="home nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="features nav-item">
                  <a class="nav-link" href="#">Features</a>
                </li>
                <li class="pricing nav-item">
                  <a class="nav-link" href="#">Pricing</a>
                </li>
                --}}
                <li class="help nav-item"><a class="nav-link" href="#"><i class="fa fa-btn fa-question-circle"></i> {{-- <span class="text">Help</span> --}}</a></li>

                <li class="user nav-item dropdown">
                    <a href="#" id="UDLabel" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="UDLabel">
                        <li class="personal-info"><a class="dropdown-item" href="{{ route('account.settings') }}"><i class="fa fa-btn fa-cog"></i> Account Settings </a></li>
                        <li class="logout"><a class="dropdown-item" href="{{ route('auth.logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                    </ul>
                </li>

              </ul>
              {{-- @todo I'll put this back when I implement search
              <form class="form-inline pull-xs-right m-r-1">
                <input class="form-control" type="text" placeholder="Search">
                <!-- <button class="btn btn-success-outline" type="submit">Search</button> -->
              </form>
              --}}
          </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">

          @yield("sidebar")


            {{--<ul class="nav nav-sidebar">--}}
              {{--<li class="heading"><h2>Another thing</h2></li>--}}
              {{--<li><a href="">Nav item</a></li>--}}
              {{--<li><a href="">Nav item again</a></li>--}}
              {{--<li><a href="">One more nav</a></li>--}}
              {{--<li><a href="">Another nav item</a></li>--}}
              {{--<li><a href="">More navigation</a></li>--}}
            {{--</ul>--}}
            {{--<ul class="nav nav-sidebar">--}}
              {{--<li class="heading"><h2>This is fun</h2></li>--}}
              {{--<li><a href="">Nav item again</a></li>--}}
              {{--<li><a href="">One more nav</a></li>--}}
              {{--<li><a href="">Another nav item</a></li>--}}
            {{--</ul>--}}

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 admin-panel main">

            @include("layouts._messages")
            @yield("content")

            <div class="copy">&copy Copyright 2016 <a href="{{ route("page.welcome") }}" class="dzg">Deni<strong>Zen Garden</strong></a>. All rights reserved.</div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    {{-- @todo Make sure this is the right version of jquery for bootstrap
    <script>window.jQuery || document.write('<script src="{{ asset("/js/vendor/jquery.js") }}"><\/script>')</script> --}}
    <script src="{{ asset('/js/vendor/jquery.js') }}"></script>
    <script src="{{ asset('/js/vendor/bootstrap.js') }}"></script>
    <script src="{{ asset('/js/admin.js') }}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('/js/vendor/ie10-viewport-bug-workaround.js') }}"></script>

    @include("layouts._swiftyjsdownload")
  </body>
</html>
