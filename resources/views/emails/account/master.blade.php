Hello {{ $user->name }},
We have created a master admin account for you with the following credentials:

Username: {{ $user->email }}
Password: {{ $password }}

We encourage you to change your password after you log in.

Signed,
Luke Visinoni
Founder - DeniZen Garden
