Hello {{ $account->name }},
Your new DeniZen account has been created! Welcome! You may find your new portal at {{ $account->getPortalUrl() }}

Signed,
Luke Visinoni
Founder - DeniZen Garden
