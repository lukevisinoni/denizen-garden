@extends('layouts.account')

@section('title')
    Edit Profile
@endsection

@section('sidebar')
    @include('account._sidebar', ['active' => 'account.profile.edit'])
@endsection

@section('content')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header"><h1 class="page-title"><i class="fa fa-user"></i> My Profile</h1></div>
            <div class="card-block">
                <form class="form-horizontal" method="POST" action="{{ route('account.profile.do_edit') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <div class="col-md-3">
                            <label class="form-control-label" for="name">Name</label>
                        </div>

                        <div class="form-input col-md-9">
                            <input type="name" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" id="name" name="name" value="{{ old('name', $user['name']) }}">

                            @if ($errors->has('name'))
                                <small class="text-help">{{ $errors->first('name') }}</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group buttons">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i> Save
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
