@extends('layouts.spa')

@section('title')
    Account Settings
@endsection

@section('sidebar')
    @include('account._sidebar', ['active' => 'account.settings'])
@endsection

@section('content')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header"><h1 class="page-title"><i class="fa fa-cog"></i> Account Settings</h1></div>
            <div class="card-block">

                <form class="form-horizontal" method="POST" action="{{ route('account.settings.do_edit') }}" id="account-settings">

                    {!! csrf_field() !!}

                    <fieldset>
                        <legend>Company Info</legend>
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <div class="col-md-3">
                                <label class="form-control-label" for="name">Name</label>
                            </div>

                            <div class="form-input col-md-9">
                                <input type="name" class="form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" id="name" name="name" value="{{ old('name', $account['name']) }}">

                                @if ($errors->has('name'))
                                    <small class="text-help">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('website') ? ' has-danger' : '' }}">
                            <div class="col-md-3">
                                <label class="form-control-label" for="website">Website</label>
                            </div>

                            <div class="form-input col-md-9">
                                <input type="website" class="form-control{{ $errors->has('website') ? ' form-control-danger' : '' }}" id="website" name="website" value="{{ old('website', $account['website']) }}">

                                @if ($errors->has('website'))
                                    <small class="text-help">{{ $errors->first('website') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <div class="col-md-3">
                                <label class="form-control-label" for="email">E-mail</label>
                            </div>

                            <div class="form-input col-md-9">
                                <input type="email" class="form-control{{ $errors->has('email') ? ' form-control-danger' : '' }}" id="email" name="email" value="{{ old('email', $account['email']) }}">

                                @if ($errors->has('email'))
                                    <small class="text-help">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group buttons">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i> Save
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
