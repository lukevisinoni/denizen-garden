@extends('layouts.account')

@section('title')
    Edit Profile
@endsection

@section('sidebar')
    @include('account._sidebar', ['active' => 'account.settings.password'])
@endsection

@section('content')

    <div class="col-md-8">
        <div class="card">
            <div class="card-header"><h1 class="page-title"><i class="fa fa-lock"></i> Change my password</h1></div>
            <div class="card-block">
                <form class="form-horizontal" method="POST" action="{{ route('account.settings.do_password') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('password_old') ? ' has-danger' : '' }}">
                        <div class="col-md-3">
                            <label class="form-control-label" for="password_old">Old Password</label>
                        </div>

                        <div class="form-input col-md-9">
                            <input type="password" class="form-control{{ $errors->has('password_old') ? ' form-control-danger' : '' }}" id="password_old" name="password_old">

                            @if ($errors->has('password_old'))
                                <small class="text-help">{{ $errors->first('password_old') }}</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                        <div class="col-md-3">
                            <label class="form-control-label" for="password">New Password</label>
                        </div>

                        <div class="form-input col-md-9">
                            <input type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" id="password" name="password">

                            @if ($errors->has('password'))
                                <small class="text-help">{{ $errors->first('password') }}</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_verify') ? ' has-danger' : '' }}">
                        <div class="col-md-3">
                            <label class="form-control-label" for="password_verify">New Password (again)</label>
                        </div>

                        <div class="form-input col-md-9">
                            <input type="password" class="form-control{{ $errors->has('password_verify') ? ' form-control-danger' : '' }}" id="password_verify" name="password_verify">

                            @if ($errors->has('password_verify'))
                                <small class="text-help">{{ $errors->first('password_verify') }}</small>
                            @endif
                        </div>
                    </div>

                    <div class="form-group buttons">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i> Save
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
