<ul class="nav nav-sidebar">
    <li @if($active == 'account.settings') class="active" @endif ><a href="{{ route('account.settings') }}">Account Settings @if($active == 'account.settings') <span class="sr-only">(current)</span> @endif</a></li>
    <li @if($active == 'account.profile.edit') class="active" @endif ><a href="{{ route('account.profile.edit') }}">My Profile @if($active == 'account.profile.edit') <span class="sr-only">(current)</span> @endif</a></li>
    <li @if($active == 'account.settings.password') class="active" @endif ><a href="{{ route('account.settings.password') }}">Change my password @if($active == 'account.settings.password') <span class="sr-only">(current)</span> @endif</a></li>
    <li><a href="#">Users</a></li>
    <li><a href="#">Domains</a></li>
</ul>
{{-- <ul class="nav nav-sidebar">
  <li><a href="">Nav item</a></li>
  <li><a href="">Nav item again</a></li>
  <li><a href="">One more nav</a></li>
  <li><a href="">Another nav item</a></li>
  <li><a href="">More navigation</a></li>
</ul>
<ul class="nav nav-sidebar">
  <li><a href="">Nav item again</a></li>
  <li><a href="">One more nav</a></li>
  <li><a href="">Another nav item</a></li>
</ul> --}}
