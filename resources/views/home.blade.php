@extends('layouts.spa')

@section('title')
    Home
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class=""> <!-- col-md-8 col-md-offset-2 -->
                <div class="card content welcome">
                    <div class="card-header">
                        <h1 class="page-header">Admin Dashboard</h1>
                    </div>
                    <div class="card-block">
                        <p class="lead">This is where the admin dashboard will go. It will have customizeable little "widgets" that display all the admin's most commonly-used functionality/reports/etc.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id lectus lacus. Praesent vehicula odio lobortis, placerat magna in, ultrices enim. In tempor, nisi sed vehicula tempor, erat eros ullamcorper sapien, volutpat dictum felis mauris eu orci. Mauris tincidunt diam aliquam eros iaculis, eu consectetur arcu malesuada. Cras dapibus eros enim, eget fringilla metus fermentum at. Vivamus pellentesque ac urna id mattis. Phasellus molestie elementum bibendum. Etiam luctus sem sit amet urna vulputate dictum. Sed eget laoreet elit, ac consectetur dui. Nunc cursus arcu et ex volutpat accumsan. Etiam nec vulputate mi, malesuada rutrum massa. Proin a erat ante. Nullam porttitor metus lacus, eget maximus justo faucibus eget. Phasellus ac tortor sit amet leo ultricies vestibulum ut eu urna. Phasellus eu dictum nisl, non maximus elit. Cras consequat aliquam nisl nec euismod.</p>
                        <p>Nunc pharetra, erat vitae consectetur vulputate, neque tellus congue mauris, sit amet dictum leo ante quis augue. Nullam eget leo urna. Nam mattis est et ante porta, non egestas purus blandit. Curabitur id feugiat dui, eu vehicula ante. Pellentesque fermentum porta augue, in ultrices mi euismod non. Etiam finibus sem id massa rhoncus rutrum. Sed vehicula lectus eu neque elementum, vitae suscipit arcu ultrices.</p>
                        <p>Nunc pharetra, erat vitae consectetur vulputate, neque tellus congue mauris, sit amet dictum leo ante quis augue. Nullam eget leo urna. Nam mattis est et ante porta, non egestas purus blandit. Curabitur id feugiat dui, eu vehicula ante. Pellentesque fermentum porta augue, in ultrices mi euismod non. Etiam finibus sem id massa rhoncus rutrum. Sed vehicula lectus eu neque elementum, vitae suscipit arcu ultrices.</p>
                        <p><a class="unsecure" href="{{ route('page.welcome') }}">Back to main site</a></p> <!-- just putting this here temporarily for testing purposes @todo I can use the copyright link in the footer now (plus I don't think I need this anymore. I was using it because I thought PHPBrowser started a new sessino when you used amOnUrl when in fact it was just not seeing Auth on the home page due to the way it was routed. -->
                        <h2>This is a heading</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id lectus lacus. Praesent vehicula odio lobortis, placerat magna in, ultrices enim. In tempor, nisi sed vehicula tempor, erat eros ullamcorper sapien, volutpat dictum felis mauris eu orci. Mauris tincidunt diam aliquam eros iaculis, eu consectetur arcu malesuada. Cras dapibus eros enim, eget fringilla metus fermentum at. Vivamus pellentesque ac urna id mattis. Phasellus molestie elementum bibendum. Etiam luctus sem sit amet urna vulputate dictum. Sed eget laoreet elit, ac consectetur dui. Nunc cursus arcu et ex volutpat accumsan. Etiam nec vulputate mi, malesuada rutrum massa. Proin a erat ante. Nullam porttitor metus lacus, eget maximus justo faucibus eget. Phasellus ac tortor sit amet leo ultricies vestibulum ut eu urna. Phasellus eu dictum nisl, non maximus elit. Cras consequat aliquam nisl nec euismod.</p>
                        <p>Nunc pharetra, erat vitae consectetur vulputate, neque tellus congue mauris, sit amet dictum leo ante quis augue. Nullam eget leo urna. Nam mattis est et ante porta, non egestas purus blandit. Curabitur id feugiat dui, eu vehicula ante. Pellentesque fermentum porta augue, in ultrices mi euismod non. Etiam finibus sem id massa rhoncus rutrum. Sed vehicula lectus eu neque elementum, vitae suscipit arcu ultrices.</p>
                        <h3>I really like headings</h3>
                        <p>Nam interdum dolor eget nulla tincidunt, a euismod urna egestas. Ut vulputate sem sit amet nulla feugiat fringilla. Curabitur volutpat dui quam, semper ultrices lectus facilisis ac. Vivamus ultricies neque ut commodo imperdiet. Vestibulum mi felis, ornare ac tortor sit amet, mollis eleifend quam. Ut feugiat lorem sit amet odio gravida feugiat. Aenean sit amet dui vulputate, iaculis ipsum ac, fermentum tortor. Nunc hendrerit nibh in metus ullamcorper semper. Integer in volutpat nunc. Vivamus vitae eros lacinia, luctus diam vitae, lacinia lacus. Etiam tincidunt mauris in metus placerat consectetur. Duis scelerisque accumsan tincidunt. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
