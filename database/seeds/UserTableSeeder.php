<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create initial user for testing
        //$user = Swift\User::create([
        //    'email' => 'luke.visinoni@swiftpropertymanager.com',
        //    'name' => 'Luke Visinoni',
        //    'password' => bcrypt('abc123')
        //]);
        //$user->save();
        
        // now create 50 random users
        // factory(Swift\User::class, 50)->create()->each(function($u) {
            // here you can define relationships... pretty nifty!
            // $u->posts()->save(factory(App\Post::class)->make());
        // });
    }
}
