<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Swift\Domain;

class AccountTableSeeder extends Seeder
{
    protected $accounts = [
        [
            'name' => 'Luke\'s Property Management',
            'short' => 'luke',
            'email' => 'luke@lukesprops.com',
            'website' => 'http://www.lukesprops.com',
        ],
        [
            'name' => 'North Valley Property Management',
            'short' => 'nvpm',
            'email' => 'mike@nvpm.net',
            'website' => 'http://www.nvpm.net',
        ],
        [
            'name' => 'Reliable Property Management',
            'short' => 'reliable',
            'email' => 'dave@reliableproperties.com',
            'website' => 'http://reliableproperties.com',
        ],
        [
            'name' => 'Ponderosa Property Management',
            'short' => 'ponderosa',
            'email' => 'carol@ponderosa.org',
            'website' => 'http://ponderosa.org',
        ],
    ];

    protected static $faker;

    protected static function getFaker() {
        if (!self::$faker) {
            $faker = Faker\Factory::create();
            $faker->seed(1234);
            self::$faker = $faker;
        }
        return self::$faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->accounts as $value) {
            $acct = Swift\Account::create($value);
            $acct->save();
            $this->createUsers($acct);
            $this->createDomains($acct);
            $this->createProperties($acct);
        }
        // now create 50 random accounts and their domain names
        factory(Swift\Account::class, 50)->create()->each(function($a) {

            $faker = self::getFaker();
            $this->createUsers($a);
            $this->createDomains($a, $faker->optional(0.1, true)->randomDigit, $faker->optional(0.3, false)->randomElement([1,2,3]));
            $this->createProperties($a);

        });
    }

    public function createUsers($acct)
    {
        $faker = self::getFaker();
        $user = factory(Swift\User::class)->make();
        $user->account_id = $acct->id;
        $user->email = strtolower(preg_replace("/[^a-z0-9]+/i", ".", $user->firstname)) ."@". $acct->short .".". config("app.domain");
        //$user->username = $faker->userName;
        //$user->firstname = $faker->firstName;
        //$user->lastname = $faker->lastName;
        //$user->password = 'abCD123!';
        $user->save();
    }

    public function createProperties($acct)
    {
        for ($i = 0; $i < mt_rand(3, 10); $i++) {
            $prop = factory(Swift\Property::class)->make();
            $prop->account_id = $acct->id;
            $prop->save();
        }
    }

    public function createDomains($a, $custPrimary = false, $more = false)
    {
        /*if ($more) for ($i = 0; $i < $more; $i++) {
            $d = factory(Swift\Domain::class)->make();
            if ($i == 0) $d->is_primary = 1;
            $a->domains()->save($d);
        }
        $is_primary = true;
        if ($custPrimary) {
            $is_primary = false;
        }

        $dom = new Swift\Domain([
            'name' => $a->short . "." . config('app.domain'),
            'is_primary' => $is_primary,
            'account_id' => $a->id
        ]);
        $dom->save();*/

    }
}
