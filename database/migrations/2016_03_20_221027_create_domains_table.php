<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Extra domain names for accounts
 * Every account gets a shortname.denizengarden.com domain. This is the domain that
 * all other domains ultimately resolve to (although sometimes they are masked and so
 * it looks like the domain is something else in the URI).
 *
 * YOU DO NOT NEED TO ADD short.denizengarden.com DOMAIN TO THE DB!!
 * This is only for custom domains provided by the customer
 */
class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name', 255);
            $table->boolean('is_primary')->default(false); // the primary domain is the domain the account-holder wants to show up in the URI no matter which domain is used to access the app (defaults to short.denizengarden.com)
            $table->timestamps();
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('domains');
    }
}
