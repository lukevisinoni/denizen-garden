<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade')->onUpdate('cascade');
            $table->string('title', 60);
            $table->string('summary')->nullable();
            $table->text('description')->nullable();
            $table->string('office_hours')->nullable();
            $table->string('phone', 10)->nullable(); // goes to account-holder if this isn't specified
            $table->string('email')->nullable(); // e-mails will go to this if specified, otherwise will go to manager (if specified) otherwise it will go to account contact e-mail
            $table->string('address_street', 65);
            $table->string('address_line2', 50)->nullable();
            $table->string('address_city', 65);
            $table->string('address_state', 2);
            $table->string('address_zip', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
