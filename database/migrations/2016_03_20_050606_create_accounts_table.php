<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
        
            $table->increments('id');
            $table->string('name', 35);
            $table->string('short', 15); // short name used for account's subdomain and occasionally as an alternate identifier
            $table->string('website', 256)->nullable();
            /* @todo Not adding these until they're necessary because I may end up putting them in a "company" table or something
            $table->string('phone', 10);
            $table->string('address', 75);
            $table->string('address2', 75);
            $table->string('city', 35);
            $table->string('state', 2);
            $table->string('postal_code', 9);
            */
            $table->softDeletes();
            $table->timestamps();
            $table->unique('short');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
    }
}
