<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('account_short', 15);
            $table->foreign('account_short')->references('short')->on('accounts')->onDelete('cascade')->onUpdate('cascade');
            $table->string('key', 25);
            $table->string('param', 25);
            $table->text('value');
            $table->timestamps();
            // can only set one param value per key per account
            $table->unique(['account_short', 'key', 'param']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configs');
    }
}
