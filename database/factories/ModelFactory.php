<?php

use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Swift\Account::class, function (Faker\Generator $faker) {

    $faker->seed(1234);
    $website = $faker->unique()->domainName;
    $chance = (boolean) $faker->optional(0.8)->randomDigit;
    return [
        'name' => $faker->company,
        'short' => preg_replace('/^([a-z0-9-]+)\.(com|info|net|org|biz|tv|io)$/i', '$1$2', strtolower($website)),
        'website' => ($chance) ? 'http://' . strtolower($website) : null,
        'email' => $faker->userName .'@'. $website,
        /*
        'phone' => $faker->randomNumber(10),
        'address' => $faker->streetAddress,
        'address2' => $faker->optional()->secondaryAddress,
        'city' => $faker->city,
        'state' => $faker->stateAbbr,
        'postalcode' => $faker->postcode,
        */
    ];

});
$factory->define(Swift\User::class, function (Faker\Generator $faker) {

    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->email,
        'username' => $faker->unique()->userName,
        'password' => 'abCD123!',
        'remember_token' => Str::random(60),
    ];

});
$factory->define(Swift\Domain::class, function (Faker\Generator $faker) {

    return [
        'name' => strtolower($faker->unique()->domainName),
        'is_primary' => false
    ];

});
$factory->define(Swift\Property::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->company,
        'summary' => $faker->boolean(40) ? $faker->realText(200, 1) : null,
        'description' => $faker->paragraphs($faker->numberBetween(1, 4), true),
        'office_hours' => $faker->boolean(90) ? $faker->randomElement(['Monday - Friday 8am-5pm', 'Mon-Thu 8am-4pm, Friday 9am-12pm, Sat/Sun Closed', 'Open every day from 8am to 2pm']) : null,
        'phone' => "530" . $faker->randomNumber(7),
        'email' => $faker->companyEmail,
        'address_street' => $faker->streetName . " " . $faker->streetSuffix,
        'address_line2' => $faker->boolean(30) ? $faker->secondaryAddress : null,
        'address_city' => $faker->city,
        'address_state' => $faker->stateAbbr,
        'address_zip' => $faker->postcode
    ];

});
