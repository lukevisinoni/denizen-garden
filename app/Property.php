<?php namespace Swift;

use Swift\Swifty\Model\Model;

class Property extends Model
{
    protected $fillable = [
        'title',
        'summary',
        'description',
        'office_hours',
        'phone',
        'email',
        'address_street',
        'address_line2',
        'address_city',
        'address_state',
        'address_zip'
    ];

    protected $appends = ['address_full'];

    public function scopeCust($query)
    {
        return $query->where('account_id', get_subdomain_account_id());
    }

    public function getAddressFullAttribute()
    {
        extract($this->attributes);
        return $this->attributes['address_full'] = "{$address_street} {$address_line2}, {$address_city}, {$address_state} {$address_zip}";
    }
}
