<?php namespace Swift;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = ['account_short', 'key', 'param', 'value'];

    public static function fetchKeys($cust)
    {
        return collect(self::select('key')
            ->where(['account_short' => $cust])
            ->orderBy('key')
            ->groupBy('key')
            ->get()
            ->toArray());
    }

    public static function fetchTree($cust, $cachemtime = null, $expand = true)
    {
        $where = [['account_short', $cust]];
        if (!is_null($cachemtime)) {
            $dt = Carbon::createFromTimestamp($cachemtime);
            $where[] = ['updated_at', '>', $dt->toDateTimeString()];
        }
        $params = collect(self::where($where)
            ->orderBy('key', 'asc') // @todo apparently orderBy can't accept multiple columns??
            ->get()
            ->toArray())
            ->groupBy('key')
            ->transform(function($item, $key) use ($expand) {
                $ret = [];
                foreach ($item as $arr) {
                    extract($arr);
                    if ($expand) {
                        array_set($ret, $param, $value);
                    } else {
                        $ret[$param] = $value;
                    }
                }
                return $ret;
            });
        return $params;
    }
}
