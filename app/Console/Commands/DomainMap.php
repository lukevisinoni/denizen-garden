<?php namespace Swift\Console\Commands;

use Swift\Console\Commands\Domain as DomainCommand;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Storage;
use Swift\Domain;
use Swift\Account;

class DomainMap extends DomainCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:map 
                            {--p|primary : Generate redirect map for non-primary to primary domains}
                            {--s|subdomain : Generate redirect map for custom domains to subdomain}
                            {--o|out= : The path to output the redirect maps}
                            {--f|format=txt : The domain map format (either txt or dbm)}
                            {--d|disk=local : Specify which storage configuration to use (see config/filesystems.php)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate one or all of the domain redirect maps';
    
    protected $maps;
    
    protected $accounts;
    
    protected $disk;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setDisk($this->option('disk'));
        $this->initMaps()
             ->generateMaps()
             ->outputMaps();
    }
    
    protected function getMaps()
    {
        if (is_null($this->maps)) { 
            $maps = [
                'primary' => "primary.map",
                'subdomain' => "subdomain.map"
            ];
            $expected = array_intersect_key($maps, array_filter($this->input->getOptions()));
            if (empty($expected) || $expected == $maps) {
                $this->maps = $maps;
            } else {
                $this->maps = $expected; // array_intersect_key($maps, $expected);
            }
        }
        return $this->maps;
    }
    
    protected function initMaps()
    {
        foreach ($this->getMaps() as $type => $file) {
            $this->disk()->put($file, $this->timestamp());
        }
        return $this;
    }
    
    protected function generateMaps()
    {
        foreach ($this->getMaps() as $type => $file) {
            $mapgen = "generate{$type}map";
            $this->$mapgen($file);
        }
        return $this;
    }
    
    /**
     * @todo The following two could be one function
     */
    protected function generatePrimaryMap($file)
    {
        foreach ($this->getAccounts() as $account) {
            if (count($account->domains) <= 1) continue;
            if (!$account->primaryDomain) {
                // @todo should log this
                $this->error("No primary domain set for {$account->name}");
                continue;
            }
            foreach ($account->domains as $dom) {
                if ($account->primaryDomain->name == $dom->name) continue;
                $this->disk()->append($file, "{$dom->name} {$account->primaryDomain->name}");
            }
        }
    }
    
    protected function generateSubdomainMap($file)
    {
        foreach ($this->getAccounts() as $account) {
            if (count($account->domains) <= 1) continue; 
            if (!$account->subDomain) {
                // @todo should log this
                $this->error("No subdomain set for {$account->name}");
                continue;
            }
            foreach ($account->domains as $dom) {
                if ($account->subDomain->name == $dom->name) continue;
                $this->disk()->append($file, "{$dom->name} {$account->short}");
            }
        }
    }
    
    protected function getOutputPath($file = null)
    {
        if (!$this->option('out')) return false;
        return rtrim(realpath($this->option('out')), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $file;
    }
    
    protected function outputMaps()
    {
        if (!$this->getOutputPath()) return false;
        foreach ($this->getMaps() as $type => $file) {
            $in = $this->getDiskPath($file);
            $out = $this->getOutputPath($file);
            if (is_link($out)) unlink($out);
            if (file_exists($out)) {
                if ($this->confirm("{$file} already exists in output path. Overwrite?")) {
                    unlink($out);
                }
            } 
            if ($this->option('format') == 'dbm') {
                // @todo Make it possible to specify where httxt2dbm can be found
                $cmd  = `/opt/httpd2.2/bin/httxt2dbm -i {$in} -o {$out}`;
            } else {
                // move the file to the output directory
                // $this->disk()->move($file, $this->getOutputPath("{$type}.map "));
                // just create a symlink here
                symlink($in, $out);
            }
        }
        return $this;
    }
}
