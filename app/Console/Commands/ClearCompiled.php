<?php namespace Swift\Console\Commands;

use Illuminate\Foundation\Console\ClearCompiledCommand;

class ClearCompiled extends ClearCompiledCommand
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the compiled class file(s)';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $compiledPath = $this->laravel->getCachedCompilePath();
        $servicesPath = $this->laravel->getCachedServicesPath();

        if (file_exists($compiledPath)) {
            @unlink($compiledPath);
        }

        if (file_exists($servicesPath)) {
            @unlink($servicesPath);
        }
    }
}
