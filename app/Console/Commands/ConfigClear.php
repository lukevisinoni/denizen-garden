<?php namespace Swift\Console\Commands;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Foundation\Console\ConfigClearCommand;

/**
 * Add options to clear for a specific account, set of accounts, or for all
 * If all, then you should be able to specify a blacklist.
 */

class ConfigClear extends ConfigClearCommand
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove configuration cache file(s)';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if ($this->option("global")) $this->files->delete($this->laravel->getGlobalCachedConfigPath());
        if (!$this->option("nocust")) {
            foreach(Finder::create()->files("config.php")->in(base_path() . "/cust/*/bootstrap/cache") as $file) {
                $this->files->delete($file);
            }
        }
        $this->info('Configuration cache cleared!');
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // name shortcut mode description default
            ['global', 'g', InputOption::VALUE_NONE, 'Delete the main config cache file (not associated with any account)'],
            ['nocust', 'c', InputOption::VALUE_NONE, 'Do not delete the account-specific cache files (so to run what used to be config:clear you would need `php artisan config:clear --global --nocust`)'],
            // @todo Implement this if you end up needing it
            // ['exclude', 'x', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'List any account names you do not want configuration caching for'],
        ];
    }
}
