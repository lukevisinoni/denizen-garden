<?php namespace Swift\Console\Commands;

use Swift\Account;
use Swift\Config;
use Swift\Swifty\Config\Aggregate\ArrayAggregate;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Filesystem\Filesystem;

use Illuminate\Foundation\Console\ConfigCacheCommand;

/**
 * Add options to generate for a specific account, set of accounts, or for all
 * If all, then you should be able to specify a blacklist.
 * Also need options for pulling config parameters from the database and for
 * combining all the directives from all sources
 */

class ConfigCache extends ConfigCacheCommand
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create cache file(s) for faster configuration loading';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->call('config:clear', ['--global' => $this->option("global"), '--nocust' => $this->option("nocust")]);

        $written = 0;

        if ($this->option("global")) {
            $global = $this->getFreshConfiguration();
            if ($this->files->put(
                $this->laravel->getGlobalCachedConfigPath(), '<?php return '.var_export($global, true).';'.PHP_EOL
            )) $written++;
        }

        if (!$this->option("nocust")) {
            $accounts = Account::withStatus(Account::STATUS_ACTIVE)->get();
            foreach ($accounts as $account) {
                $config = $this->getFreshConfiguration($account->short);
                if (!$this->files->isDirectory($dir = pathinfo($file = $this->laravel->getCustCachedConfigPath($account->short), PATHINFO_DIRNAME))) {
                    $this->files->makeDirectory($dir, 0755, true);
                }
                if ($this->files->put(
                    $file, '<?php return '.var_export($config, true).';'.PHP_EOL
                )) $written++;
            }
        }

        $this->info("{$written} config cache files written successfully!");
    }

    /**
     * Boot a fresh copy of the application configuration.
     *
     * @todo Make sure that this isn't pulling config values for "luke" for ALL
     *       customers
     * @todo And for that matter figure out a way to run artisan commands without
     *       needing an ACCT_NAME at all
     * @return array
     */
    protected function getFreshConfiguration($acct = null)
    {
        $app = require $this->laravel->bootstrapPath().'/app.php';
        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
        $agg = $app->getConfigAggregator($acct);
        if ($acct && !$this->option("nodb")) $agg->push(new ArrayAggregate(Config::fetchTree($acct)->toArray()));
        return $agg->toArray();
        // This WOULD return the correct config array but there's no way to tell it which customer to pull
        // return $app['config']->all();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // name shortcut mode description default
            ['nodb', null, InputOption::VALUE_NONE, 'Do not pull config params from the database'],
            ['global', 'g', InputOption::VALUE_NONE, 'Generate the main config cache file (not associated with any account)'],
            ['nocust', 'c', InputOption::VALUE_NONE, 'Do not write the account-specific cache files (so to run what used to be config:cache you would need `php artisan config:cache --global --nocust`)'],
            // @todo Implement this if you end up needing it
            // ['exclude', 'x', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'List any account names you do not want configuration caching for'],
        ];
    }
}
