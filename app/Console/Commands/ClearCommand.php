<?php namespace Swift\Console\Commands;

use Illuminate\Cache\Console\ClearCommand as BaseClearCommand;
use Illuminate\Cache\CacheManager;
use Symfony\Component\Console\Input\InputArgument;

/**
 * @todo I don't think I need this 
 * As it turns out, I don't think I'll end up needing this. Caching on a
 * per-account basis can be done purely through configuration directives. I'll
 * keep the file for now, but I don't think I'm going to need it.
 */

class ClearCommand extends BaseClearCommand
{

}
