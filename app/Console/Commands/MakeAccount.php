<?php namespace Swift\Console\Commands;

use Swift\Account;
use Illuminate\Console\Command;

class MakeAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:account {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new account';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $account = new Account();
        $account->short = $this->argument('id');
        $account->name = $this->ask("Enter the account name:");
        $account->email = $this->ask("Enter the account e-mail:");
        if ($this->confirm("Would you like to enter the account website? [y|n]")) {
            $account->website = $this->ask("Enter the account website (or press enter to leave blank):");
        }
        try {
            if ($account->save()) {
                $this->info("Account successfully created!");
                return 0;
            } else {
                $this->error("Account could not be created.");
                return 1;
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return 2;
        }
    }
}
