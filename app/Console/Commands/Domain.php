<?php namespace Swift\Console\Commands;

use Swift\Account;
use Illuminate\Console\Command;
use Storage;
use File;
use Carbon\Carbon;

abstract class Domain extends Command
{
    protected $defaults = [];
    
    protected $accounts;
    
    protected $disk = "local";

    public function setDisk($key)
    {
        if (!$this->getDiskPath()) {
            // @todo I'm not sure if this should spit out an error or do this
            throw new \Exception("Filesystem disk not found: " . $key);
        }
        $this->disk = $key;
    }
    
    protected function getDiskPath($file = null)
    {
        return rtrim(config('filesystems.disks.' . $this->option('disk') . '.root'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $file;
    }
    
    protected function disk()
    {
        return Storage::disk($this->disk);
    }
    
    protected function timestamp()
    {
        return "# generated automatically " . Carbon::now() . PHP_EOL;
    }
    
    /**
     * Retrieve all active accounts
     * So that I don't end up querying the DB more than once
     *
     * @return Collection of account models
     */
    protected function getAccounts()
    {
        if (is_null($this->accounts)) {
            $this->accounts = Account::all();
        }
        return $this->accounts;
    }
    
    protected function getOptionOrDefault($key, $default = null)
    {
        if ($option = $this->option($key)) {
            return $option;
        }
        return ($default) ?: array_key_exists($key, $this->defaults) ? $this->defaults[$key] : false;
    }
    
}
