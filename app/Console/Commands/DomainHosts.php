<?php namespace Swift\Console\Commands;

use Swift\Console\Commands\Domain as DomainCommand;
use Swift\Account;
use Storage;
use Illuminate\Console\Command;

/**
 * Local hosts file generator
 * $Id$
 */
class DomainHosts extends DomainCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domain:hosts
                            {--i|in= : Name of a file containing hosts that should be conserved}
                            {--o|out= : Path and filename to write hosts to}
                            {--a|all : If set, all domains (even local .dev) will be written to hosts file}
                            {--b|backup : If set, hosts file will be backed up to "hosts.bak" before writing}
                            {--d|disk=local : Filesystem disk key (see config/filesystems.php) to store local data}
                            {--f|flushdns : Flush DNS cache (need administrator privs)}
                            {--r|restartdns : Restart DNS (dnsmasq) (need administrator privs)}';
    
    protected $defaults = [
        'in' => '/etc/hosts~orig',
        'out' => '/etc/hosts',
        'local' => 'dzg_add_hosts',
        'backup-ext' => 'bak',
    ];
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate hosts file for local development';

    protected $accounts;
    
    /**
     * Create a new command instance.
     *
     * @return void 
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->setDisk($this->option('disk'));
        if ($this->option('backup')) {
            $this->backUpHosts($this->defaults['backup-ext']);
        }
        $this->generateHostsFile()
             ->importOriginalHosts()
             ->outputHostsFile();
        if ($this->option('flushdns')) {
            $this->flushDns();
        }
        if ($this->option('restartdns')) {
            $this->restartDns();
        }
    }
    
    protected function backUpHosts($filext = null)
    {
        if (is_null($filext)) $filext = $this->defaults['backup-ext'];
        $out = $this->getOptionOrDefault('out');
        if (file_exists($out)) {
            // back up hosts in laravel storage AND in its original directory
            $this->disk()->put("{$out}.{$filext}", file_get_contents($out));
            file_put_contents("{$out}.{$filext}", file_get_contents($out));
        } else {
            $this->info("Not necessary to back up {$out} -- file doesn't exist.");
        }
    }
    
    /**
     * Retrieve all active accounts
     * So that I don't end up querying the DB more than once
     *
     * @return Collection of account models
     */
    protected function getAccounts()
    {
        if (is_null($this->accounts)) {
            $this->accounts = Account::all();
        }
        return $this->accounts;
    }
    
    protected function getIp()
    {
        return "127.0.0.1";
    }
    
    protected function generateHostsFile()
    {
        $this->disk()->put($this->defaults['local'], "\n");
        foreach ($this->getAccounts() as $acct) {
            foreach ($acct->domains as $dom) {
                if (!$dom->isSubdomain() || $this->option("all")) {
                    $host = $this->getIp() . "\t{$dom->name}";
                    $this->disk()->append($this->defaults['local'], $host);
                }
            }
        }
        return $this;
    }
    
    protected function importOriginalHosts()
    {
        $orig = $this->getOptionOrDefault('in');
        if (file_exists($orig)) {
            $this->disk()->append($this->defaults['local'], file_get_contents($orig));
        } else {
            $this->info("Cannot import hosts from original hosts file {$orig} -- it does not exist.");
        }
        return $this;
    }
    
    protected function outputHostsFile()
    {
        $out = $this->getOptionOrDefault('out');
        if (file_exists($out)) unlink($out);
        // symlink(rtrim(config('filesystems.disks.' . $this->option('disk') . '.root'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->defaults['local'], $out);
        copy(rtrim(config('filesystems.disks.' . $this->option('disk') . '.root'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $this->defaults['local'], $out);
    }
    
    protected function flushDns()
    {
        `discoveryutil mdnsflushcache`;
        `discoveryutil udnsflushcaches`;
    }
    
    protected function restartDns()
    {
        `killall dnsmasq`;
        `/usr/local/sbin/dnsmasq`;
    }
}
