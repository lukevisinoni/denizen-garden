<?php namespace Swift;

use Swift\Swifty\Model\Model;

class Domain extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'is_primary'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['account_id',];
    
    public function account()
    {
        return $this->belongsTo('Swift\Account', 'account_id', 'id');
    }
    
    public function isPrimary()
    {
        return $this->is_primary;
    }
    
    public function isSubdomain()
    {
        return strpos($this->name, config('app.domain')) !== false;
    }
}
