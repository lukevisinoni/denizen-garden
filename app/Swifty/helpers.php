<?php 

if (!function_exists('with')) {
    function with($obj) { return $obj; }
}

function words($str)
{
    return preg_split(
        "/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/",
        $str, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
    );
}

/**
 * @todo 
 * Retrieve the account "short" name for customization per account. This value
 * is pulled from the subdomain (luke.denizengarden.com). The problem with this
 * is that when run in the cli, SERVER_NAME is not available. Some of the
 * acceptance tests fail because (I believe) PhpBrowser is run in the CLI and
 * because of that, this function doesn't work. I need to find a way to more
 * reliably pull this information.
 */
function get_account_custname()
{
    return env("ACCT_NAME", value(function(){
        // not really sure if I should use HTTP_HOST or SERVER_NAME here, so I'm going with the technically "safer" of the two
        if (array_key_exists('SERVER_NAME', $_SERVER)) {
            if (array_key_exists('ACCT_NAME', $_SERVER)) return $_SERVER['ACCT_NAME']; // @todo I put this here because letting it get to the next line caused errors... figure out why
            if(preg_match('/^([a-z0-9]+)\.denizengarden\.(com|dev)' /*preg_replace('/([\.-])/', "\\\\$1", config('app.domain'))*/ .'$/i', $_SERVER['SERVER_NAME'], $match)) { // had to hard-code domain beacause not doing so causes errors
                return $match[1];
            }
        }
        return false;
    }));
}

// @alias for get_account_custname()
function get_subdomain()
{
    return get_account_custname();
}

/**
 * Retreive the account associated with the subdomain we're on (if we're on one)
 */
function get_subdomain_account()
{
    return \Swift\Account::getCust();
}

/**
 * Retreive the account associated with the subdomain we're on (if we're on one)
 */
function get_subdomain_account_id()
{
    try {
        $account = get_subdomain_account();
    } catch (\Exception $e) {
        // @todo handle exception
        throw $e;
    }
    return $account->id;
}

/**
 * Return the path to the per-account customizations directory
 *
 * @param string Path to add to the return path
 * @param string Account "short" name (pulled from the SERVER_NAME if not specified)
 * @todo Unit test this
 * @todo SERVER_NAME not available in CLI
 */
function cust_path($path = null, $acct = null)
{
    if (!$cust = $acct ?: get_account_custname()) {
        throw new \Error("Unable to determine account name");
    }
    return base_path('cust'. DIRECTORY_SEPARATOR . $cust . DIRECTORY_SEPARATOR . trim($path, DIRECTORY_SEPARATOR));
}
