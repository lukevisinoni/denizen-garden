<?php namespace Swift\Swifty;

use Swift\Swifty\Config\ConfigAggregator;
use Swift\Swifty\Config\Aggregate\ArrayAggregate;
use Swift\Swifty\Config\Aggregate\DirAggregate;
use Swift\Config;
use Illuminate\Foundation\Application as LaravelApplication;

class Application extends LaravelApplication
{

    /**
     * Get the path to the bootstrap directory.
     *
     * @return string
     */
    public function bootstrapPath($acct = null)
    {
        return is_null($acct) ?
            $this->basePath.DIRECTORY_SEPARATOR.'bootstrap' :
            $this->getAccountCustPath('bootstrap', $acct);
    }

    /**
     * If there is a custom cache file (for the given account), return the path
     * to that. Otherwise, return the standard file path.
     *
     * @return string
     */
    protected function getDeterminedCacheFilePath($file, $acct = null)
    {
        $file = ltrim($file, DIRECTORY_SEPARATOR);
        if (file_exists($path = $this->getAccountCustPath('bootstrap'. DIRECTORY_SEPARATOR . $file, $acct))) {
            return $path;
        }
        return $this->bootstrapPath() . DIRECTORY_SEPARATOR . $file;
    }

    /**
     * Get the path to the account customization directory
     *
     * @return string
     * @throws Error if unable to determine account name
     */
    public function getAccountCustPath($path = null, $acct = null)
    {
        return cust_path($path, $acct);
    }

    public function getCustCachedConfigPath($acct)
    {
        return $this->getAccountCustPath('bootstrap/cache/config.php', $acct);
    }

    /**
     * Determine if the application configuration is cached.
     *
     * @return bool
     */
    // public function configurationIsCached()
    // {
    //     echo parent::configurationIsCached();
    //     exit;
    // }

    // public function getCachedConfigPath()
    // {
    //     return file_exists($custpath = $this->getCustCachedConfigPath(get_account_custname())) ?
    //         $custpath :
    //         parent::getCachedConfigPath();
    // }

    public function getGlobalCachedConfigPath()
    {
        return parent::getCachedConfigPath();
    }

    public function getCachedConfigPath()
    {
        if ($acct = get_account_custname()) {
            if (file_exists($custpath = $this->getCustCachedConfigPath($acct))) {
                return $custpath;
            }
        }
        return parent::getCachedConfigPath();
    }

    public function getConfigAggregator($acct = null, $nocust = false)
    {
        $config = new ConfigAggregator;
        $config->push(new DirAggregate(config_path()));
        if (!is_null($acct) && !$nocust) {
            if (is_dir($path = cust_path('config', $acct))) {
                $config->push(new DirAggregate($path));
            }
        }
        return $config;
    }

    /**
     * Get the path to the routes cache file.
     *
     * @return string
     */
    //public function getCachedRoutesPath()
    //{
    //    return $this->getDeterminedCacheFilePath('cache/routes.php');
    //}

    /**
     * Get the path to the cached "compiled.php" file.
     *
     * @return string
     */
    //public function getCachedCompilePath($acct = null)
    //{
    //    return $this->getDeterminedCacheFilePath('cache/compiled.php', $acct);
    //}

    /**
     * Get the path to the cached services.php file.
     *
     * @return string
     */
    //public function getCachedServicesPath()
    //{
    //    return $this->getDeterminedCacheFilePath('cache/services.php');
    //}

    /**
     * Register the core class aliases in the container.
     *
     * @return void
     */
    //public function registerCoreContainerAliases()
    //{
    //    $this->alias('app', 'Swift\Swifty\Application');
    //    parent::registerCoreContainerAliases();
    //}

    /**
     * Register the core class aliases in the container.
     *
     * @return void
     * @todo   I think I need to chance app's first index to this file... ?
     */
    public function registerCoreContainerAliases()
    {
        $aliases = [
            'app'                  => ['Swift\Swifty\Application', 'Illuminate\Foundation\Application', 'Illuminate\Contracts\Container\Container', 'Illuminate\Contracts\Foundation\Application'],
            'auth'                 => ['Illuminate\Auth\AuthManager', 'Illuminate\Contracts\Auth\Factory'],
            'auth.driver'          => ['Illuminate\Contracts\Auth\Guard'],
            'blade.compiler'       => ['Illuminate\View\Compilers\BladeCompiler'],
            'cache'                => ['Illuminate\Cache\CacheManager', 'Illuminate\Contracts\Cache\Factory'],
            'cache.store'          => ['Illuminate\Cache\Repository', 'Illuminate\Contracts\Cache\Repository'],
            'config'               => ['Illuminate\Config\Repository', 'Illuminate\Contracts\Config\Repository'],
            'cookie'               => ['Illuminate\Cookie\CookieJar', 'Illuminate\Contracts\Cookie\Factory', 'Illuminate\Contracts\Cookie\QueueingFactory'],
            'encrypter'            => ['Illuminate\Encryption\Encrypter', 'Illuminate\Contracts\Encryption\Encrypter'],
            'db'                   => ['Illuminate\Database\DatabaseManager'],
            'db.connection'        => ['Illuminate\Database\Connection', 'Illuminate\Database\ConnectionInterface'],
            'events'               => ['Illuminate\Events\Dispatcher', 'Illuminate\Contracts\Events\Dispatcher'],
            'files'                => ['Illuminate\Filesystem\Filesystem'],
            'filesystem'           => ['Illuminate\Filesystem\FilesystemManager', 'Illuminate\Contracts\Filesystem\Factory'],
            'filesystem.disk'      => ['Illuminate\Contracts\Filesystem\Filesystem'],
            'filesystem.cloud'     => ['Illuminate\Contracts\Filesystem\Cloud'],
            'hash'                 => ['Illuminate\Contracts\Hashing\Hasher'],
            'translator'           => ['Illuminate\Translation\Translator', 'Symfony\Component\Translation\TranslatorInterface'],
            'log'                  => ['Illuminate\Log\Writer', 'Illuminate\Contracts\Logging\Log', 'Psr\Log\LoggerInterface'],
            'mailer'               => ['Illuminate\Mail\Mailer', 'Illuminate\Contracts\Mail\Mailer', 'Illuminate\Contracts\Mail\MailQueue'],
            'auth.password'        => ['Illuminate\Auth\Passwords\PasswordBrokerManager', 'Illuminate\Contracts\Auth\PasswordBrokerFactory'],
            'auth.password.broker' => ['Illuminate\Auth\Passwords\PasswordBroker', 'Illuminate\Contracts\Auth\PasswordBroker'],
            'queue'                => ['Illuminate\Queue\QueueManager', 'Illuminate\Contracts\Queue\Factory', 'Illuminate\Contracts\Queue\Monitor'],
            'queue.connection'     => ['Illuminate\Contracts\Queue\Queue'],
            'redirect'             => ['Illuminate\Routing\Redirector'],
            'redis'                => ['Illuminate\Redis\Database', 'Illuminate\Contracts\Redis\Database'],
            'request'              => ['Illuminate\Http\Request', 'Symfony\Component\HttpFoundation\Request'],
            'router'               => ['Illuminate\Routing\Router', 'Illuminate\Contracts\Routing\Registrar'],
            'session'              => ['Illuminate\Session\SessionManager'],
            'session.store'        => ['Illuminate\Session\Store', 'Symfony\Component\HttpFoundation\Session\SessionInterface'],
            'url'                  => ['Illuminate\Routing\UrlGenerator', 'Illuminate\Contracts\Routing\UrlGenerator'],
            'validator'            => ['Illuminate\Validation\Factory', 'Illuminate\Contracts\Validation\Factory'],
            'view'                 => ['Illuminate\View\Factory', 'Illuminate\Contracts\View\Factory'],
        ];

        foreach ($aliases as $key => $aliases) {
            foreach ($aliases as $alias) {
                $this->alias($key, $alias);
            }
        }
    }

}
