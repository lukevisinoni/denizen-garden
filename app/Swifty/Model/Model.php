<?php namespace Swift\Swifty\Model;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Base Swifty Model
 * All models in the application extend from this model. 
 */
class Model extends EloquentModel
{
    
}
