<?php namespace Swift\Swifty\Debugbar\Controllers;

class DownloadController extends BaseController
{
    public function index($requestId)
    {
        $storage = $this->debugbar->getStorage();
        if ($data = $storage->get($requestId)) {
            $json = json_encode($data);
            // @todo Is there a more useful format I could use here? CSV? var_dump?
            return response($json)
                ->withHeaders([
                    'Content-Description' => 'File Transfer',
                    'Content-Type' => 'application/octet-stream',
                    'Content-Length' => strlen($json),
                ]);
        } else {
            // @todo Do nothing? I don't know?
        }
    }
}