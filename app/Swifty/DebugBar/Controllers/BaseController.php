<?php namespace Swift\Swifty\Debugbar\Controllers;

use Barryvdh\Debugbar\LaravelDebugbar;
use Illuminate\Routing\Controller;

/**
 * @todo I believe this will make this work even in Lumen
 */
if (class_exists('Illuminate\Routing\Controller')) {

    class BaseController extends Controller
    {
        protected $debugbar;

        public function __construct(LaravelDebugbar $debugbar)
        {
            $this->debugbar = $debugbar;
        }
    }

} else {

    class BaseController
    {
        protected $debugbar;

        public function __construct(LaravelDebugbar $debugbar)
        {
            $this->debugbar = $debugbar;
        }
    }
}
