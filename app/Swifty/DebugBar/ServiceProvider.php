<?php namespace Swift\Swifty\Debugbar;

use Illuminate\Routing\Router;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // nothing to do here...
    }

    /**
     * The only reason I even need a service provider so far is to bootstrap
     * the routes that I need for my data download debugbar extension
     *
     * @return void
     */
    public function boot()
    {
        $routeConfig = [
            'namespace' => '\Swift\Swifty\Debugbar\Controllers',
            'prefix' => '_swifty/debug',
        ];

        $this->getRouter()->group($routeConfig, function($router) {
            $router->get('/download/{requst_id}', [
                'uses' => 'DownloadController@index',
                'as' => 'swifty.debugbar.download',
            ])->where(['request_id' => '[a-zA-Z0-9]{32}']);
        });
    }

    /**
     * Get the active router.
     *
     * @return Router
     */
    protected function getRouter()
    {
        return $this->app['router'];
    }

}
