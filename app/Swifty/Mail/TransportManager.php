<?php namespace Swift\Swifty\Mail;

use Illuminate\Mail\TransportManager as LaravelTransportManager;
use Illuminate\Filesystem\Filesystem;

class TransportManager extends LaravelTransportManager
{
    /**
     * Create an instance of the File Transport driver.
     *
     * @return \Swift\Swifty\Mail\Storage\StorageTransport
     */
    protected function createStorageDriver()
    {
        $storage = new Storage\FilesystemStorage(new Filesystem, config("mail.storage.dir", storage_path()), config("mail.storage.ext", ""));
        return new Transport\StorageTransport($storage);
    }
}
