<?php namespace Swift\Swifty\Mail\Transport;

use Illuminate\Mail\Transport\Transport as AbstractTransport;
use Swift_Mime_Message;
use Swift_Mime_MimeEntity;
use Swift\Swifty\Mail\Storage\StorageInterface;

/**
 * An e-mail transport that saves e-mails rather than sending them.
 */
class StorageTransport extends AbstractTransport
{
    /**
     * A storage instance
     */
    protected $storage;
    
    /**
     * Class constructor
     *
     * @param Storage instance that can write and retrieve e-mails 
     * @return void
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritdoc}
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        $this->beforeSendPerformed($message);
        $this->save($message);
    }
    
    /**
     * Save the message using storage object
     *
     * @param  Swift_Mime_Message The e-mail message being sent
     * @return void
     */
    protected function save(Swift_Mime_Message $message)
    {
        $this->storage->save(
            $message->getId(),
            (string) $message
        );
    }
}
