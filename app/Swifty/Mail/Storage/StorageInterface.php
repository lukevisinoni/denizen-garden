<?php namespace Swift\Swifty\Mail\Storage;

interface StorageInterface
{
    /**
     * Saves an e-mail
     *
     * @param  string Unique identifier for e-mail
     * @param  string The e-mail message in string format
     * @throws Swifty\Mail\Storage\StorageException
     * @return void
     */
    public function save($id, $data);
    
    /**
     * Retrieves an e-mail by ID
     *
     * @param  string Unique identifier for e-mail
     * @return string The e-mail message in string format
     */
    public function get($id);
    
    /**
     * Deletes an e-mail by ID
     *
     * @param  string Unique identifier for e-mail
     * @return void
     */
    public function delete($id);
    
    /**
     * Deletes all e-mails
     *
     * @return void
     */
    public function clear();
}
