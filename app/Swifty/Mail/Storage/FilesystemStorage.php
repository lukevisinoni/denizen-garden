<?php namespace Swift\Swifty\Mail\Storage;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Local file system storage for e-mails
 * @todo use str_finish
 */
class FilesystemStorage implements StorageInterface
{
    /**
     * @var Illuminate\Filesystem\Filesystem
     */
    protected $filesystem;
    
    /**
     * @var string The file extension to use for saving
     */
    protected $ext;
    
    /**
     * @var string The directory to store e-mails
     */
    protected $dir;
    
    /**
     * @var boolean Keep directory with gitignore file
     */
    protected $gitignore;
    
    /**
     * Class constructor
     * 
     * @param  Illuminate\Filesystem\Filesystem
     * @param  string Directory to save files under
     * @param  string Extension to save files under
     * @pram   bool   Set to false if you do not want a gitignore file added to keep your directory
     * @return void
     * @todo   I should probably create accessor methods for ext and dir
     *         I'm not going to create accessors for filesystem though. I
     *         want that to be immutable.
     */
    public function __construct(Filesystem $filesystem, $dir = null, $ext = null, $gitignore = true)
    {
        $this->filesystem = $filesystem;
        $this->dir = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->setExt($ext);
        $this->gitignore = $gitignore;
    }
    
    /**
     * Set the file extension
     *
     * @param  string File extension
     * @return void
     * @throws Swift\Swifty\Mail\Storage\StorageException
     */
    public function setExt($ext)
    {
        $ext = trim(ltrim($ext, "."));
        // extension can only contain alpha-numeric characters and dots/underscores
        if ($ext && !preg_match('/^[a-z0-9\._]+$/i', $ext)) {
            throw new StorageException('Invalid extension: "' . $ext .'"');
        }
        $this->ext = $ext;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function save($id, $data)
    {
        if (!$this->filesystem->isDirectory($this->dir)) {
            if ($this->filesystem->makeDirectory($this->dir, 0755, true)) {
                // this allows git to keep the directory but not the contents
                if ($this->gitignore) $this->filesystem->put($this->dir . '.gitignore', "*\n!.gitignore");
            } else {
                throw new StorageException('Cannot create directory: "' . $this->dir . '"');
            }
        }
        
        try {
            if (!$this->filesystem->put($this->getFilenameFromId($id), $data)) {
                throw new StorageException("Could not save file: " . $this->getFilenameFromId($id));
            }
        } catch (\Exception $e) {
            // just pass on whatever message was given
            throw new StorageException("Could not save: " . $e->getMessage());
        }
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        return $this->filesystem->get($this->getFilenameFromId($id));
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        return $this->filesystem->delete($this->getFilenameFromId($id));
    }
    
    /**
     * Deletes all files in given directory with given ext
     *
     * @return void
     */
    public function clear()
    {
        foreach (Finder::create()->files()->name('*.'.$this->ext)->in($this->dir) as $file) {
            $this->filesystem->delete($file->getRealPath());
        }
        return $this;
    }
    
    /**
     * Generate a valid filename from storage item ID
     */
    protected function getFilenameFromId($id)
    {
        return sprintf("%s%s%s",
            $this->dir,
            $id,
            (empty($this->ext)) ? "" : ".{$this->ext}"
        );
    }
}
