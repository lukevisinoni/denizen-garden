<?php namespace Swift\Swifty\Bootstrap;

use Swift\Swifty\Config\ConfigAggregator;
use Illuminate\Config\Repository;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Config\Repository as RepositoryContract;
use Illuminate\Foundation\Bootstrap\LoadConfiguration;

class OverloadConfiguration extends LoadConfiguration
{
    /**
     * Load the configuration items from all of the files.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Contracts\Config\Repository  $repository
     * @return void
     */
    protected function loadConfigurationFiles(Application $app, RepositoryContract $repository)
    {
        // @todo I need to make sure $app->getCachedConfigPath is acct-aware because it is called in this classes constructor
        // foreach ($this->getConfigurationFiles($app) as $key => $path) {
        //     $repository->set($key, require $path);
        // }

        if ($cust = get_account_custname()) {
            $agg = $app->getConfigAggregator($cust);
            // can I change this to config.agg instead of that long ass name?
            $app->instance('Swift\Swifty\Config\ConfigAggregator', $agg);
            foreach ($agg->toArray() as $key => $config) {
                $repository->set($key, $config);
            }
        }
    }
}
