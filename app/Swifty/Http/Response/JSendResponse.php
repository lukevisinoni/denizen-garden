<?php namespace Swift\Swifty\Http\Response;

class JSendResponse extends AjaxResponse
{
    protected function init() {
        $this->header('Content-Type', 'application/json; charset=utf-8');
    }
}
