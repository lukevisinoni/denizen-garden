<?php namespace Swift\Swifty\Http\Response;

use Illuminate\Http\JsonResponse;
use Swift\Swifty\Http\Response\Payload\PayloadInterface as Payload;

class AjaxResponse extends JsonResponse
{
    /**
     * Constructor.
     *
     * @param  mixed  $data
     * @param  int    $status
     * @param  array  $headers
     * @param  int    $options
     */
    public function __construct(Payload $data = null, $status = 200, $headers = [], $options = 0)
    {
        parent::__construct($data, $status, $headers, $options);
        $this->init();
    }

    // provided for convenience so that you don't have to overwrite constructor
    protected function init() {

    }
}
