<?php namespace Swift\Swifty\Http\Response\Payload;

use Swift\Swifty\Http\Response\Payload\PayloadInterface as IsPayload;
use Illuminate\Contracts\Support\Jsonable;

class JSendPayload implements Jsonable, IsPayload
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    const STATUS_ERROR = 'error';

    protected $data = [];
    protected $status;
    protected $message;
    protected $code;

    public function __construct(Array $data, $status = self::STATUS_SUCCESS, $message = null, $code = null)
    {
        $this->setData($data)
            ->setStatus($status)
            ->setMessage($message)
            ->setCode($code);
    }

    public function setData(Array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setStatus($status)
    {
        $this->status = (string) $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setMessage($message)
    {
        $this->message = (string) $message;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setCode($code)
    {
        if (!is_null($code)) $this->code = (integer) $code;
        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function toJson($options = 0)
    {
        //if ($options === 0) $options = JSON_FORCE_OBJECT;
        return json_encode($this->getPayload(), $options);
    }

    public function getPayload()
    {
        $payload = [
            'status' => $this->getStatus(),
            'data' => $this->getData()
        ];
        if ($message = $this->getMessage()) $payload['message'] = $message;
        if ($code = $this->getCode()) $payload['code'] = $code;
        return $payload;
    }
}
