<?php namespace Swift\Swifty\Http\Response\Payload\JSend;

use Swift\Swifty\Http\Response\Payload\JSendPayload;

class JSendSuccess extends JSendPayload
{
    public function __construct(Array $data)
    {
        parent::__construct($data, self::STATUS_SUCCESS);
    }

}
