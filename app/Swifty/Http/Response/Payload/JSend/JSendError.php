<?php namespace Swift\Swifty\Http\Response\Payload\JSend;

use Swift\Swifty\Http\Response\Payload\JSendPayload;

class JSendError extends JSendPayload
{
    public function __construct($message, Array $data = [], $code = null)
    {
        parent::__construct($data, self::STATUS_ERROR, $message, $code);
    }

    public function getPayload()
    {
        $payload = [
            'status' => $this->getStatus(),
            'message' => $this->getMessage()
        ];
        if ($data = $this->getData()) $payload['data'] = $data;
        if ($code = $this->getCode()) $payload['code'] = $code;
        return $payload;
    }
}