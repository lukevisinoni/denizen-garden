<?php namespace Swift\Swifty\Http\Response\Payload;

interface PayloadInterface
{
    public function getPayload();
}