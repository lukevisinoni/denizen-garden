<?php namespace Swift\Swifty\Config;

use Swift\Swifty\Config\Aggregate\AggregateContract;

/**
 * Coalesces config parameters from various sources into one unified array.
 */
class ConfigAggregator
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array A list of aggregates
     */
    protected $aggregates = [];

    /**
     * @var boolean Determines whether toArray() needs to re-assemble
     */
    protected $dirty = false;

    public function __construct()
    {

    }

    public function push(AggregateContract $aggregate)
    {
        $this->dirty = true;
        return array_push($this->aggregates, $aggregate);
    }

    public function pop()
    {
        $this->dirty = true;
        return array_pop($this->aggregates);
    }

    public function unshift(AggregateContract $aggregate)
    {
        $this->dirty = true;
        return array_unshift($this->aggregates, $aggregate);
    }

    /**
     * Just an alias for unshift
     */
    public function prepend($aggregate)
    {
        return $this->unshift($aggregate);
    }

    public function shift()
    {
        $this->dirty = true;
        return array_shift($this->aggregates);
    }

    public function toArray()
    {
        if ($this->isDirty()) {
            $this->config = $this->assemble();
            $this->dirty = false;
        }
        return $this->config;
    }

    protected function assemble()
    {
        $config = [];
        foreach ($this->aggregates as $agg) {
            $config = array_replace_recursive($config, $agg->toArray());
        }
        return $config;
    }

    protected function isDirty()
    {
        return $this->dirty;
    }
}
//
//$dbConfig = new DBConfig($acct);
//$standardConfig = new FileConfig($standardConfigPath);
//$customAccountConfig = new FileConfig($customConfigPath);
//
//$aggregator = new Aggregator();
//$aggregator->aggregate($standardConfig)
//           ->aggregate($customAccountConfig)
//           ->aggregate($dbConfig);
//
//$configArray = $aggregator->assemble();
