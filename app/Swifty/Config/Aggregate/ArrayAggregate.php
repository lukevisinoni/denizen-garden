<?php namespace Swift\Swifty\Config\Aggregate;

// use SwiftAggregateContract;

class ArrayAggregate implements AggregateContract
{
    protected $arr;
    
    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }
    
    public function toArray()
    {
        return $this->arr;
    }
}
