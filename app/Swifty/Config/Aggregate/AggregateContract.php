<?php namespace Swift\Swifty\Config\Aggregate;

interface AggregateContract
{
    // public function __construct($input);
    public function toArray();
}