<?php namespace Swift\Swifty\Config\Aggregate;

use Symfony\Component\Finder\Finder;

/**
 * Directory Config Aggregate
 * Specify a directory of config files and this will create an array containing
 * the basename of each file as a key and the return array contained in each file
 * as its value
 */
class DirAggregate implements AggregateContract
{
    protected $dir;

    protected $arr = [];

    public function __construct($dir)
    {
        $this->setDir($dir);
        $this->walk();
    }

    protected function walk()
    {
        // @todo throw exception if file doesn't contain array?
        foreach (Finder::create()->files()->name('*.php')->in($this->dir) as $file) {
            if (!is_array($config = include $file)) {
                // report error?
            } else {
                $this->arr[pathinfo($file, PATHINFO_FILENAME)] = $config;
            }
        }
    }

    public function setDir($dir)
    {
        // @todo test that this is a real directory
        $this->dir = $dir;
    }

    public function toArray()
    {
        return $this->arr;
    }
}
