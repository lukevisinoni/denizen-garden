<?php namespace Swift\Swifty\Config\Aggregate;

class EloquentAggregate implements AggregateContract
{
    protected $arr;
    
    public function __construct($json)
    {
        $this->arr = json_decode($json, true);
    }
    
    public function toArray()
    {
        return $this->arr;
    }
}
