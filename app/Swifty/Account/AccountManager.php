<?php namespace Swift\Swifty\Account;

use Swift\Account;
use Swift\Jobs\Account\CreateHomeDirectory;
// use Swift\Jobs\Account\CreateMasterAdminUser;
use Swift\Jobs\Account\CreateSubdomain;
use Swift\Jobs\Account\SendWelcomeEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class AccountManager
{
    use DispatchesJobs;

    public function creating(Account $account)
    {

    }

    /**
     * Create account home directory, config files, etc.
     */
    public function created(Account $account)
    {
        // create subdomain
        $this->dispatch(new CreateSubdomain($account));
        // create home directory
        $this->dispatch(new CreateHomeDirectory($account));
        // send welcome e-mail
        $this->dispatch(new SendWelcomeEmail($account));
        // $this->dispatch(new CreateMasterAdminUser($account));
    }

    public function updating(Account $account)
    {

    }

    public function updated(Account $account)
    {

    }

    public function deleting(Account $account)
    {

    }

    public function deleted(Account $account)
    {

    }
}
