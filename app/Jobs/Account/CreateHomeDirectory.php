<?php namespace Swift\Jobs\Account;

use File;
use Swift\Account;
use Swift\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateHomeDirectory extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $template = realpath(__DIR__ . '/../../Swifty/Account') . DIRECTORY_SEPARATOR .'home-template';
        $template = app_path('Swifty/Account/home-template');
        $home = $this->account->getHomePath();
        if (File::isDirectory($home)) {
            // throw exception? There shouldn't ever be a directory where $home points to so this is definitely an error...
        }
        // create home directory from template
        if (!File::copyDirectory($template, $home)) {
            // throw exception because we couldn't copy the template directory
            // @todo Create custom exception for this
            // @todo Set account status to something like "invalid"
            throw new \Exception('Could not create home directory from template.');
        }
    }
}
