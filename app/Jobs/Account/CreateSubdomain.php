<?php namespace Swift\Jobs\Account;

use Swift\Domain;
use Swift\Account;
use Swift\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSubdomain extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domain = new Domain([
            'account_id' => $this->account->id,
            'name' => $this->account->short .'.'. config('app.domain'),
            'is_primary' => true
        ]);
        $domain->account_id = $this->account->id;
        $domain->save();
    }
}
