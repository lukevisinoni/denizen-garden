<?php namespace Swift\Jobs\Account;

use Mail;
use Swift\Account;
use Swift\User;
use Swift\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * This job is only ran when a dzg admin creates an account without knowing what
 * they want their password (and the like) to be.
 */
class CreateMasterAdminUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;

    protected $options = [
        'send_email' => true, // send the master user a welcome e-mail
        'password_length' => 10, // length of generated password
        'password_chars' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#%^-?_', // characters to use when generating a password
        'attributes' => [], // you can specify whatever attributes you like (e-mail defaults to account e-mail and password is auto-generated if not specified)
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account, $options = [])
    {
        $this->account = $account;
        $this->setOptions($options);
    }

    protected function setOptions($options)
    {
        $this->options = array_replace_recursive($this->options, $options);
    }

    protected function getOption($key, $default = null)
    {
        return array_get($this->options, $key) ?: $default;
    }

    protected function getAttribute($attr)
    {
        // return array_key_exists($attr, $this->options['attributes']) ?
        //     $this->options['attributes'][$attr] :
        //     false;
        return $this->getOption("attributes.". $attr);
    }

    protected function generatePassword()
    {
        $chars = $this->getOption('password_chars');
        $pass = "";
        for ($i = 0; $i < $this->getOption('password_length'); $i++) {
            $key = array_rand(str_split($chars));
            $pass .= $chars[$key];
        }
        return $pass;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = new User([
            'name' => $this->getAttribute("name") ?: $this->account->name .' Administrator',
            'email' => $this->getAttribute("email") ?: $this->account->email,
            'password' => User::hashPassword($pass = $this->getAttribute("password") ?: $this->generatePassword()),
            'account_id' => $this->account->id,
        ]);
        $user->save();
        if ($this->getOption('send_email')) {
            Mail::send('emails.account.master', ['user' => $user, 'password' => $pass], function ($m) use ($user) {
                $m->to($user->email, $user->name)
                  ->subject('Master admin user created for you at '. config('app.title', 'DeniZen Garden') .'!');
            });
        }
    }
}
