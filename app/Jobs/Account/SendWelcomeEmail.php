<?php namespace Swift\Jobs\Account;

use Mail;
use Swift\Account;
use Swift\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $account;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $account = $this->account;
        Mail::send('emails.account.welcome', ['account' => $account], function ($m) use ($account) {
            $m->to($account->email, $account->name)
              ->subject('Welcome to '. config('app.title', 'DeniZen Garden') .'!');
        });
    }
}
