<?php namespace Swift\Providers;

use Illuminate\Mail\MailServiceProvider as BaseMailServiceProvider;
use Swift\Swifty\Mail\TransportManager;

class MailServiceProvider extends BaseMailServiceProvider
{
    /**
     * Register the Swift Transport instance.
     *
     * @return void
     */
    protected function registerSwiftTransport()
    {
        $this->app['swift.transport'] = $this->app->share(function ($app) {
            return new TransportManager($app);
        });
    }
}
