<?php namespace Swift\Providers;

use File;
use Swift\Config;
use Swift\Swifty\Config\Aggregate\ArrayAggregate;
use Illuminate\Config\Repository;
use Illuminate\Support\ServiceProvider;

class CustServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @todo Move all this crap into OverloadConfiguration bootstrapper
     *       See if you can do it using an event listener. If not, then overwrite
     *       the kernal bootstrappers array and put OverloadConfiguration in it
     * @return void
     */
    public function boot()
    {
        // if ($cust = get_account_custname()) {
            // if (is_dir($confPath = cust_path("config"))) {
            // 	foreach (File::files($confPath) as $file) {
            // 		$this->mergeConfigOver(
            // 			require cust_path('config' . DIRECTORY_SEPARATOR . basename($file)),
            // 			pathinfo($file, PATHINFO_FILENAME)
            // 		);
            // 	}
            // }
            // foreach (Config::fetchTree($cust)->toArray() as $key => $params) {
            //     $this->mergeConfigOver($params, $key);
            // }
            // $config = new Repository($this->app->getAggregatedConfigArray($cust));
            // typically this kind of thing would be done within register() but
            // I need DB access
            //$this->app['config'] = $config;
        // }
        $this->pullDbConfig();
    	$this->configViewPaths();
    }

    /**
     * Register the application services.
     *
     * @return void
     * @todo move this stuff into Bootstrap/LoadConfiguration
     */
    public function register()
    {
    }

    protected function pullDbConfig()
    {
        if ($acct = get_account_custname()) {
            $agg = $this->app->make('Swift\Swifty\Config\ConfigAggregator');
            $cachemtime = file_exists($cachefile = $this->app->getCachedConfigPath()) ? filemtime($cachefile) : null;
            $dbconfig = new ArrayAggregate(Config::fetchTree($acct, $cachemtime)->toArray());
            $agg->push($dbconfig);
            foreach ($agg->toArray() as $key => $config) {
                $this->mergeConfigOver($config, $key);
            }
        }
    }

	/**
	 * Configure view paths for per-account customization
	 * When the view finder searches for views, it first needs to look in the
	 * account view folder, only fetching the base view(s) if no custom account
	 * view is available. This method adds the custom per-account view paths to
	 * the view config array.
     *
     * @todo Eventually I may scrap this and just automatically add a view.php
     *       (or put it in the DB) that sets this stuff instead (so that it
     *       can be changed on a customer-to-customer basis if, for instance,
     *       a particular customer has TWO sources of views and neither of them
     *       are where this would expect them)
	 */
	protected function configViewPaths()
	{
        if ($acct = get_account_custname()) {
    		$vp = $this->app['config']['view.paths'];
    		if ($cp = realpath(cust_path('resources/views'))) {
    			array_unshift($vp, $cp);
    			$this->app['config']['view.paths'] = $vp;
    		}
    		if ($vc = realpath(cust_path('storage/framework/views'))) {
    			$this->app['config']['view.compiled'] = $vc;
    		}
        }
	}

	/**
	 * Merge the given configuration over the existing configuration.
	 *
	 * @param  string  $path
	 * @param  string  $key
	 * @return void
	 * @todo   Create a Swifty\ServiceProvider class and inherit from that
     *         (although I don't need this method any more, this is probably
     *         still a good idea... think about it)
     * @todo   I don't think I need this any more... kill it!
	 */
	protected function mergeConfigOver($arr, $key)
	{
		$config = $this->app['config']->get($key, []);
		$this->app['config']->set($key, array_merge($config, $arr));
	}
}
