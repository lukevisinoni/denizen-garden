<?php namespace Swift\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Cache\Console\ClearCommand;

/**
 * @todo I don't think I need this 
 * As it turns out, I don't think I'll end up needing this. Caching on a
 * per-account basis can be done purely through configuration directives. I'll
 * keep the file for now, but I don't think I'm going to need it.
 */

class CacheServiceProvider extends ServiceProvider
{

}
