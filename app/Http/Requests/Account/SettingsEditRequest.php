<?php namespace Swift\Http\Requests\Account;

use Swift\Http\Requests\Request;

class SettingsEditRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:35',
            'website' => 'url',
        ];
    }
}
