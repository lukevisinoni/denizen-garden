<?php namespace Swift\Http\Requests\Account;

use Swift\Http\Requests\Request;

class PasswordChangeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old' => 'required',
            'password' => 'required|min:8', // @todo Require the password contain certain chars
            'password_verify' => 'required|same:password'
        ];
    }
}
