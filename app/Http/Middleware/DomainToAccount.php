<?php namespace Swift\Http\Middleware;

use Closure;
use Swift\Account;
use Swift\Domain;
Use Illuminate\Http\Request;

class DomainToAccount
{
    /**
     * Resolve account by inspecting the hostname used to access the application
     * Eventually I am going to probably have to come up with something a little
     * more robust because both the admin panel AND the customer app (SaaS) are
     * served from this same codebase, but for now I'm writing this thing as if
     * only the customer portion of the app will be run from it.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @todo   You could probably skip a db request here and just fetch the 
     *         account by its "short" name derived from the subdomain
     */
    public function handle($request, Closure $next)
    {
        $host = $request->server('SERVER_NAME');
        if (!$acct = $request->session()->has("account")) {
            try {
                $domain = Domain::where(['name' => $host])->firstOrFail();
                if ($acct = $domain->account) {
                    // save account information in the session so that we don't have to hit the DB on every request
                    $request->session()->put("account", $acct->toArray());
                } else {
                    // domain name is in the DB but isn't associated with any valid account (error)
                    abort("500", "The subdomain you requested is not associated with any known account.");
                }
            } catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                // no record found for domain
                abort("404");
            } catch (\Exception $e) {
                // handle it
                abort("404");
            }
        }
        
        return $next($request);
    }
}
