<?php namespace Swift\Http\Controllers\Account;

use Auth;
use Swift\Http\Requests\Account\ProfileEditRequest;
use Swift\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function init()
    {
        // protects route from unauthenticated requests (@todo move this into route file instead)
        $this->middleware('auth');
    }

    /**
     * Show the edit profile form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('account.profile.edit', ['user' => Auth::user()]);
    }

    /**
     * post to this page to change profile info
     *
     * @return \Illuminate\Http\Response
     * @todo After validation framework is complete and tested, use it instead
     */
    public function postEdit(ProfileEditRequest $request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->save();
        // fire profile update event
        event('ProfileUpdate');
        return redirect()->back()
            ->withInput()
            ->with("success", "Your profile information has been saved.");
    }
}
