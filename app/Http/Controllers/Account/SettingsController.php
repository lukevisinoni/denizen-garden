<?php namespace Swift\Http\Controllers\Account;

use Auth;
use Swift\User;
use Swift\Account;
use Swift\Http\Requests\Account\SettingsEditRequest;
use Swift\Http\Requests\Account\PasswordChangeRequest;
use Swift\Http\Controllers\Controller;

class SettingsController extends Controller
{

    public function index()
    {
        $account = Account::findOrFail(session('account.id'));
        return view('account.settings.index', ['account' => $account->toArray()]);
    }

    public function postEdit(SettingsEditRequest $request)
    {
        $account = Account::findOrFail(session('account.id'));
        $account->update($request->only(['name', 'website', 'email']));
        session(['account' => $account->toArray()]); // replace account in the session
        // fire accout settings update event
        // event('AccountSettingsUpdate');
        return redirect()->back()
            ->withInput()
            ->with("success", "Your account settings have been saved.");
    }

    public function password()
    {
        return view('account.settings.password');
    }

    public function postPassword(PasswordChangeRequest $request)
    {
        $user = Auth::user();
        if ($user->verifyPassword($request->get('password_old'))) {
            return redirect()->back()
                ->with("danger", 'The password you entered does not match your existing password');
        }
        $user->password = $request->get('password');
        $user->save();
        return redirect()->back()
            ->with("success", "You have successfully changed your password");
    }

}
