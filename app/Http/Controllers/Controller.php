<?php namespace Swift\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->init();
    }
    
    /**
     * Placeholder initialize method 
     * Override this method (rather than __construct) in child controllers for
     * initialization tasks (but do not put any init code here, use __construct)
     *
     * @return void
     */
    protected function init() {}
}
