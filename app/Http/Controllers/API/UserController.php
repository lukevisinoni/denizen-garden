<?php namespace Swift\Http\Controllers\API;

use Swift\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidationException;
use Illuminate\Http\ResponseTrait;

class UserController extends Controller
{

    protected static $rules = [
        'name' => 'required|max:65',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
        // 'password_verify' => 'same:password'
    ];

    protected function init()
    {

    }

    /**
     * @todo Create a lookup table so one user can sign up on multiple accounts and share a profile
     * @param Request $request
     * @param $email
     * @return AjaxResponse
     */
    public function getEmailAvailability(Request $request, $email)
    {
        $email = trim(urldecode($email));
        try {
            // @todo eventually I'm going ot want to also provide a list of all accounts this user is associated with as well
            $data = ['email' => $email, 'available' => false];
            $user = User::cust()->select(['id', 'email', 'account_id'])->where(['email' => $email])->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $data['available'] = true;
        } finally {
            return $this->success($data);
        }
    }

    /**
     * Register new user via AJAX
     */
    public function do_register(Request $request)
    {
        try {
            $this->validate($request, self::$rules);
            $user = new User($request->only('name', 'email'));
            $user->account_id = get_subdomain_account_id();
            $user->password = User::hashPassword($request->get('password'));
            $id = $user->save();
            return $this->success([
                'user' => $user->toArray()
            ]);
        } catch (ValidationException $e) {
            return $this->fail([
                'errors' => $e->getResponse()->getData() // I can't get the fucking god damn mother fucking cock sucking JSON encode in my response to handle this correctly
            ]);
        } finally {
            // @todo Send a general error here?
        }
    }

}