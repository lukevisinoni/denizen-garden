<?php namespace Swift\Http\Controllers\API;

use Illuminate\Http\Request;
// use Illuminate\Contracts\Support\Arrayable;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Routing\Controller as BaseController;
use Swift\Swifty\Http\Response\JSendResponse;
use Swift\Swifty\Http\Response\Payload\JSendPayload;
use Swift\Swifty\Http\Response\Payload\JSend\JSendSuccess;
use Swift\Swifty\Http\Response\Payload\JSend\JSendFail;
use Swift\Swifty\Http\Response\Payload\JSend\JSendError;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

abstract class Controller extends BaseController
{
    use ValidatesRequests;
    // @todo Write trait for API Controllers

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        // @todo I'm using the fetch API... it uses a polyfill that uses XMLHttpRequest right now but make sure this will always work
        if (!$request->ajax() || !$request->acceptsJson()) {
            throw new NotAcceptableHttpException("This is a JSON API for use with AJAX requests.");
        }
        $this->init();
    }

    /**
     * Placeholder initialize method
     * Override this method (rather than __construct) in child controllers for
     * initialization tasks (but do not put any init code here, use __construct)
     *
     * @return void
     */
    protected function init() {}

    protected function success(Array $data)
    {
        return $this->jsendResponse(new JSendSuccess($data));
    }

    protected function fail(Array $data)
    {
        return $this->jsendResponse(new JSendFail($data));
    }

    // @todo use an exception to trigger this
    protected function error($message, $code = null, $data = [])
    {
        return $this->jsendResponse(new JSendError($message, $data, $code));
    }
    
    protected function jsendResponse(JSendPayload $payload)
    {
        return new JSendResponse($payload, Response::HTTP_OK);
    }
}
