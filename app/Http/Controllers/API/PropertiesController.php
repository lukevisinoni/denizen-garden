<?php namespace Swift\Http\Controllers\API;

use Collection;
use Swift\User;
use Swift\Property;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidationException;
use Illuminate\Http\ResponseTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @todo Rename this AccountController
 */
class PropertiesController extends Controller
{
    use SoftDeletes;

    // @todo Temporarily disable header checking while writing features
    public function __construct() {}

    public function index(Request $request)
    {
        try {
            $acct_id = get_subdomain_account_id();
            $properties = Property::cust()->orderBy('created_at')->get();
            return $this->success([
                'properties' => $properties
            ]);
        } catch (\Exception $e) {
            return $this->fail(['errors' => ['_error' => 'Could not load initial data']]);
        }
    }

    public function create(Request $request)
    {
        try {
            $acct_id = get_subdomain_account_id();
            $property = new Property($request->all());
            $property->account_id = $acct_id;
            $property->save();
            // @todo Implement redirectTo() method that redirects the API caller
            // to the newly created property "view"... or just supply a URL in
            // the response instead
            // return $this->redirectTo($property);
            return $this->success([
                'property' => $property
            ]);
        } catch (\Exception $e) {
            // @todo Not sure if this should be fail or error
            return $this->fail(['errors' => ['_error' => $e->getMessage()]]);
        }
    }

}
