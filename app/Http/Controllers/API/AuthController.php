<?php namespace Swift\Http\Controllers\API;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Swift\User;
use Swift\Swifty\Http\Response\JSendResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected function init()
    {

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return array_add(
            $request->only($this->loginUsername(), 'password'),
            "account_id",
            get_subdomain_account_id()
        );
    }

    /**
     * @todo Create a lookup table so one user can sign up on multiple accounts and share a profile
     * @param Request $request
     * @param $email
     * @return AjaxResponse
     */
    public function getEmailAvailability(Request $request, $email)
    {
        $email = trim(urldecode($email));
        try {
            // @todo eventually I'm going ot want to also provide a list of all accounts this user is associated with as well
            $data = ['email' => $email, 'available' => false];
            $user = User::cust()->select(['id', 'email', 'account_id'])->where(['email' => $email])->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $data['available'] = true;
        } finally {
            return $this->success($data);
        }
    }

    /**
     * Authenticate user via AJAX
     */
    public function postAuthenticate()
    {
        
    }

}