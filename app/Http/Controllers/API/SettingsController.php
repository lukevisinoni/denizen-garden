<?php namespace Swift\Http\Controllers\API;

use Collection;
use Swift\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidationException;
use Illuminate\Http\ResponseTrait;
/**
 * @todo Rename this AccountController
 */
class SettingsController extends Controller
{

    protected static $rules = [
        'name' => 'required|max:35',
        'email' => 'required|email|max:255|unique:users',
        'website' => 'url'
    ];

    public function __construct() {}

    protected function init()
    {

    }

    public function settings(Request $request)
    {
        try {
            $account = get_subdomain_account();
            return $this->success([
                'settings' => [
                    'name' => $account->name,
                    'website' => $account->website,
                    'email' => $account->email,
                ]
            ]);
        } catch (\Exception $e) {
            return $this->fail(['errors' => ['_error' => 'Could not load initial data']]);
        }
    }

    /**
     * Save (post) account settings
     */
    public function update(Request $request)
    {
        try {
            $this->validate($request, self::$rules);
            $account = get_subdomain_account();
            $account->update($request->only(['name', 'email', 'website']));
            return $this->success([
                'account' => $account->toArray()
            ]);
        } catch (ValidationException $e) {
            return $this->fail([
                'errors' => $e->getResponse()->getData() // I can't get the fucking god damn mother fucking cock sucking JSON encode in my response to handle this correctly
            ]);
        } finally {
            // @todo Send a general error here?
        }
    }

}
