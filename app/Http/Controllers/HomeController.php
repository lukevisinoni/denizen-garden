<?php

namespace Swift\Http\Controllers;

use Swift\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function init()
    {
        // protects route from unauthenticated requests (@todo move this into route file instead)
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
