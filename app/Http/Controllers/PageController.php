<?php namespace Swift\Http\Controllers;

use Swift\Http\Requests;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Show the application home page
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('welcome');
    }
}
