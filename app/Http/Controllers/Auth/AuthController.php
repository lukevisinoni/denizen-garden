<?php namespace Swift\Http\Controllers\Auth;

// @todo Why do mine always have to sart with backslash?
use \Swift\User;
use Validator;
use Illuminate\Http\Request;
use Swift\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function init()
    {
        $this->middleware('guest', ['except' => 'logout']);
        // eventually this will depend on user type but for now just send to home
        // when it comes type to change this, do something like $this-redirectTo = SomeUserTypeHelperClass::resolveDashboardRoute();
        $this->redirectTo = route('dashboard.admin');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return array_add(
            $request->only($this->loginUsername(), 'password'),
            "account_id",
            array_get($request->session()->get("account"), "id")
        );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     * @todo   Change validation rules (name and email should reject anything larger than like 40 chars
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'account_id' => session("account.id"),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
        ]);
    }
}
