<?php

namespace Swift\Http\Controllers\Auth;

use Swift\User;
use Swift\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // redirects user if authenticated
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password. I am overwriting the method
     * defined in the ResetsPasswords mixin because it calls bcrypt(). I use a
     * mutator on the password field so that would result in it twice being hashed
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;
        $user->save();
        Auth::guard($this->getGuard())->login($user);
    }

}
