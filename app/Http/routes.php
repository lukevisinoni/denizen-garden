<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| DeniZen Garden uses named routes exclusively. This allows us to
| completely decouple the application code from its URL scheme. We can
| change the dashboard URL from /dashboard to /admin-panel by making one
| change to this routes file rather than having to go through the entire
| application and change every reference to "/dashboard". And because
| Laravel provides route caching, we get this amazing flexibility without
| sacraficing speed.
|
| @todo Look into caching routes with memcached. In fact, look into caching
|       everything using memcached. Apparently it's pretty rad.
|
*/

/*
|--------------------------------------------------------------------------
| Conventions for route names
|--------------------------------------------------------------------------
|
| * Names should be lower case, all one word (other than exceptions below)
| * Related routes should be prefixed with a namespace and seperated from
|   that namespace with a dot. (auth.login, auth.register, etc.)
| * All POST routes should start with "do_"
| * If the same route url is used for GET and POST, the POST should be
|   named whatever GET is, but prefixed with "do_" (login/do_login)
| * When defining routes, always include the starting slash in the url
|   parameter. This makes it harder to confuse with the route name.
| * Unless there is a good reason, route namespace should coincide with the
|   controller it points to. One possible exception might be when routes
|   are closely related but actions reside in more than one controller. In
|   that case I'd rather the routes have the same namespace.
|
*/

/*
|--------------------------------------------------------------------------
| Account-level application routes
|--------------------------------------------------------------------------
|
| This application acts as if it is installed individually on each account's
| subdomain, but in reality, it maintains everything in one database and serves
| all pages from one router. These are the routes used to serve up the account-
| holder's admin/manager/tenant control panels as well as their listings, etc.
|
*/
Route::group(['middleware' => 'web'], function () {

    Route::group(['namespace' => 'Account'], function() {

        // Controllers Within The "Swift\Http\Controllers\Account" Namespace
        Route::get('/account', ['as' => 'account.settings', 'uses' => 'SettingsController@index']);
        Route::post('/account', ['as' => 'account.settings.do_edit', 'uses' => 'SettingsController@postEdit']);

        Route::get('/account/password', ['as' => 'account.settings.password', 'uses' => 'SettingsController@password']);
        Route::post('/account/password', ['as' => 'account.settings.do_password', 'uses' => 'SettingsController@postPassword']);

        // @todo I think I should change the controller to ProfileController and route names to profile.bla
        Route::get('/profile/edit', ['as' => 'account.profile.edit', 'uses' => 'ProfileController@edit']);
        Route::post('/profile/edit', ['as' => 'account.profile.do_edit', 'uses' => 'ProfileController@postEdit']);

    });

    Route::get('/', ['as' => 'page.welcome', 'uses' => 'PageController@index']);

    // For now I'm going to call this dashboard.admin but eventually the dashboard
    // routes will be generated dynamically
    Route::get('/home', ['as' => 'dashboard.admin', 'uses' => 'HomeController@index']);

    // temporary routes (eventually I'll just add some code to redirect all URLs
    // to the SPA
    Route::get('/properties', ['uses' => 'HomeController@index']);

    // Authentication Routes...
    Route::get('/login', ['as' => 'auth.login', 'uses' => 'PageController@index']);
    Route::post('/login', ['as' => 'auth.do_login', 'uses' => 'PageController@index']);
    //Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'PageController@index']);

    // Registration Routes...
    Route::get('/register', ['as' => 'auth.register', 'uses' => 'PageController@index']);
    Route::post('/register', ['as' => 'auth.do_register', 'uses' => 'PageController@index']);

    /**
     * Boilerplate code calls Route::auth() here but I can't use it because
     * I want exclusively named routes. So I went to where auth() is defined
     * (Illuminate\Routing\Router::auth()) and pulled the routes from it.
     * Eventually it might be a good idea to put them (and other similarly
     * grouped routes) into a method (like Router::auth()) of my own. Possibly
     * even replacing the default router with my own.
     */
    // // Authentication Routes...
    // Route::get('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
    // Route::post('/login', ['as' => 'auth.do_login', 'uses' => 'Auth\AuthController@login']);
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);
    //
    // // Registration Routes...
    // Route::get('/register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    // Route::post('/register', ['as' => 'auth.do_register', 'uses' => 'Auth\AuthController@register']);

    // Password Reset Routes...
    Route::get('/password/reset/{token?}', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@showResetForm'])->where(['token' => '[a-zA-Z0-9]{60}']);
    Route::post('/password/reset', ['as' => 'password.do_reset', 'uses' => 'Auth\PasswordController@reset']);
    Route::post('/password/email', ['as' => 'password.do_sendlink', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);

});

/*
|--------------------------------------------------------------------------
| Local AJAx/JSON API for AJAX/SPA functionality
|--------------------------------------------------------------------------
|
| I think I'm going ot change the route naming convention in the AJAX API.
| Each route should be prefixed with the HTTP verb used to access it. And
| the controller/action names should reflect the HTTP verb and model as well
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'API', 'prefix' => 'ajax'], function () {

    Route::get('/auth/email/{email}', ['as' => 'ajax.auth.email', 'uses' => 'AuthController@getEmailAvailability']);

    Route::post('/user', ['as' => 'ajax.user.create', 'uses' => 'UserController@do_register']);

    Route::get('/account/settings', ['as' => 'ajax.account.settings', 'uses' => 'SettingsController@settings']);
    Route::post('/account/settings', ['as' => 'ajax.account.do_settings', 'uses' => 'SettingsController@update']);

    Route::get('/properties', ['as' => 'ajax.properties', 'uses' => 'PropertiesController@index']);
    Route::post('/properties', ['as' => 'ajax.post_property_create', 'uses' => 'PropertiesController@create']);

});
