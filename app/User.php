<?php namespace Swift;

use Swift\Swifty\Model\User as AuthUser;

class User extends AuthUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'username', 'email', 'password', 'account_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Allows you to access user's full name via $user->name. I added this mainly
     * because I migrated "name" column to firstname and lastname and this will
     * allow any calls to $user->name to still work
     */
    protected $appends = ['name'];

    /**
     * Hash Password
     * In Laravel's standard Auth boilerplate, a naked call to bcrypt()
     * is used to hash user passwords and while that is probably all
     * I'll ever need, I like the comfort of knowing that all I have to
     * do if I want to switch to a different hashing algorithm is change
     * it in this one and only spot and I'm good. So that's why I moved
     * it here.
     *
     * @var string The password to be hashed
     * @return A one-way hash of $password
     */
    static public function hashPassword($password)
    {
        return bcrypt($password);
    }

    public function account()
    {
        return $this->belongsTo('Swift\Account', 'account_id', 'id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception if cant determine account
     */
    public function scopeCust($query)
    {
        return $query->where('account_id', get_subdomain_account_id());
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfAccount($query, $acctId)
    {
        return $query->where('account_id', $acctId);
    }

    public function verifyPassword($password)
    {
        return password_verify($this->password, $password);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = self::hashPassword($password);
    }

    public function getNameAttribute()
    {
        return $this->attributes['name'] = sprintf("%s %s", $this->attributes['firstname'], $this->attributes['lastname']);
    }

    public function setNameAttribute($name)
    {
        list($this->attributes['firstname'], $this->attributes['lastname']) = explode(" ", $name, 2);
    }
}
