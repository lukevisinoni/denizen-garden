<?php namespace Swift;

use Swift\Swifty\Model\Model;
use Swift\Swifty\Account\AccountManager;

class Account extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const STATUS_PENDING = 'pending';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','short','website','email'];

    protected static $cust;

    public static function boot()
    {
        self::observe(new AccountManager);
    }

    /**
     * @todo Cache this?
     */
    public static function getCust()
    {
        return self::$cust = (is_null(self::$cust)) ? self::where(['short' => get_account_custname()])->first() : self::$cust;
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = ['short',];

    public function domains()
    {
        return $this->hasMany('Swift\Domain', 'account_id', 'id');
    }

    public function primaryDomain()
    {
        return $this->hasOne('Swift\Domain', 'account_id', 'id')->where('is_primary', true);
    }

    public function subDomain()
    {
        return $this->hasOne('Swift\Domain', 'account_id', 'id')->where('name', "{$this->short}." . config('app.domain'));
    }

    /**
     * Eventually I guess I'll have to have one user with type "master" but for
     * now this just grabs the oldest user it finds...
     */
    public function primaryUser()
    {
        return $this->hasOne('Swift\User', 'account_id', 'id')->orderBy('created_at', 'asc');
    }

    public function hasCustomPrimaryDomain()
    {
        return ($this->primaryDomain->name != $this->subDomain->name);
    }

    public function getDomains()
    {
        $domains = [];
        foreach ($this->domains as $domain) {
            $domains []= $domain->name;
        }
        return $domains;
    }

    

    /**
     * Scope a query to only include users with a particular status.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function getPortalUrl()
    {
        return 'https://' . $this->primaryDomain->name;
    }

    public function getHomePath($extra = null)
    {
        return cust_path($extra, $this->short);
    }

    public function getBootstrapPath($extra = null)
    {
        return $this->getHomePath('bootstrap/' . $extra);
    }

    public function getConfigPath($extra = null)
    {
        return $this->getHomePath('config/' . $extra);
    }

    public function getPublicPath($extra = null)
    {
        return $this->getHomePath('public/' . $extra);
    }

    public function getResourcesPath($extra = null)
    {
        return $this->getHomePath('resources/' . $extra);
    }

    public function getStoragePath($extra = null)
    {
        return $this->getHomePath('storage/' . $extra);
    }
}
