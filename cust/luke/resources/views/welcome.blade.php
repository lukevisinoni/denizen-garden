@extends('layouts.guest')

@section('title')
    Welcome
@endsection

@section('content')
        
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card content welcome">
                    <div class="card-header">
                        <ul class="pull-right login-options">
                            <li class="login"><a href="{{ route("auth.login") }}">Login</a></li>
                            <li class="register"><a href="{{ route("auth.register") }}">Register</a></li>
                        </ul>
                        <h1 id="logo"><a href="{{ route("page.welcome") }}" class="dzg">Luke's<strong> Properties</strong></a></h1>
                    </div>
                    <div class="card-block">
                        <p>This is a customized home page for luke.denizengarden.com. If you were to load up one of the other subdomains for denizengarden.com you would get the default home page because there aren't any custom views defined for any other accounts but this one.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
