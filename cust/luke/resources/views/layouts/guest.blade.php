<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('title') | {{ config('app.title') }}</title>
        <link href="{{ asset("/css/guest.css") }}" rel="stylesheet">
    </head>
    <body>
    
        @yield("content")
    
        <div class="copy">&copy Copyright 2016 <a href="{{ route("page.welcome") }}" >Luke's Properties</a>. All rights reserved.</div>
        <div class="copy">Powered by <a href="{{ route("page.welcome") }}" class="dzg">Deni<strong>Zen Garden</strong></a></div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        {{-- @todo Make sure this is the right version of jquery for bootstrap
        <script>window.jQuery || document.write('<script src="{{ asset("/js/vendor/jquery.js") }}"><\/script>')</script> --}}
        <script src="{{ asset('/js/vendor/jquery.js') }}"></script>
        <script src="{{ asset('/js/vendor/bootstrap.js') }}"></script>
        <script src="{{ asset('/js/guest.js') }}"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="{{ asset('/js/vendor/ie10-viewport-bug-workaround.js') }}"></script>
        
        @include("layouts._swiftyjsdownload")
    </body>
</html>