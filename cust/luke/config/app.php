<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Title
    |--------------------------------------------------------------------------
    |
    | Overwrites application title with whatever title the account holder desires
    |
    */

    'title' => 'Luke Property Management',

];
