# Swift Property Manager

Built using [Laravel 5.2](http://laravel.com), 
[PHP 5.6](https://php.net),
[MySQL 5.5](http://www.mysql.com),
[PHPUnit 4.4](https://phpunit.de) and
[Composer](https://getcomposer.org)

Swift Property Manager is an SaaS-style application that makes life easier for property owners, managers, tenants, and maintenance personnel. It will have features such as online rent payment, property listings/search, maintenance requests, manager/tenant communications, credit checks and more.

As I determine the features this application will have, I will update this readme. For now, it's time to write some code...