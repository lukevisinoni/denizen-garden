var elixir = require('laravel-elixir');
// var fs = require('fs');
var glob = require('glob');
var gulp = require('gulp');
var sass = require('gulp-sass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management (gulp default)
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 | @todo Combine bootstrap, jquery, and all my files into one?
 | @todo Combine debugbar script(s) into global script when in debug mode
 |
 */

elixir(function(mix) {
    mix.sass(['admin.scss'], 'public/css/admin.css')
       .sass(['guest.scss'], 'public/css/guest.css')
       .scripts(['global.js', 'admin.js'], 'public/js/admin.js')
       .scripts(['global.js', 'guest.js'], 'public/js/guest.js')
       .copy('app/Swifty/Debugbar/Resources/assets/js/swifty.debugutils.js', 'public/js/swifty.debugutils.js')
       .copy('node_modules/bootstrap/dist/js/bootstrap.js', 'public/js/vendor/bootstrap.js')
       .copy('node_modules/jquery/dist/jquery.js', 'public/js/vendor/jquery.js');
});

/*
 |--------------------------------------------------------------------------
 | Customized (per-account) bootstrap themes (gulp cust)
 |--------------------------------------------------------------------------
 |
 | Loop through each of the account directories in the "./cust" directory
 | and run any file not starting with an underscore through "sass" module.
 |
 | @todo If any particular .scss file is unavailable, copy it from the
 |       main sass/bootstrap theme so that custom themes only have to
 |       define the things they want to change. Everything else they will
 |       get for free. Also, create a very basic "main" theme rather than
 |       the very stylized dark theme you have now. And then put that dark
 |       theme (if you want to use it) in cust/www.
 |
 | @todo it would be nice to be able to run this on a specific account only...
 */
gulp.task("cust", function(){

    //return gulp.src("./cust/*/build/sass/@([^_]*).scss")
    //           .pipe(sass().on("error", sass.logError))
    //           .pipe(gulp.dest());

    glob("./cust/*/resources/assets/sass/@([^_]*).scss", function (er, files) {

        if (er) {
            return console.log(er);
        }

        for (i in files) {
            var file = files[i];
            var parts = file.split("/");
            var cust = parts[2];
            var fname = parts[6].split(".")[0];
            gulp.src("./cust/"+ cust +"/resources/assets/sass/"+ fname +".scss")
                .pipe(sass().on("error", sass.logError))
                .pipe(gulp.dest("./cust/"+ cust +"/public/css"));
        }

    });

});


gulp.task("tmp", function(){

    //return gulp.src("./cust/*/build/sass/@([^_]*).scss")
    //           .pipe(sass().on("error", sass.logError))
    //           .pipe(gulp.dest());

    glob("./cust/reliable/resources/assets/sass/@([^_]*).scss", function (er, files) {

        if (er) {
            return console.log(er);
        }

        for (i in files) {
            var file = files[i];
            var parts = file.split("/");
            var cust = parts[2];
            var fname = parts[6].split(".")[0];
            gulp.src("./cust/"+ cust +"/resources/assets/sass/"+ fname +".scss")
                .pipe(sass().on("error", sass.logError))
                .pipe(gulp.dest("./cust/"+ cust +"/public/css"));
        }

    });

});
